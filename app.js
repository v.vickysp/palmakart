var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var hbs = require('handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mysql = require('mysql');

//For Backend Admin Panel(Web Application)
var routes = require('./routes/index');
var users = require('./routes/users');
var service = require('./routes/service');
var settings = require('./routes/settings');
var job_review = require('./routes/job_review');
var dashboard = require('./routes/dashboard');
var amount_details = require('./routes/amount_details');
var report_details = require('./routes/reports');

//For API
var customerRouter = express.Router();
var settingsRouter = express.Router();
var memberRouter = express.Router();
var employeeRouter = express.Router();
var dashboardRouter = express.Router();
var paymentRouter = express.Router();
var changestatusRouter = express.Router();
var acceptRouter = express.Router();


var mysql = require('mysql');

//Mysql Connection
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'palma'
});

//init app

var app = express();

//view engine

app.set('views',path.join(__dirname,'views'));
app.engine('handlebars',exphbs({defaultLayout: 'layout'}));
app.set('view engine','handlebars');


//Helper Functions
hbs.registerHelper('plusone',(val,opts)=>{
	return parseInt(val) + 1;
});

hbs.registerHelper('stat',(val)=>{
	if(val == 1){
		return 1;
	}
});

hbs.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 == v2) {
    return 1;
  }else{
	  return 0;
  }
  
});

hbs.registerHelper('json', function(context) {
	var c = JSON.parse(context);
	
    return c;
});

hbs.registerHelper('offer', function(items, idd, options) {
	var out = '';
	
	var json_parse = JSON.parse(items);
	var l = json_parse.items.length;
	var c = 0;var flag = 1;
	for(var i=0; i<l ; i++ ){
		var offr;
		if(json_parse.items[i] == ""){
			offr = 0;
		}else{
			offr = json_parse.items[i];
		}
		out += "<div id='del_off"+i+"'><div class='col-sm-4' ><label id='disp_label"+i+"'>Count"+(++c)+"(Perc %):</label><input type='number' class='form-control' name ='offer' id='offer"+i+"' placeholder = 'Offer' value='"+offr+"' /></div><div class='col-sm-2' style='padding: 25px;' id='del_bt"+i+"'><a href='#' onclick='cnfrm"+i+"();'><i class='fa fa-trash fa-2x' aria-hidden='true'></i></a></div></div>";

		out += "<script>function cnfrm"+i+"(){swal({ title: 'Are You Sure?',type: 'info',showCancelButton: true,confirmButtonColor: '#DD6B55',confirmButtonText: 'Yes',cancelButtonText: 'No',closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {del_ele"+i+"();} else {swal('Cancelled', ' ', 'error');}});}function del_ele"+i+"(){var ss = document.getElementById('del_off"+i+"');  ss.parentNode.removeChild(ss);del_json("+i+","+idd+"); }";
		
		out +="function del_json(i,id){$.ajax({type: 'POST',data: {str: i,id: id},url: '/settings/del_unit', success: function(data) {if(data == 1){swal({ title: 'Deleted Successfully',type: 'info',confirmButtonColor: '#DD6B55',confirmButtonText: 'Ok',closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href='/settings/edit_price/"+idd+"';} });}else{swal('Failed To Delete');}} });}</script>";

		
	}
	//var c = JSON.parse(context);
	
    return out;
});

hbs.registerHelper('check', function(v1) {
	if(v1 != 0) {
	  return 1;
	}
	
  });
//Helper Functions End Here

//BodyParser Middleware

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
}));
app.use(cookieParser());

//set static folder

app.use(express.static(path.join(__dirname, 'public')));

//Express Session

app.use(session({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
}));


//passport init
app.use(passport.initialize());
app.use(passport.session());
//Express Validator

app.use(expressValidator({
	errorFormatter: function(param, msg, value){
		var namespace = param.split('.')
		, root = namespace.shift()
		, formParam = root;
		
		while(namespace.length){
			formParam += '[' + namespace.shift() + ']';
		}
		return{
			param: formParam,
			msg: msg,
			value: value
		};
	}
}));

//connect flash
app.use(flash());

//Global vars

app.use(function(req,res,next){
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	res.locals.user = req.session.user;
	
	next();
});

app.use('/',routes);
app.use('/users',users);
app.use('/service',service);
app.use('/settings',settings);
app.use('/job_review',job_review);
app.use('/dashboard',dashboard);
app.use('/amount_details',amount_details);
app.use('/reports',report_details);
//app.use('/customer',customer_api);
app.use('/customer',customerRouter);
app.use('/settings_list',settingsRouter);
app.use('/member',memberRouter);
app.use('/employee',employeeRouter);
app.use('/dashboard',dashboardRouter);
app.use('/payment',paymentRouter);
app.use('/statuschange',changestatusRouter);
app.use('/accept_job',acceptRouter);

require('./routes/api/customer')(customerRouter);
require('./routes/api/api_settings')(settingsRouter);
require('./routes/api/member')(memberRouter);
require('./routes/api/employee')(employeeRouter);
require('./routes/api/dashboard')(dashboardRouter);
require('./routes/api/customer_payment')(paymentRouter);
require('./routes/api/change_status')(changestatusRouter);
require('./routes/api/acceptance')(acceptRouter);

//Set Port

app.set('port',(process.env.PORT || 3000));

app.listen(app.get('port'),function(){
	console.log('Server Started On Port'+app.get('port'));
});