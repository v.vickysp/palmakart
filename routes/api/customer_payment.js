var connection = require('./db');
var path = require('path');
var dateFormat = require('dateformat');
var mydate = require('current-date');

module.exports = function(paymentRouter){
    //API For Payment On Customer Module
    paymentRouter.post('/payment_details',function(req,res){
        var booking_id      = req.body.booking_id;
        var transaction_id  = req.body.transaction_id;
        var payment_type    = req.body.payment_type;
        var total_amount    = req.body.total_amount;
        var payable_amount  = req.body.payable_amount;
        var transaction_date        = mydate('date');
        var trans_time              = mydate('time');
        var transaction_datetime    = transaction_date+' '+trans_time;

        if(payment_type == 'Wallet'){
            var wallet_amount   = req.body.wallet_amount;
            var sel_cusid = "select service_booking.customer_id as cus_id,customer_register.wallet as wallet_amount from service_booking inner join customer_register on customer_register.id = service_booking.customer_id where service_booking.job_no='"+booking_id+"'";
            connection.query(sel_cusid,function(err,sel_cus,fields){
                var customer_id     = sel_cus[0]['cus_id'];
                var wallet_old_amount     = sel_cus[0]['wallet_amount'];
                var balance_wallet     = wallet_old_amount - wallet_amount;
                var up_wallet = "update customer_register set wallet='"+balance_wallet+"' where id='"+customer_id+"'";
                connection.query(up_wallet,function(errors,update_wall,fields){
                    var sel_commission = "select commission_amount from commission order by id desc LIMIT 1";
                    console.log(sel_commission);
                    connection.query(sel_commission,function(errors,commission_res,fields){
                            var commission_amount = commission_res[0]['commission_amount'];
                            var pay_commission = payable_amount *(commission_amount/100);
                        var ins_pay = "insert into payment_transaction (booking_id,transaction_id,transaction_date,transaction_datetime,payment_type,payment_mode,total_amount,wallet_amount,paid_amount,pay_commission) values ('"+booking_id+"','"+transaction_id+"','"+transaction_date+"','"+transaction_datetime+"','"+payment_type+"','Online','"+total_amount+"','"+wallet_amount+"','"+payable_amount+"','"+pay_commission+"')";
                        
                        connection.query(ins_pay,function(errors,rows,fields){
                            if(rows){
                                var up_pay = "update service_booking set transaction_id='"+transaction_id+"',paid_amount='"+payable_amount+"',commission_amount='"+pay_commission+"',payment_type='"+payment_type+"',payment_status='Paid',payment_mode='Online' where job_no='"+booking_id+"'";
                                
                                connection.query(up_pay,function(errors,results,fields){
                                    var ins_wallet = "insert into wallet (booking_id,customer_id,type,create_date,paid_amount,amount) values ('"+booking_id+"','"+customer_id+"','Used','"+transaction_date+"','"+payable_amount+"','"+wallet_amount+"')";
                                    connection.query(ins_wallet,function(errors,wallet_res,fields){
                                        if(results){
                                            res.format({
                                                'application/json': function(){
                                                    res.send({status : 'Success',response : 'Payment Done Successfully'});
                                                }
                                            });
                                        }else{
                                            res.format({
                                                'application.json': function(){
                                                    res.send({status: 'Failure',response: 'Payment Failed'});
                                                }
                                            });
                                        } 
                                    });
                                });
                            }
                        });
                    });
                });
            }); 
        }else{
            var wallet_amount   = 0;
            var sel_commission = "select commission_amount from commission order by id desc LIMIT 1";
                    
            connection.query(sel_commission,function(errors,commission_res,fields){
                var commission_amount = commission_res[0]['commission_amount'];
                var pay_commission = payable_amount *(commission_amount/100);
            var ins_pay = "insert into payment_transaction (booking_id,transaction_id,transaction_date,transaction_datetime,payment_type,payment_mode,total_amount,wallet_amount,paid_amount,pay_commission) values ('"+booking_id+"','"+transaction_id+"','"+transaction_date+"','"+transaction_datetime+"','"+payment_type+"','Online','"+total_amount+"','"+wallet_amount+"','"+payable_amount+"','"+pay_commission+"')";
        
            connection.query(ins_pay,function(errors,rows,fields){
                if(rows){
                    var up_pay = "update service_booking set transaction_id='"+transaction_id+"',paid_amount='"+payable_amount+"',commission_amount='"+pay_commission+"',payment_type='"+payment_type+"',payment_status='Paid',payment_mode='Online' where job_no='"+booking_id+"'";
                    connection.query(up_pay,function(errors,results,fields){
                        if(results){
                            res.format({
                                'application/json': function(){
                                    res.send({status : 'Success',response : 'Payment Done Successfully'});
                                }
                            });
                        }else{
                            res.format({
                                'application.json': function(){
                                    res.send({status: 'Failure',response: 'Payment Failed'});
                                }
                            });
                        } 
                    });
                }
            });
        });
        }
    });
}