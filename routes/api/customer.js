//Include Required Modules

//Module For Database Connections
var connection = require('./db');
var path = require('path');

//Module For Mail Functions
var nodemailer = require('nodemailer');

//Module For Date Functions
var dateFormat = require('dateformat');
var mydate = require('current-date');

//Module For Getting Lattitude And Longitude
var NodeGeocoder = require('node-geocoder');
var options = {
  provider: 'google',
  httpAdapter: 'https',
  formatter: null 
};
var geocoder = NodeGeocoder(options);


module.exports = function(customerRouter){
	
	//API For Customer Login
	customerRouter.post('/customer_login', function (req, res) {
		var email = req.body.email;
		var pass = req.body.password;
		var sql = "SELECT customer_register.*,state.state_name AS state_name,state.id as state_id,city.id as city_id, city.city AS city_name FROM customer_register INNER JOIN state ON customer_register.state = state.id  INNER JOIN city ON customer_register.city = city.id where customer_register.email='"+email+"' and customer_register.password='"+pass+"'";
		
		connection.query(sql, function (error, results, fields) {
			if (error) throw error;
			var cunt = results.length;
			if(cunt!=0){
				res.format({
					'application/json': function(){
						res.send({ status:'success',response: results });
					}
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({ status:'Failure',response: 'Invalid Username & Password' });
					}
				});
			}
		});
	});
	
	/* Order History API*/

	function getDriver(employee_id,service_company_id,callback) { 
	 
		var sub = "select staff_name,staff_photo,staff_mobile,staff_unique_id from company_staff where id='"+employee_id+"' and service_company_id='"+service_company_id+"'";
						 var qr = connection.query(sub,function(err,rows,fields){
							 var sub_cnt = rows.length;
							 
							 if(sub_cnt != 0){
								callback(err, rows); 
								
							 }
						 });
	}
	var row = new Array;
	function dis(employee_details,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,subcategory,image,service_company_id,employee_id,customer_name,company_name,company_user_id,mobile_number,profile_image,type,booking_date,cunt,i,res){
		
		row.push({'job_no' : job_no,'work_date' : work_date,'work_time' : work_time,'work_address' : work_address,'max_count' : max_count,'offer' : offer,'offer_amount' : offer_amount,'total_amount' : total_amount,'status' : status,'sub_total' : sub_total,'payment_status' : payment_status,'payment_type' : payment_type,'work_name' : work_name,'work_category' : work_category,'subcategory': subcategory,'image': image,'service_company_id': service_company_id,'employee_id':employee_id,'customer_name' : customer_name,'company_name' : company_name,'company_user_id': company_user_id,'mobile_number': mobile_number,'profile_image': profile_image,'type':type,'booking_date': booking_date,'staff_details' : employee_details});
		
		if(cunt - 1 == i){
			res.format({
				'application/json': function(){
					res.send({ status:'success',response: row });
				}
			});
			row = [];
		}
	}

	customerRouter.post('/order_history', function (req, res) {
		var customer_id = req.body.customer_id;
		var status = req.body.status;
		var where = '';
		var where1 = '';
		var where2 = '';
		if(status == '1'){
			where += "and service_booking.status='Pending'";
			
		}else if(status == '2'){
			where += "and service_booking.status='Processing'";
			where1 += "INNER JOIN service_company_register ON service_company_register.id=service_booking.service_company_id";
			where2 += ",service_company_register.company_name,service_company_register.user_id,service_company_register.type,service_company_register.mobile_number,service_company_register.profile_image";
		}else if(status == '3'){
			where += "and service_booking.status='Ongoing'";
			where1 += "INNER JOIN service_company_register ON service_company_register.id=service_booking.service_company_id";
			where2 += ",service_company_register.company_name,service_company_register.user_id,service_company_register.type,service_company_register.mobile_number,service_company_register.profile_image";
		}else if(status == '4'){
			where += "and (service_booking.status='Completed' || service_booking.status='Cancel')";
			where1 += "INNER JOIN service_company_register ON service_company_register.id=service_booking.service_company_id";
			where2 += ",service_company_register.company_name,service_company_register.user_id,service_company_register.type,service_company_register.mobile_number,service_company_register.profile_image";
		}
		var sql = "SELECT service_booking.job_no,service_booking.date_time,service_booking.work_date,service_booking.work_time,service_booking.work_address,service_booking.max_count,service_booking.offer,service_booking.offer_amount,service_booking.total_amount,service_booking.status,service_booking.service_company_id,service_booking.employee_id,service_booking.sub_total,service_booking.payment_status,service_booking.payment_type, service_name.name as service, service_subcategory.subcategory as subcategory,service_subcategory.image,work_category.work_category,customer_register.customer_name "+where2+" FROM service_booking INNER JOIN customer_register ON service_booking.customer_id = customer_register.id  INNER JOIN service_name ON service_booking.service_id = service_name.id INNER JOIN service_subcategory ON service_booking.service_subcategory_id=service_subcategory.id "+where1+" INNER JOIN work_category ON service_booking.work_category_id = work_category.id and service_booking.customer_id = '"+customer_id+"' "+where+" order by service_booking.id desc";
		
		connection.query(sql, function (error, results, fields) {
			if (error) throw error;
				var cunt = results.length;
							
			if(cunt!=0){
				var i=0;
				
				results.forEach(function(element){
					
					var employee_id	= element.employee_id;
					var service_company_id = element.service_company_id;
					var job_no	= element.job_no;
					var work_date	= element.work_date;
					var work_time	= element.work_time;
					var work_address	= element.work_address;
					var max_count	=	element.max_count;
					var offer	=	element.offer;
					var offer_amount	= element.offer_amount;
					var total_amount	= element.total_amount;
					var status = element.status;
					var sub_total = element.sub_total;
					var payment_status = element.payment_status;
					var payment_type = element.payment_type;
					var work_name = element.work_name;
					var work_category = element.work_category;
					var customer_name =element.customer_name;
					var company_name = element.company_name;
					var company_user_id = element.user_id;
					var type	= element.type;
					var mobile_number = element.mobile_number;
					var profile_image = element.profile_image;
					var subcategory	  = element.subcategory;
					var image		  = '/uploads/subcategory/'+element.image;
					var booking_date	= dateFormat(element.date_time,'dd-mm-yyyy');
					if(employee_id != '0'){
						getDriver(employee_id,service_company_id,function(err, driverRsult){
							var employee_details = new Array;
							driverRsult.forEach(function(ele){
								var staff_name = ele.staff_name;
								var staff_photo = ele.staff_photo;
								var emp_mobile_number = ele.staff_mobile;
								var staff_unique_id = ele.staff_unique_id;

								employee_details.push({staff_name,staff_photo,emp_mobile_number,staff_unique_id});
							});

							dis(employee_details,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,subcategory,image,service_company_id,employee_id,customer_name,company_name,company_user_id,mobile_number,profile_image,type,booking_date,cunt,i,res);
							i++;
						});
					}else{
						var employee_details = new Array;
						var staff_name ='';
						var staff_photo ='';
						var emp_mobile_number = '';
						var staff_unique_id = '';
						
						employee_details.push({staff_name,staff_photo,emp_mobile_number,staff_unique_id});

						dis(employee_details,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,subcategory,image,service_company_id,employee_id,customer_name,company_name,company_user_id,mobile_number,profile_image,type,booking_date,cunt,i,res);
						i++;
					}
					
				});
				
			}else{
				res.format({
					'application/json': function(){
						res.send({ status:'Failure',response: 'No Records Found' });
					}
				});
			}
		});
	});

	/* API For Allot Service Company For Particular Job */

	customerRouter.post('/allot_service',function(req,res){
		var booking_id 			= req.body.booking_id;
		var status              = req.body.status;
        var service_company_id  = req.body.service_company_id;
		var employee_id         = req.body.employee_id;
		
		var sq = "select latitude,longitude from service_booking where job_no='"+booking_id+"'";
		
		connection.query(sq,function(errors,results,fields){	
			var latitude = results[0]['latitude'];
			var longitude = results[0]['longitude'];
			
			var sql_lat = "select *, (6371 * acos(cos(radians("+latitude+")) * cos(radians(`latitude`)) * cos(radians(`longitude`) - radians("+longitude+")) + sin(radians("+latitude+")) * sin(radians(`latitude`)))) AS distance FROM service_company_register HAVING distance < 5 and active_status='1'";
			connection.query(sql_lat,function(errors,rows,fields){
				var len = rows.length;
				var row_array = new Array;
				var id_array = new Array;
				if(status != '1'){
					if(len > 0){
						var sel_cancel = "select count(*) as cancel_count from cancel_history where booking_id='"+booking_id+"'";
						connection.query(sel_cancel,function(errors,cancel_res,fields){
							var cancel_count = cancel_res[0]['cancel_count'];
							if(cancel_count == len){
								res.format({
									'application/json': function(){
									res.send({ status:'success',response: 'Service Company Not Available' });
									}
								});
							}else{
								rows.forEach(function(element){
									var id = element.id;
									var allot_job = element.allot_job;
									row_array.push(allot_job);
									id_array.push(id);
								});
								var r = Math.min.apply(null,row_array);
								
								var min = row_array[0];
								var minIndex = 0;
								var u_id = id_array[0];
							
								for (var i = 1; i < row_array.length; i++) {
									
									if (row_array[i] < min) {
										minIndex = i;
										min = row_array[i];
										u_id = id_array[i];
									}
								}
								
								if(status != '0'){
									var sql_allot = "update service_company_register set allot_job='"+(Number(min) + 1)+"' where id='"+u_id+"'";
									connection.query(sql_allot,function(errors,up_res,fields){
										var up_book = "update service_booking set service_company_id='"+u_id+"' where job_no='"+booking_id+"'";
										connection.query(up_book,function(errs,book_res,fields){
											if(book_res){
												if(cancel_count == len){
													// Push Notification area
													res.format({
														'application/json': function(){
														res.send({ status:'Failure',response: 'Service Company Not Available' });
														}
													});
												}else{
												// Push Notification area
													res.format({
												
														'application/json': function(){
														res.send({ status:'success',response: 'Temporary Allotment Done' });
														}
													});
												}
											}
										});
									});
			
								}
						
								if(status == '0'){
									var cur_date        = mydate('date');
									var cur_time        = mydate('time');
									var cur_datetime    = cur_date+' '+cur_time;

									var ins_cancel_details = "insert into cancel_history (booking_id,service_company_id,dated_at,dated_attime) values ('"+booking_id+"','"+service_company_id+"','"+cur_date+"','"+cur_datetime+"')";
									connection.query(ins_cancel_details,function(errors,ins_his,fields){
										
												var up_cancel = "update service_company_register set cancel_count=(@cur_value := cancel_count) + 1 where id='"+service_company_id+"'";
												connection.query(up_cancel,function(errors,results,fields){
													var up_book = "update service_booking set service_company_id='0' where job_no='"+booking_id+"'";
													connection.query(up_book,function(errs,book_res,fields){
														
														res.format({
															'application/json': function(){
															res.send({ status:'success',response: 'Company Cancelled' });
															}
														});
													});
												});
											
										});
								}
							}
						});
						
					}else{
						res.format({
							
							'application/json': function(){
							res.send({ status:'Failure',response: 'Service Company Not Availavble' });
							}
						});
					}
				}else{
					var up_accept = "update service_company_register set accept_count=(@cur_value := accept_count) + 1 where id='"+service_company_id+"'";
					connection.query(up_accept,function(errors,results,fields){
						if(employee_id == ''){
							employee_id = 0;
						}
						var up_booking = "update service_booking set service_company_id='"+service_company_id+"',employee_id='"+employee_id+"',status='Processing' where job_no='"+booking_id+"'";
						connection.query(up_booking,function(errs,rows,fields){
							res.format({
								'application/json': function(){
									res.send({status: 'Success',response: 'Job Accepted Successfully'});
								}
							});
						});
					});
				}
			});
			
		});
	});
	
	//API For Change Password
	customerRouter.post('/change_password',function(req,res){
		var old_password = req.body.old_password;
		var customer_id = req.body.customer_id;
		var new_password = req.body.new_password;
		var sql_old_password = "select password,id from customer_register where id='"+customer_id+"'";
		connection.query(sql_old_password,function(errors,results,fields){
			if(results[0]['password'] == old_password){
				var sql_password = "update customer_register set password='"+new_password+"' where id='"+customer_id+"'";
				connection.query(sql_password,function(errs,rows,fields){
					if(rows){
						res.format({
							'application/json': function(){
								res.send({status: 'Success',response: 'Password Changed Successfully'});
							}
						});
					}
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({status: 'Failure',response: 'Your Old Password Is Incorrect'});
					}
				});
			}
		});
	});

	//API For Customer FeedBack
	customerRouter.post('/feedback',function(req,res){
		var email = req.body.email;
		var description = req.body.description;
		var feed_date = mydate('date');
	 	var feed_t = mydate('time');
	   	var feed_datetime = feed_date+' '+feed_t;
		var sql_cus = "select customer_name from customer_register where email='"+email+"'";
		connection.query(sql_cus,function(errs,rows,fields){
			var customer_name = rows[0]['customer_name'];
			var sql_feed = "insert into palma_feedback (customer_name,email,description,feed_date,feed_datetime) values ('"+customer_name+"','"+email+"','"+description+"','"+feed_date+"','"+feed_datetime+"')";
			connection.query(sql_feed,function(errors,results,fields){
				if(results){
					res.format({
						'application/json': function(){
							res.send({status: 'Success',response: 'Sent Successfully'});
						}
					});
				}else{
					res.format({
						'application/json': function(){
							res.send({status: 'Failure',response: 'Failed To Insert'});
						}
					});
				}
			});
		});
	});

	//API For Customer Complaints
	customerRouter.post('/complaint_new',function(req,res){
		var email = req.body.email;
		var job_no = req.body.job_no;
		var complaint = req.body.complaint;
		var sql_cust = "select customer_name,address from customer_register where email='"+email+"'";
		connection.query(sql_cust,function(errs,rows,fields){
			var customer_name = rows[0]['customer_name'];
			var customer_address = rows[0]['address'];
			var sql_service = "select service_company_id,employee_id from service_booking where job_no='"+job_no+"'";
			var flag = 0;
			connection.query(sql_service,function(errs,news,fields){
				var service_company_id = news[0]['service_company_id'];
				var employee_id = news[0]['employee_id'];
				var sql_employee;
				
				if(employee_id == '0'){
					sql_employee = "select company_name,user_id from service_company_register where id='"+service_company_id+"'";
				}else{
					sql_employee = "select company_staff.staff_name,company_staff.staff_unique_id,service_company_register.company_name,service_company_register.user_id from company_staff inner join service_company_register on service_company_register.id = company_staff.service_company_id where company_staff.id='"+employee_id+"' and service_company_id='"+service_company_id+"'";
					flag = 1;
				}
				
				
				connection.query(sql_employee,function(errors,staff_na,fields){
					var company_name;
					var user_id;
					var staff_name;
					var staff_unique_id;
					
					if(flag == '0'){
						company_name = staff_na[0]['company_name'];
						user_id = staff_na[0]['user_id'];
						staff_name = '';
						staff_unique_id = '';
					}else{
						company_name = staff_na[0]['company_name'];
						user_id = staff_na[0]['user_id'];
						staff_name = staff_na[0]['staff_name'];
						staff_unique_id = staff_na[0]['staff_unique_id'];
					}

					var sql_complaint =  "insert into palma_complaints (customer_name,customer_address,email,job_no,description,company_name,user_id,staff_name,staff_unique_id	) values ('"+customer_name+"','"+customer_address+"','"+email+"','"+job_no+"','"+complaint+"','"+company_name+"','"+user_id+"','"+staff_name+"','"+staff_unique_id+"')";
					
					connection.query(sql_complaint,function(errors,results,fields){
						if(results){
							res.format({
								'application/json': function(){
								res.send({ status:'Success',response: 'Complaint Done Successfully' });
								}
							});
						}else{
							res.format({
								'application/json': function(){
								res.send({ status:'Failure',response: 'Failed To Insert' });
								}
							});
						}
					});
				});
			});
		});
	});

	//API For Listing Order number For Complaints
	customerRouter.post('/booking_list',function(req,res){
		var customer_id = req.body.customer_id;
		var sql_cus = "select service_booking.job_no,work_category.work_category from service_booking INNER JOIN work_category ON work_category.id= service_booking.work_category_id where service_booking.customer_id='"+customer_id+"' and service_booking.status='Completed'";
		
		connection.query(sql_cus,function(errors,results,fields){
			var len = results.length;
			var row2 = new Array;
			if(len > 0){
				results.forEach(function(element){
					var work_category = element.work_category;
					var job_no = work_category+" ("+element.job_no+")";
					var booking = element.job_no;
					row2.push({job_no,booking});
					
				});

				res.format({
			   
					'application/json': function(){
					   res.send({ status:'Success',response: row2 });
					}
				});
			}else{
				res.format({
					'application/json': function(){
					   res.send({ status:'Failure',response: 'No Records Found' });
					}
				});
			}
		});
	});

	//Wallet History API
	customerRouter.post('/wallet_history',function(req,res){
		var customer_id = req.body.customer_id;
		var sql_cus = "select wallet from customer_register where id='"+customer_id+"'";
		connection.query(sql_cus,function(errors,results,fields){
			
			if(results.length != '0'){
				var total_wallet = results[0]['wallet'];
				var sql_his = "select * from wallet where customer_id = '"+customer_id+"'";
				connection.query(sql_his,function(errs,rows,fields){
					var history = new Array;
					
					rows.forEach(function(element){
						var booking_id = element.booking_id;
						var type = element.type;
						var create_date = dateFormat(element.create_date, "dd-mm-yyyy");
						var amount = element.amount;
						history.push({booking_id,type,create_date,amount});
					});
					res.format({
						'application/json' : function(){
							res.send({status: 'Success',response: {total_wallet,history}});
						}
					});
				});

			}else{
				res.format({
					'application/json' : function(){
						res.send({status: 'Failure',response: 'No Records Found'});
					}
				});
			}
		});
	});

	//API For Customer Register
	customerRouter.post('/customer_register', function (req, res) {
	   var cus_name  = req.body.customer_name;
	   var contact_name = req.body.contact_name;
	   var address = req.body.address;
	   var pincode = req.body.pincode;
	   var city = req.body.city;
	   var state = req.body.state;
	   var contact_number = req.body.contact_number;
	   var mobile_number = req.body.mobile_number;
	   var password = req.body.password;
	   var email = req.body.email;
	   
	   var reg_date = mydate('date');
	   var reg_time = mydate('time');
	   var reg_date_time = reg_date+' '+reg_time;
	   
	   var bank_name = req.body.bank_name;
	   var branch_name = req.body.branch_name;
	   var ifsc = req.body.ifsc;
	   var cheque_name = req.body.cheque_name;
	   var account_number = req.body.account_number;

	   var sel_customer = "select user_id from customer_register order by id desc LIMIT 1";
	   connection.query(sel_customer,function(errors,sel_com,fields){
		var user_unique_id;
		if(sel_com.length == 0){
			user_unique_id = 'PSCUS'+1;
		}else{
			var job_auto = sel_com[0]['user_id'];
			var left_text = job_auto.slice(5);
			
			var auto_id = Number(left_text) + 1;
			user_unique_id = 'PSCUS'+auto_id;
		}
		var sql = "insert into customer_register (user_id,customer_name,contact_name,address,pincode,city,state,contact_number,mobile_number,password,email,reg_date,reg_date_time,account_number,bank_name,branch_name,ifsc_code,cheque) values ('"+user_unique_id+"','"+cus_name+"','"+contact_name+"','"+address+"','"+pincode+"','"+city+"','"+state+"','"+contact_number+"','"+mobile_number+"','"+password+"','"+email+"','"+reg_date+"','"+reg_date_time+"','"+account_number+"','"+bank_name+"','"+branch_name+"','"+ifsc+"','"+cheque_name+"')";

		connection.query(sql, function (error, results, fields) {
			if (error) throw error;
			//res.end(JSON.stringify("Success: Successfully Registered."));
			res.format({
				
				'application/json': function(){
					res.send({ status:'success',response: 'Successfully Registered' });
				}
				});
			});
		});
	});
	
	

	//API For Forgot Password
	customerRouter.post('/forgot_password', function (req, res) {
		var email  = req.body.email;

		var sq = "select password from customer_register where email='"+email+"'";
		connection.query(sq,function(err,rows,fields){
			var pass = rows[0]['password'];
			const sendmail = require('sendmail')();
 
				sendmail({
					from: 'no-reply@yourdomain.com',
					to: email,
					subject: 'Regarding Password',
					html: pass,
				}, function(err, reply) {
					if(err){
						console.log(err && err.stack);
					}else{
					
						res.format({
							
							'application/json': function(){
							res.send({ status:'success',response: 'Mail Sent' });
							}
						});
					}
				});
		
		});
	});

	
	//API For Customer Update
	customerRouter.post('/customer_update', function (req, res) {
		var params  = req.body;
	   	var up_date = mydate('date');
	   	var up_time = mydate('time');
		var up_date_time = up_date+' '+up_time;
		
	  	 var qury = connection.query('UPDATE `customer_register` SET `customer_name`=?,`contact_name`=?,`address`=?,`pincode`=?,`city`=?,`state`=?,`contact_number`=?,`mobile_number`=?,`email`=?, account_number=?,bank_name=?,branch_name=?,ifsc_code=?,cheque=?,updated_date=?,updated_datetime=? where `id`=?', [req.body.customer_name,req.body.contact_name,req.body.address,req.body.pincode,req.body.city,req.body.state,req.body.contact_number,req.body.mobile_number,req.body.email,req.body.account_number,req.body.bank_name,req.body.branch_name,req.body.ifsc,req.body.cheque_name,up_date,up_date_time,req.body.id], function (error, results, fields) {
		  if (error) throw error;
		  //res.end(JSON.stringify(results));
		  	if(results.changedRows == '1'){
				  
				  var sel_qry = "select customer_register.id,customer_register.customer_name,customer_register.contact_name,customer_register.contact_number,customer_register.mobile_number,customer_register.address,customer_register.pincode,customer_register.email,customer_register.password,customer_register.account_number,customer_register.bank_name,customer_register.branch_name,customer_register.ifsc_code,customer_register.cheque,customer_register.wallet,state.state_name AS state_name,state.id as state_id,city.id as city_id, city.city AS city_name from customer_register INNER JOIN state ON state.id = customer_register.state INNER JOIN city ON city.id = customer_register.city where customer_register.id='"+req.body.id+"'";
				connection.query(sel_qry,function(errors,rows,fields){
					res.format({
						
						'application/json': function(){
							res.send({ status:'success',response: rows });
						}
					});
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({status:'Failure',response: 'Failed To Update'});
					}
				});
			}
		});
		
	});

	//Service Booking For Customer

	customerRouter.post('/book', function (req, res) {
	   var customer_id  = req.body.customer_id;
	   var service_id  = req.body.service_id;
	   var service_subcategory_id  = req.body.service_subcategory_id;
	   var brand_id  = req.body.brand_id;
	   var work_id  = req.body.work_id;
	   var work_category_id = req.body.work_category_id;
	   var work_time = req.body.work_time;
	   var work_address = "'"+req.body.work_address+"'";
	   var work_date = req.body.work_date;
	   var state_id = req.body.state_id;
	   var city_id = req.body.city_id;

	   var created_dt = mydate('date');
	   var created_time = mydate('time');
	   var date_time = created_dt+' '+created_time;
	   
	   var max_count = req.body.max_count;
	   var sub_total = req.body.sub_total;
	   var offer_percentage = req.body.offer_percentage;
	   var offer_amount = req.body.offer_amount;
	   var total = req.body.total;
		
		var sq = "select job_no from service_booking order by id desc limit 1";
		connection.query(sq,function(err,rows,fields){
			var job_no;
			
			if(rows.length == '0'){
				job_no = 'PSJOB_'+1;
			}else{
				var job_auto = rows[0]['job_no'];
				var left_text = job_auto.slice(6);
				
				var auto_id = Number(left_text) + 1;
				job_no = 'PSJOB_'+auto_id;
			}
			
			geocoder.geocode(work_address, function(err, ress) {
				
				var lattitude = ress[0]['latitude'];
				var longitude = ress[0]['longitude'];
				var values =[customer_id,job_no,service_id,service_subcategory_id,brand_id,work_id,work_category_id,work_date,work_time,work_address,city_id,state_id,date_time,max_count,offer_percentage,offer_amount,'Pending',sub_total,total,'Unpaid',lattitude,longitude];
				connection.query('INSERT INTO service_booking (customer_id, job_no, service_id, service_subcategory_id, brand_id, work_id, work_category_id, work_date, work_time, work_address, city,state,date_time,max_count,offer,offer_amount,status,sub_total,total_amount,payment_status,latitude,longitude) VALUES ?', [[values]], function (error, results, fields) {
					if (error) throw error;
					var sel_booking = "select job_no from service_booking where id='"+results.insertId+"'";
					connection.query(sel_booking,function(errors,book_res,fields){
						var booking_id = book_res[0]['job_no'];
						
						if(results){
							res.format({
								'application/json': function(){
									res.send({ status:'success',response: 'Service Booked Successfully',booking_id });
								}
							});
						}else{
							res.format({
								'application/json': function(){
									res.send({status:'Failure',response: 'Failed To Book'});
								}
							});
						}
					});
				});
			});
		});
		
	});
	
	//API For Check Email Already Exist For Customer

	customerRouter.post('/check', function (req, res) {
		var email = req.body.email;
		
		if(email != ''){
		   var qr = connection.query('select * from customer_register where email=?', [email],function (error, results, fields) {
			   var num = results.length;
			   
			  if (error) throw error;
			  
			  if(num != 0){
				  res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'Email Already Exists!' });
					   }
					});
				 
			  }else{
				   res.format({
					   
					   'application/json': function(){
						  res.send({ status:'success',response: 'Welcome' });
					   }
					});
			  }
			});
			
		}
	});

	//API For Cancel Job In Customer
	customerRouter.post('/cancel_job',function(req,res){
		var booking_id	= req.body.booking_id;
		var cancel_date	= mydate('date');
		var cancel_time	= mydate('time');
		var cancel_datetime = cancel_date+' '+cancel_time;

		var up_booking = "update service_booking set status='Cancel',cancel_date='"+cancel_date+"',cancel_date_time='"+cancel_datetime+"' where job_no='"+booking_id+"'";
		connection.query(up_booking,function(errors,results,fields){
			if(results){
				res.format({
					'application/json': function(){
						res.send({status: 'Success',response: 'Job Cancelled Successfully'});
					}
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({status: 'Failure',response: 'Failed To Cancel'});
					}
				});
			}
		});
	});
	
}

