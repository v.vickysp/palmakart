
var mysql = require('mysql');
var path = require('path');
var dateFormat = require('dateformat');
var mydate = require('current-date');

//Mysql Connection
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'palma'
});

function getDriver(service_id,callback) { 
	var sub = 'select * from service_subcategory where service_name="'+service_id+'"';
	var qr = connection.query(sub,function(err,rows,fields){
		var sub_cnt = rows.length;
		if(sub_cnt != 0){
			callback(err, rows); 
		}
	});
}
var row = new Array;
function dis(ss,na,service_id,res,cunt,i){	
	row.push({'service_id':service_id,'service_name': na,'subcategories': ss});
	if(cunt - 1 == i){
		res.format({
			'application/json': function(){
				res.send({ status:'success',response:row });
			}
		});
		row = [];
	}
}


function getBrand(service_id,subcategory_id,brand_id,callback) { 
	var sub = 'select work_name from palma_work where service_id="'+service_id+'" and subcategory_id="'+subcategory_id+'" and brand_id="'+brand_id+'" and active_status="1"';
	var qr = connection.query(sub,function(err,rows,fields){
		var sub_cnt = rows.length;
		if(sub_cnt != 0){
			callback(err, rows); 
		}
	});
}
var row_work = new Array;
function dis_work(row2,service_id,service_sub_id,service_sub_name,brand_name,brand_id,brand_image,res,cunt,i){
	
	row_work.push({'brand_id':brand_id,'brand_name': brand_name,'brand_image': brand_image, 'brand': row2});
	
	if(cunt - 1 == i){
		res.format({
			'application/json': function(){
			res.send([{ status:'success',response:[{'service_id': service_id,'service_sub_id': service_sub_id,'subcategory_name' : service_sub_name,'work': row_work}] }]);
				
			}
		});
		row_work = [];
		
	}
}

module.exports = function(settingsRouter){
	//rest api to get List Of Service
	settingsRouter.post('/service_list', function (req, res) {
			
			//var sql = "SELECT service_name.name,service_name.id,service_subcategory.subcategory,service_subcategory.image FROM service_name INNER JOIN service_subcategory ON service_name.id = service_subcategory.service_name";
			var sql = 'select * from service_name where active_status="1" limit 6';
		  connection.query(sql, function (error, results, fields) {
			  if (error) throw error;
					var cunt = results.length;
				 if(cunt!=0){
					 var i=0
					 
					 results.forEach(function(element){

						var service_name =element.name;
						var service_id = element.id;
						getDriver(service_id,function(err, driverRsult){
							var row2 = new Array;
							driverRsult.forEach(function(ele){
								na = ele.service_name;
							var subcategory_id = ele.id;
							var sub_cat = ele.subcategory;
							var image = '/uploads/subcategory/'+ele.image;
							   
							   row2.push({subcategory_id,sub_cat,image});
							});
							dis(row2,service_name,service_id,res,cunt,i);
							i++;
						});
					 });
					
				 }else{
					 res.format({
						   
						   'application/json': function(){
							  res.send({ status:'Failure',response: 'No Records Found' });
						   }
						});
				 }
			});
	});
	

	settingsRouter.post('/service_sub_catid', function (req, res) {

		var service_id = req.body.service_id;
		var sql = 'select name from service_name where id="'+service_id+'"';
	  connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
				var cunt = results.length;
			 if(cunt!=0){
				 var i=0
				 var service_name = results[0]['name'];
				 var sq = 'select * from service_subcategory where service_name="'+service_id+'" and active_status="1"';
				 connection.query(sq, function (error, serv_na, fields) {
					
					var sub_serv = new Array;
					serv_na.forEach(function(element){

						var subcategory =element.subcategory;
						var image = '/uploads/subcategory/'+element.image;
						sub_serv.push({subcategory,image});

					 });

					 res.format({
						'application/json': function(){
						res.send({ status:'success',response:{'service_name': service_name,'subcategories': sub_serv} });
							
						}
					});

				});
				
			 }else{
				 res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'No Records Found' });
					   }
					});
			 }
		});
	});

	settingsRouter.post('/brand_list', function (req, res) {
		var service_id = req.body.service_id;
		var service_sub_id = req.body.service_sub_id;
		
		var sql = "select palma_brand.brand_name,palma_brand.id,palma_brand.brand_img,service_subcategory.subcategory from palma_brand INNER JOIN service_subcategory ON service_subcategory.id='"+service_sub_id+"' where palma_brand.service_id='"+service_id+"' and palma_brand.subcategory_id='"+service_sub_id+"' and palma_brand.active_status = '1'";
		connection.query(sql, function (error, results, fields) {
			if (error) throw error;
				  var cunt = results.length;
			   if(cunt!=0){
				var service_sub_name = results[0]['subcategory'];
				   	  var i=0
					  var sub_serv = new Array;
					  results.forEach(function(element){
						  var brand_name = element.brand_name;
						  var brand_id = element.id;
						  var brand_image = '/uploads/brand/'+element.brand_img;
						  getBrand(service_id,service_sub_id,brand_id,function(err, driverRsult){
							var row2 = new Array;
							driverRsult.forEach(function(ele){
							
							   var work_name = ele.work_name;
							   var work_id = element.id;
							   row2.push({work_name,work_id});
							});
							dis_work(row2,service_id,service_sub_id,service_sub_name,brand_name,brand_id,brand_image,res,cunt,i);
							i++;
						  });
					   });
  
			   }else{
					var sq = "select palma_work.work_name,palma_work.id from palma_work where palma_work.service_id='"+service_id+"' and palma_work.subcategory_id='"+service_sub_id+"' and palma_work.brand_id='0' and palma_work.active_status='1'";
					
				   connection.query(sq,function(err,row,fields){
					   var ser_work = new Array;
					   var sel_sub = "select subcategory from service_subcategory where id='"+service_sub_id+"'";
					   connection.query(sel_sub,function(errors,subs,fields){

						var service_sub_name = subs[0]['subcategory'];
						
					   if(row.length != '0'){
							var brand = new Array;
							var brand_name = service_sub_name;
								var brand_id = service_sub_id;
								var brand_image = '';
							row.forEach(function(element){
								
								var work_name = element.work_name;
								var work_id = element.id;
								brand.push({work_name,work_id});
							});
							ser_work.push({brand_name,brand_id,brand_image,brand});
						}
							res.format({
								'application/json': function(){
								res.send([{ status:'success',response:[{'service_id': service_id,'service_sub_id': service_sub_id,'subcategory_name':service_sub_name,'work': ser_work} ]}]);
									
								}
							});
						});
					});
			   }
		  });
		 
	});
	
	
	settingsRouter.post('/work_category', function (req, res) {

		var work_id = req.body.work_id;
		var sql = 'select work_category,id,work_id from work_category where work_id="'+work_id+'" and active_status="1"';
	  connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
				var cunt = results.length;
			 if(cunt!=0){
					var sub_serv = new Array;
					var work_id = results[0]['work_id'];
					results.forEach(function(element){
						var work_category =element.work_category;
						var work_category_id =element.id;
						sub_serv.push({work_category_id,work_category});
					 });

					 res.format({
						'application/json': function(){
						res.send({ status:'success',response: [{'work_id': work_id,sub_serv}]});
							
						}
					});
				
			 }else{
				 res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'No Records Found' });
					   }
					});
			 }
		});
	});


	//rest api to create a new customer record into mysql database
	settingsRouter.post('/customer_register', function (req, res) {
	   var cus_name  = req.body.customer_name;
	   var contact_name = req.body.contact_name;
	   var address = req.body.address;
	   var pincode = req.body.pincode;
	   var city = req.body.city;
	   var state = req.body.state;
	   var contact_number = req.body.contact_number;
	   var mobile_number = req.body.mobile_number;
	   var password = req.body.password;
	   var email = req.body.email;
	   
	   var reg_date = mydate('date');
	   var reg_time = mydate('time');
	   var reg_date_time = reg_date+' '+reg_time;
	   
	   
	   var sql = "insert into customer_register (customer_name,contact_name,address,pincode,city,state,contact_number,mobile_number,password,email,reg_date,reg_date_time) values ('"+cus_name+"','"+contact_name+"','"+address+"','"+pincode+"','"+city+"','"+state+"','"+contact_number+"','"+mobile_number+"','"+password+"','"+email+"','"+reg_date+"','"+reg_date_time+"')";

	   connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
		  //res.end(JSON.stringify("Success: Successfully Registered."));
		  res.format({
			   
			   'application/json': function(){
				  res.send({ status:'success',response: 'Successfully Registered' });
			   }
			});
		});
		
	 
		
	});
	

	
	//rest api to update record into mysql database
	settingsRouter.post('/customer_update', function (req, res) {
		var params  = req.body;
		var cname = req.body.cname;
	   var mobile1 = req.body.cmobile;
	   
	   
	  if(cname != '' && mobile1 != ''){
	   var qury = connection.query('UPDATE `customer_register` SET `customer_name`=?,`contact_name`=?,`address`=?,`pincode`=?,`city`=?,`state`=?,`contact_number`=?,`mobile_number`=?,`email`=?, `password`=? where `id`=?', [req.body.cname,req.body.con_name,req.body.address,req.body.pincode,req.body.city,req.body.state,req.body.cmobile,req.body.mobile,req.body.email,req.body.password,req.body.id], function (error, results, fields) {
		  if (error) throw error;
		  //res.end(JSON.stringify(results));
		  res.format({
			   
			   'application/json': function(){
				  res.send({ status:'success',response: 'Successfully Updated' });
			   }
			});
		});
		console.log(qury.sql);
	  }else{
		  res.format({
			   
			   'application/json': function(){
				  res.send({ status:'Failure',response: 'Enter All Mandatory Fields' });
			   }
			});
	  }
		
	});
	

	//Price List API

	settingsRouter.post('/price_list', function (req, res) {

		var work_id = req.body.work_id;
		var work_category_id = req.body.work_category_id;
		var sql = 'select * from palma_price where work_id="'+work_id+'" and work_category_id = "'+work_category_id+'" and active_status="1"';
	  connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
				var cunt = results.length;
			 if(cunt!=0){
					var sub_serv = new Array;
					var price = results[0]['price'];
					var max_unit = results[0]['max_unit'];
					var perc = new Array;
					results.forEach(function(element){
						//var price =element.price;
						//var max_unit =element.max_unit;
						var off = element.offer;
						var json_off = JSON.parse(off);
						var l = json_off.items.length;
						
						var c = 0;
						for(var i=0; i<l ; i++ ){
							//var count = 'Count'+i+'';
							//var nn = 'Count'+(++c)+':';
							var percentage = json_off.items[i];
							if(percentage == ""){
								percentage = '0';
							}
							//var con = nn + tt;
							//console.log(con);
							perc.push({percentage});
						}
						
						
						//sub_serv.push({price,max_unit,perc});
					 });

					 res.format({
						'application/json': function(){
						res.send({ status:'success',response: {'Price': price,'Max_count' : max_unit,'Offers': perc}});
							
						}
					});
				
			 }else{
				 res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'No Records Found' });
					   }
					});
			 }
		});
	});
	
	//rest api to delete record from mysql database
	/* customerRouter.post('/customer_delete', function (req, res) {
	   db.query('DELETE FROM `customer_register` WHERE `id`=?', [req.body.id], function (error, results, fields) {
		  if (error) throw error;
		  res.format({
			   
			   'application/json': function(){
				  res.send({ status:'success',response: 'Successfully Deleted' });
			   }
			});
		  //res.end('Record has been deleted!');
		});
	}); */
	
	//State List

	settingsRouter.post('/state_list', function (req, res) {

		
		var start = req.body.start;
		var end = req.body.end;
		var search = req.body.name;
		if(search == ''){
			var sql = 'select state_name,id from state where active_status="1" LIMIT '+start+','+end;
		}else{
			var sql = 'select state_name,id from state where active_status="1" and state_name like "%'+search+'%" order by id asc LIMIT '+start+','+end;
		}
	  connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
				var cunt = results.length;
				var sub_serv = new Array;
			 if(cunt!=0){
				
					results.forEach(function(element){
						var state_name =element.state_name;
						var state_id =element.id;
						sub_serv.push({state_id,state_name});
					 });

					 res.format({
						'application/json': function(){
						res.send({ status:'success',response: sub_serv});
							
						}
					});
				
			 }else{
				 res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'No Records Found' });
					   }
					});
			 }
		});
	});

	//City List

	settingsRouter.post('/city_list', function (req, res) {

		var state_id = req.body.state_id;
		var start = req.body.start;
		var end = req.body.end;
		var search = req.body.name;

		if(search == ''){
			var sql = 'select city,id from city where stateid = "'+state_id+'" and active_status="1" LIMIT '+start+','+end;
		}else{
			var sql = 'select city,id from city where stateid = "'+state_id+'" and active_status="1" and city like "%'+search+'%" LIMIT '+start+','+end;
		}

		
	  	connection.query(sql, function (error, results, fields) {
		  if (error) throw error;
				var cunt = results.length;
				var cities = new Array;
			 if(cunt!=0){
				
					results.forEach(function(element){
						var city =element.city;
						var city_id =element.id;
						cities.push({city_id,city});
					 });

					 res.format({
						'application/json': function(){
						res.send({ status:'success',response: {'State_id' : state_id,cities}});
							
						}
					});
				
			 }else{
				 res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'No Records Found' });
					   }
					});
			 }
		});
	});
	
	settingsRouter.post('/book', function (req, res) {
	   var customer_id  = req.body.customer_id;
	   var brand_id  = req.body.brand_id;
	   var brand_type_id  = req.body.brand_type_id;
	   var work_id  = req.body.work_id;
	   var job_id  = req.body.job_id;
	   var description  = req.body.description;
	   var created_dt = mydate('date');
	   var created_time = mydate('time');
	   var date_time = created_dt+' '+created_time;
	   var image_url = req.body.image_url;
	   var service_company_id = req.body.service_company_id;
	   var active = req.body.active;
	   
		var values =[customer_id,brand_id,brand_type_id,work_id,job_id,description,date_time,image_url,service_company_id,active];
		
	   connection.query('INSERT INTO service_booking (customer_id, brand_id, brand_type_id, work_id, job_id, description, date_time, image, service_company_id, status) VALUES ?', [[values]], function (error, results, fields) {
		  if (error) throw error;
		  res.format({
			   
			   'application/json': function(){
				  res.send({ status:'success',response: 'Service Booked Successfully' });
			   }
			});
		});
		
	});
	
	settingsRouter.post('/check', function (req, res) {
		var email = req.body.email;
		
		if(email != ''){
		   var qr = connection.query('select * from customer_register where email=?', [email],function (error, results, fields) {
			   var num = results.length;
			   
			  if (error) throw error;
			  
			  if(num != 0){
				  res.format({
					   
					   'application/json': function(){
						  res.send({ status:'Failure',response: 'Email Already Exists!' });
					   }
					});
				 
			  }else{
				   res.format({
					   
					   'application/json': function(){
						  res.send({ status:'success',response: 'Welcome' });
					   }
					});
			  }
			});
			
		}
	});
	
}

