var connection = require('./db');
var path = require('path');

module.exports = function(dashboardRouter){

    dashboardRouter.post('/member_dashboard',function(req,res){
        var service_company_id  = req.body.service_company_id;
        var sql = 'select count(*) as tot, sum(CASE WHEN status="Completed" THEN 1 ELSE 0 END) as com, sum(CASE WHEN status="Cancel" THEN 1 ELSE 0 END) as canc,sum(CASE WHEN status="Pending" THEN 1 ELSE 0 END) as pend, sum(CASE WHEN status="Processing" THEN 1 ELSE 0 END) as process,sum(CASE WHEN status="Ongoing" THEN 1 ELSE 0 END) as ongoing from service_booking where service_company_id="'+service_company_id+'"';
        connection.query(sql, function(er,jobs,fields){
            if(jobs.length !='0'){
                var num_jobs = jobs[0]['tot'];
                var num_com_job;
                var num_can_job;
                var num_pend_job;
                var num_process_job;
                var num_ongoing_job;

                if(jobs[0]['com'] == null){
                    num_com_job = 0;
                }else{
                    num_com_job = jobs[0]['com'];
                }
                
                if(jobs[0]['canc'] == null){
                    num_can_job =0;
                }else{
                    num_can_job =jobs[0]['canc'];
                }
                
                if(jobs[0]['pend'] == null){
                    num_pend_job =0;
                }else{
                    num_pend_job =jobs[0]['pend'];
                }

                if(jobs[0]['process'] == null){
                    num_process_job =0;
                }else{
                    num_process_job =jobs[0]['process'];
                }

                if(jobs[0]['ongoing'] == null){
                    num_ongoing_job =0;
                }else{
                    num_ongoing_job =jobs[0]['ongoing'];
                }
                                                          
                res.format({
                    'application/json': function(){
                        res.send({status:'Success',response:{total_jobs:num_jobs,completed_job: num_com_job,cancel_job: num_can_job,pending_job : num_pend_job,num_process_job : num_process_job,num_ongoing_job : num_ongoing_job}});
                    }
                });
            }
            
        });
    });
}