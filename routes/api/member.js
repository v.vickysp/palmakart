var db = require('./db');
var mydate = require('current-date');
var dateFormat = require('dateformat');
var NodeGeocoder = require('node-geocoder');
var options = {
	provider: 'google',
	httpAdapter: 'https', // Default
	//apiKey: 'YOUR_API_KEY', // for Mapquest, OpenCage, Google Premier
	formatter: null 
  };
  var geocoder = NodeGeocoder(options);
  
//rest api to get all customers
module.exports = function(memberRouter){
	
	//API For Member Login
	memberRouter.post('/login', function (req, res) {
		var email = req.body.email;
		var pass = req.body.password;
		
		var sql_sel = "select service_company_register.*,state.state_name,state.id as state_id,city.city,city.id as city_id from service_company_register inner join state on service_company_register.state=state.id inner join city on service_company_register.city=city.id where service_company_register.email='"+email+"' and password='"+pass+"'";
		db.query(sql_sel,[email,pass], function (error, results, fields) {

				var cunt = results.length;
				
				if(cunt!=0){
					var id = results[0]['id'];
					var user_id = results[0]['user_id'];
					var type= results[0]['type'];
					var company_name = results[0]['company_name'];
					var contact_name = results[0]['contact_name'];
					var address = results[0]['address'];
					var pincode = results[0]['pincode'];
					var city = results[0]['city'];
					var city_id = results[0]['city_id'];
					var state = results[0]['state_name'];
					var state_id = results[0]['state_id'];
					var contact_number = results[0]['contact_number'];
					var mobile_number =  results[0]['mobile_number'];
					var email = results[0]['email'];
					var password = results[0]['password'];

					var address_proof = results[0]['address_proof'];
					var id_proof = results[0]['id_proof'];
					var profile_image = results[0]['profile_image'];

					var bank_name = results[0]['bank_name'];
					var account_number = results[0]['account_number'];
					var branch_name = results[0]['branch_name'];
					var cheque_name = results[0]['cheque_name'];
					var ifsc_code = results[0]['ifsc_code'];
					
					var experience = results[0]['experience'];
					var description = results[0]['description'];
					var jobs = results[0]['job_name'];
					var jobs_json = JSON.parse(jobs);
					var job_sel = "select * from service_subcategory where id IN ("+jobs_json+")";
					db.query(job_sel,function(errors,job_na,fields){
						var job_name;
						var job_id;
						var jobarray = new Array;
						for(var i = 0;i<job_na.length;i++){
							job_name = job_na[i]['subcategory'];
							job_id = job_na[i]['id'];
							jobarray.push({job_name,job_id});
						}
						res.format({
						
							'application/json': function(){
								res.send({ status:'success',response: {"id" : id,"user_id" : user_id,"type" : type,"company_name" : company_name,"contact_name" : contact_name,"address" : address,"pincode" : pincode,"city" : city,"city_id" : city_id,"state" : state,"state_id" : state_id,"contact_number" : contact_number,"mobile_number" : mobile_number,"email" : email,"password" : password,"address_proof" : address_proof,"id_proof" : id_proof,"profile_image" : profile_image,"bank_name" : bank_name,"branch_name" : branch_name,"account_number" : account_number,"cheque_name" : cheque_name,"ifsc_code" : ifsc_code,"experience" : experience,"description" : description,"jobs" : jobarray} });
							}
						});
					});
				}else{
					res.format({
						
						'application/json': function(){
							res.send({ status:'Failure',response: 'Invalid Username & Password' });
						}
					});
				}
		});
		
	});
	
	//API For Update Member Profile
	memberRouter.post('/update_profile',function(req,res){
		var type = req.body.type;
		var id = req.body.id;
		
		//Personal Profile
		if(type == 'Company'){
			var company_name  = req.body.company_name;
			var contact_name	 = req.body.contact_name;
		}else{
			var company_name = req.body.person_name;
			var contact_name = '';
		}

	   
	   var address		 = "'"+req.body.address+"'";
	   var pincode		 = req.body.pincode;
	   var city			 = req.body.city;
	   var state		 = req.body.state;
	   var mobile_number = req.body.mobile_number;
	   var contact_number = req.body.contact_number;
	   
	   //Account Details
	   var jobs		= req.body.jobs;
	   var experience	= req.body.experience;
	   var description	= req.body.description;

		var updated_dt = mydate('date');
		var updated_time = mydate('time');
		var updated_date = updated_dt+' '+updated_time;
		
		var job_json = JSON.stringify([jobs]);

		geocoder.geocode(address, function(err, respond) {
			
			var lattitude = respond[0]['latitude'];
			var longitude = respond[0]['longitude'];
		
			if(id != ''){
				var up_sql = "update service_company_register set type='"+type+"',company_name='"+company_name+"',contact_name='"+contact_name+"',address="+address+",pincode='"+pincode+"',city='"+city+"',state='"+state+"',contact_number='"+contact_number+"',mobile_number='"+mobile_number+"',experience='"+experience+"',job_name='"+job_json+"',description='"+description+"',updated_date='"+updated_dt+"',updated_date_time='"+updated_date+"',latitude='"+lattitude+"',longitude='"+longitude+"' where id='"+id+"'";
				db.query(up_sql, function (error, results, fields) {
				if (error) throw error;
						res.format({
							'application/json': function(){
							res.send({ status:'success',response: 'Successfully Updated' });
							}
						});
				});
			}else{
				res.format({
					'application/json': function(){
					res.send({ status:'Failure',response: 'Failed To Update' });
					}
				});
			}
		});
	});
	
	//rest api to create a new Member record into mysql database
	memberRouter.post('/new', function (req, res) {

		var type = req.body.type;
		//Personal Profile
		if(type == 'Company'){
			var company_name  = req.body.company_name;
			var contact_name	 = req.body.contact_name;
			var profile_image = '';
			 //Employee Details
			var employee_details = req.body.employee_details;
		}else{
			var company_name = req.body.person_name;
			var contact_name = '';
			var profile_image = req.body.profile_image;
		}
	  
	   var email		 = req.body.email;
	   var password 	 = req.body.password;
	   var address		 = req.body.address;
	   var pincode		 = req.body.pincode;
	   var city			 = req.body.city;
	   var state		 = req.body.state;
	   var mobile_number = req.body.mobile_number;
	   var contact_number = req.body.contact_number;
	   
	   //Account Details
	   var jobs		= req.body.jobs;
	   var experience	= req.body.experience;
	   var description	= req.body.description;
	   var cheque_name	 = req.body.cheque_name;
	   var bank_name	 = req.body.bank_name;
	   var account_number = req.body.account_number;
	   var branch_name	 = req.body.branch_name;
	   var ifsc_code	= req.body.ifsc_code;

	   //Proof Details
	   var address_proof = req.body.address_proof;
	   var id_proof		 = req.body.id_proof;

		var created_dt = mydate('date');
		var created_time = mydate('time');
		var created_date = created_dt+' '+created_time;
		//var job_json = new Array();
		//job_json.push(jobs);
		var job_json = JSON.stringify([jobs]);
		
		
		var sel_company = "select user_id from service_company_register order by id desc LIMIT 1";
		db.query(sel_company,function(errors,sel_com,fields){
			var user_unique_id;
			if(sel_com.length == 0){
				user_unique_id = 'PSCOMP'+1;
			}else{
				var job_auto = sel_com[0]['user_id'];
				var left_text = job_auto.slice(6);
				
				var auto_id = Number(left_text) + 1;
				user_unique_id = 'PSCOMP'+auto_id;
			}
			
			
			geocoder.geocode(address, function(err, ress) {
				
				var lattitude = ress[0]['latitude'];
				var longitude = ress[0]['longitude'];
				var values =[user_unique_id,type,company_name,contact_name,address,pincode,city,state,contact_number,password,mobile_number,email,address_proof,id_proof,profile_image,created_date,cheque_name,bank_name,account_number,branch_name,job_json,experience,description];
				db.query('INSERT INTO service_company_register (user_id,type,company_name, contact_name, address, pincode, city, state, contact_number, password, mobile_number, email, address_proof, id_proof, profile_image,created_date, cheque_name, bank_name, account_number, branch_name, job_name, experience, description) VALUES ?', [[values]], function (error, results, fields) {
				if (error) throw error;
					var service_company_id = results.insertId;
					var i = 0;
					/*if(type=='Company'){
							var i =1;
						employee_details.forEach(function(ele){
							var employee_name = ele.employee_name;
							var employee_number = ele.employee_number;
							var staff_photo = ele.staff_photo;
							//For Auto Code
							var staff_unique_id;
							staff_unique_id = 'EMP'+(i++);
								//console.log(sql_sel);
								//For inserting employee details
								var emp_insrt = "insert into company_staff (service_company_id,staff_name,staff_unique_id,staff_mobile,staff_photo) values ('"+service_company_id+"','"+employee_name+"','"+staff_unique_id+"','"+employee_number+"','"+staff_photo+"')";
								db.query(emp_insrt,function(errors,rows,fields){

								});
								
						});
					}*/
					if(results){
						res.format({
							'application/json': function(){
							res.send({ status:'success',response: 'Successfully Registered' });
							}
						});
					}else{
						res.format({
							'application/json': function(){
							res.send({ status:'Failure',response: 'Failed To Register' });
							}
						});
					}
					
				});
			});
		}); 
	});
	
	
	// API for Email Check

	memberRouter.post('/check',function(req,res){
		var email = req.body.email;
				
		if(email != ''){
			db.query('select * from service_company_register where email=?', [email],function (error, results, fields) {
				var num = results.length;
				if (error) throw error;
				if(num != 0){
					res.format({
						'application/json': function(){
							res.send({ status:'Failure',response: 'Email Already Exists!' });
						}
					});
					
				}else{
					res.format({
						'application/json': function(){
							res.send({ status:'success',response: 'Welcome' });
						}
					});
				}
			});
		}
	});

	// API for Change Password
	memberRouter.post('/change_password',function(req,res){
		var old_password = req.body.old_password;
		var company_id = req.body.company_id;
		var new_password = req.body.new_password;
		var sql_old_password = "select password,id from service_company_register where id='"+company_id+"'";
		console.log(sql_old_password);
		db.query(sql_old_password,function(errors,results,fields){
			if(results[0]['password'] == old_password){
				var sql_password = "update service_company_register set password='"+new_password+"' where id='"+company_id+"'";
				db.query(sql_password,function(errs,rows,fields){
					if(rows){
						res.format({
							'application/json': function(){
								res.send({status: 'Success',response: 'Password Changed Successfully'});
							}
						});
					}
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({status: 'Failure',response: 'Your Old Password Is Incorrect'});
					}
				});
			}
		});
	});

	//Forgot Password
	memberRouter.post('/forgot_password', function (req, res) {
		var email  = req.body.email;
		var sq = "select password from service_company_register where email='"+email+"'";
		db.query(sq,function(err,rows,fields){
			var pass = rows[0]['password'];
			const sendmail = require('sendmail')();
			sendmail({
				from: 'no-reply@yourdomain.com',
				to: email,
				subject: 'Regarding Password Recovery',
				html: pass,
			}, function(err, reply) {
				if(err){
					console.log(err && err.stack);
				}else{
				
					res.format({
						
						'application/json': function(){
						res.send({ status:'Success',response: 'Mail Sent' });
						}
					});
				}
			});
			
		});
	});

	//For Order History Loop

	function getDriver(employee_id,callback) { 
	 
		var sub = "select staff_name,staff_photo,staff_mobile,staff_unique_id from company_staff where id='"+employee_id+"'";
						 var qr = db.query(sub,function(err,rows,fields){
							 var sub_cnt = rows.length;
							 
							 if(sub_cnt != 0){
								callback(err, rows); 
								
							 }
						 });
	}
	var row = new Array;
	function dis(row2,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,customer_name,customer_mobile,type,company_name,company_user_id,city_id,city,state_id,state,booking_date,cunt,i,res){
		
		row.push({'job_no' : job_no,'work_date' : work_date,'work_time' : work_time,'work_address' : work_address,'max_count' : max_count,'offer' : offer,'offer_amount' : offer_amount,'total_amount' : total_amount,'status' : status,'sub_total' : sub_total,'payment_status' : payment_status,'payment_type' : payment_type,'work_name' : work_name,'work_category' : work_category,'customer_name' : customer_name,'customer_mobile' : customer_mobile,'type' : type,'company_name' : company_name,'company_user_id': company_user_id,'city_id' : city_id,'city' : city,'state_id' : state_id,'state' : state,'booking_date' : booking_date,'staff_details' : row2});
		
		if(cunt - 1 == i){
			res.format({
				'application/json': function(){
					res.send({ status:'success',response: {row} });
				}
			});
			row = [];
		}
	}

	//Job history API For Member
	memberRouter.post('/order_history',function(req,res){
		var service_company_id = req.body.service_company_id;
		var status = req.body.status;
		var search = req.body.search;
		if(search != ''){
			var sql = "SELECT service_booking.job_no,service_booking.work_date,service_booking.date_time,service_booking.work_time,service_booking.work_address,service_booking.max_count,service_booking.offer,service_booking.offer_amount,service_booking.total_amount,service_booking.status,service_booking.sub_total,service_booking.payment_status,service_booking.payment_type,work_category.work_category,customer_register.customer_name,customer_register.mobile_number,service_company_register.company_name,service_company_register.user_id,service_company_register.type,service_booking.service_company_id,service_booking.customer_id,service_booking.employee_id,city.city,city.id as city_id,state.state_name,state.id as state_id,palma_work.work_name FROM service_booking INNER JOIN customer_register ON service_booking.customer_id = customer_register.id INNER JOIN palma_work ON palma_work.id=service_booking.work_id INNER JOIN service_company_register ON service_company_register.id=service_booking.service_company_id INNER JOIN city ON city.id = service_booking.city INNER JOIN state ON state.id = service_booking.state INNER JOIN work_category ON service_booking.work_category_id = work_category.id and service_booking.service_company_id = '"+service_company_id+"' and service_booking.status='"+status+"' and service_booking.employee_id='"+search+"' order by service_booking.id desc";
		}else{
			var sql = "SELECT service_booking.job_no,service_booking.work_date,service_booking.date_time,service_booking.work_time,service_booking.work_address,service_booking.max_count,service_booking.offer,service_booking.offer_amount,service_booking.total_amount,service_booking.status,service_booking.sub_total,service_booking.payment_status,service_booking.payment_type,work_category.work_category,customer_register.customer_name,customer_register.mobile_number,service_company_register.type,service_company_register.company_name,service_company_register.user_id,service_booking.service_company_id,service_booking.customer_id,service_booking.employee_id,city.city,city.id as city_id,state.state_name,state.id as state_id,palma_work.work_name FROM service_booking INNER JOIN customer_register ON service_booking.customer_id = customer_register.id INNER JOIN palma_work ON palma_work.id=service_booking.work_id INNER JOIN service_company_register ON service_company_register.id=service_booking.service_company_id INNER JOIN city ON city.id = service_booking.city INNER JOIN state ON state.id = service_booking.state INNER JOIN work_category ON service_booking.work_category_id = work_category.id and service_booking.service_company_id = '"+service_company_id+"' and service_booking.status='"+status+"' order by service_booking.id desc";
		}
		
		
		db.query(sql, function (error, results, fields) {
			if (error) throw error;
				var cunt = results.length;
			if(cunt!=0){
				var i=0;
				results.forEach(function(element){

					var employee_id =element.employee_id;

					var job_no = element.job_no;
					var work_date = element.work_date;
					var work_time = element.work_time;
					var work_address = element.work_address;
					var max_count = element.max_count;
					var offer	= element.offer;
					var offer_amount = element.offer_amount;
					var total_amount = element.total_amount;
					var status = element.status;
					var sub_total = element.sub_total;
					var payment_status = element.payment_status;
					var payment_type = element.payment_type;
					var work_name = element.work_name;
					var work_category = element.work_category;
					var customer_name =element.customer_name;
					var company_name = element.company_name;
					var company_user_id = element.user_id;
					var customer_mobile = element.mobile_number;
					var type 		= element.type;
					var city_id		= element.city_id;
					var city		= element.city;
					var state_id	= element.state_id;
					var state		= element.state_name;
					var booking_date	= dateFormat(element.date_time,'dd-mm-yyyy');
					if(employee_id != "0"){
						
						getDriver(employee_id,function(err, driverRsult){
							var row2 = new Array;
							driverRsult.forEach(function(ele){
								var staff_name = ele.staff_name;
								var staff_photo = ele.staff_photo;
								var mobile_number = ele.staff_mobile;
								var staff_unique_id = ele.staff_unique_id;

								row2.push({staff_name,staff_photo,mobile_number,staff_unique_id});
							});
							dis(row2,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,customer_name,customer_mobile,type,company_name,company_user_id,city_id,city,state_id,state,booking_date,cunt,i,res);
							i++;
						});
					}else{
						var row2 = [];
						var job_no = element.job_no;
						var work_date = element.work_date;
						var work_time = element.work_time;
						var work_address = element.work_address;
						var max_count = element.max_count;
						var offer	= element.offer;
						var offer_amount = element.offer_amount;
						var total_amount = element.total_amount;
						var status = element.status;
						var sub_total = element.sub_total;
						var payment_status = element.payment_status;
						var payment_type = element.payment_type;
						var work_name = element.work_name;
						var work_category = element.work_category;
						var customer_name =element.customer_name;
						var company_name = element.company_name;
						var company_user_id = element.user_id;
						var customer_mobile = element.mobile_number;
						var type 		= element.type;
						var city_id		= element.city_id;
						var city		= element.city;
						var state_id	= element.state_id;
						var state		= element.state_name;
						var booking_date	= dateFormat(element.date_time,'dd-mm-yyyy');

						dis(row2,job_no,work_date,work_time,work_address,max_count,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_type,work_name,work_category,customer_name,customer_mobile,type,company_name,company_user_id,city_id,city,state_id,state,booking_date,cunt,i,res);
							i++;
					}
				});
				
					
			}else{
				res.format({
					'application/json': function(){
						res.send({ status:'Failure',response: 'No Records Found' });
					}
				});
			}
		});
	});

	// Amount History API For Member
	memberRouter.post('/amount_history',function(req,res){
		var from_date			= req.body.from_date;
		var to_date				= req.body.to_date;
		var service_company_id	= req.body.service_company_id;
		var today_date = '';
		
		if(from_date != '' && to_date != ''){
			var sel_history = "select * from member_wallet where create_date between '"+from_date+"' and '"+to_date+"' and service_company_id = '"+service_company_id+"'";
		}else{
			today_date = mydate('date');
			var sel_history = "select count(*) as total_job,sum(CASE WHEN type='Cash' THEN commission_to_pay ELSE 0 END) as cash_commission, sum(CASE WHEN type='Online' THEN commission_to_get ELSE 0 END) as online_commission,sum(CASE WHEN type = 'Online' THEN paid_amount ELSE 0 END) as online_pay,sum(CASE WHEN type = 'Cash' THEN paid_amount ELSE 0 END) as cash_pay from member_wallet where create_date='"+today_date+"' and service_company_id='"+service_company_id+"'";
		}

		db.query(sel_history,function(errors,history_res,fields){
			
			if(history_res.length != '0'){
				
				if(today_date == ''){
					//var total_jobs = history_res.length;
					var his_arry = new Array;
					var overall_amount = 0;
					history_res.forEach(function(element){
						
						var type	= element.type;
						if(type == 'Cash'){
							var cash_pay = element.paid_amount; 
							var online_pay = 0;
							var online_commission = 0;
							var cash_commission = element.commission_to_pay;
						}else{
							var online_pay	= element.paid_amount;
							var cash_pay = 0;
							var online_commission = element.commission_to_get;
							var cash_commission = 0;
						}
						var total_amount		= cash_pay + online_pay;
						var create_date			= dateFormat(element.create_date,'dd-mm-yyyy');
						//var total_jobs			= element.total_job;
						var online_commission	= online_commission;
						var cash_commission		= cash_commission;
						var online_pay			= online_pay;
						var cash_pay			= cash_pay;
						overall_amount			= overall_amount + total_amount;
						his_arry.push({create_date,total_amount,type,online_commission,cash_commission,online_pay,cash_pay});
					});
				}else{
					var his_arry = new Array;
					var overall_amount = 0;
					var type = '';
					history_res.forEach(function(element){
						var total_amount		= element.cash_pay + element.online_pay;
						var total_jobs			= element.total_job;
						var online_commission	= element.online_commission;
						var cash_commission		= element.cash_commission;
						var online_pay			= element.online_pay;
						var cash_pay			= element.cash_pay;
						var create_date			= today_date;
						overall_amount			= overall_amount + total_amount;
						his_arry.push({create_date,total_amount,type,total_jobs,online_commission,cash_commission,online_pay,cash_pay});
					});
				}
				
				
				
				res.format({
					'application/json' : function(){
						res.send({status: 'Success',response: {overall_amount,his_arry}});
					}
				});
			}else{
				res.format({
					'application/json' : function(){
						res.send({status: 'Failure',response: 'No Records Availbale'});
					}
				});
			}
		});
	});

	//Job List API
	memberRouter.post('/jobs_list',function(req,res){
		var sel_sub	= "select subcategory,id from service_subcategory where active_status='1' order by id desc";
		db.query(sel_sub,function(errors,results,fields){
			if(results.length != '0'){
				res.format({
					'application/json': function(){
						res.send({status: 'Success',response: results});
					}
				});
			}else{
				res.format({
					'application/json': function(){
						res.send({status: 'Failure',response: 'No Records Available'});
					}
				});
			}
		});
	});



	memberRouter.post('/service_list', function (req, res) {
		var member_id = req.body.member_id;
		var customer_id = req.body.customer_id;
		var status = req.body.status;
		var start = req.body.start;
		var end = req.body.end;
		if(member_id != '' && status != '' && customer_id == ''){
		   db.query('select * from service_booking where status=? and service_company_id=? LIMIT ?,?',[status,member_id,start,end], function (error, results, fields) {
			  if (error) throw error;
			  var len = results.length;
				  
				  if(len!=0){
					  res.format({
						   
						   'application/json': function(){
							  res.send({ status:'success',response: results });
						   }
						});
				  }else{
					  res.format({
						   
						   'application/json': function(){
							  res.send({ status:'Failure',response: 'No Records Found' });
						   }
						});
				  }
			});
			
		}else if(customer_id != '' && status != '' && member_id == ''){
			db.query('select * from service_booking where status=? and customer_id=? LIMIT ?,?',[status,customer_id,start,end], function (error, results, fields) {
			  if (error) throw error;
			  var len = results.length;
				  
				  if(len!=0){
					  res.format({
						   
						   'application/json': function(){
							  res.send({ status:'success',response: results });
						   }
						});
				  }else{
					  res.format({
						   
						   'application/json': function(){
							  res.send({ status:'Failure',response: 'No Records Found' });
						   }
						});
				  }
			});
		}
	});
	
	
}