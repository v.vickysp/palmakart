var db = require('./db');
var mydate = require('current-date');
module.exports = function(employeerRouter){

    // Employee List API
    employeerRouter.post('/employee_list',function(req,res){
        var service_company_id = req.body.service_company_id;
        var start = req.body.start;
        var end = req.body.end;
        var search = req.body.search;
        var sel_employee;
        if(search == ''){
            sel_employee = "select * from company_staff where service_company_id='"+service_company_id+"' LIMIT "+start+","+end;
        }else{
            sel_employee = "select * from company_staff where service_company_id='"+service_company_id+"' and staff_name like '%"+search+"%' LIMIT "+start+","+end+"";
        }
        
        db.query(sel_employee,function(errors,results,fields){
            if(results.length){
                var employee_array = new Array;
                results.forEach(function(element){
                    var staff_id        = element.id;
                    var staff_name      = element.staff_name;
                    var staff_unique_id = element.staff_unique_id;
                    var staff_mobile    = element.staff_mobile;
                    var staff_photo     = element.staff_photo;
                    if(element.active_status == '1'){
                        var status = 'Enable';
                    }else{
                        var status = 'Disable';
                    }
                    employee_array.push({staff_id,staff_name,staff_unique_id,staff_mobile,staff_photo,status});
                    
                });
                res.format({
                    'application/json':function(){
                        res.send({status:'Success',response: employee_array});
                    }
                });
            }else{
                res.format({
                    'application/json':function(){
                        res.send({status:'Failure',response: 'No Records Found'});
                    }
                });
            }
        });
    });

    //Employee Add API
    employeerRouter.post('/employee_add',function(req,res){
        var service_company_id  = req.body.service_company_id;
        var staff_name          = req.body.staff_name;
        var staff_mobile        = req.body.staff_mobile;
        var staff_photo         = req.body.staff_photo;
        var sel_employee = "select staff_unique_id from company_staff where service_company_id='"+service_company_id+"' order by id desc LIMIT 1";

        db.query(sel_employee,function(errors,results,fields){
            if(results.length == 0){
                var staff_unique_id = "EMP"+1;
            }else{
                var staff_auto      = results[0]['staff_unique_id'];
                var left_text       = staff_auto.slice(3);
                var auto_id         = Number(left_text) + 1;
                var staff_unique_id = "EMP"+auto_id;
            }

            var emp_insert = "insert into company_Staff (service_company_id,staff_name,staff_unique_id,staff_mobile,staff_photo) values ('"+service_company_id+"','"+staff_name+"','"+staff_unique_id+"','"+staff_mobile+"','"+staff_photo+"')";
            db.query(emp_insert,function(errors,rows,fields){
                
                if(rows.affectedRows == '1'){
                    res.format({
                        'application/json': function(){
                            res.send({status: 'Success',response: 'Employee Added Successfully'});
                        }
                    });
                }else{
                    res.format({
                        'application/json': function(){
                            res.send({status: 'Failure',response: 'Failed to add'});
                        }
                    });
                }
            });
        }); 
    });

   //Employee Mobile Check
   employeerRouter.post('/mobile_check',function(req,res){
       var mobile_number        = req.body.mobile_number;

       var mobile_check = "select staff_mobile from company_staff where staff_mobile='"+mobile_number+"'";
       var qr = db.query(mobile_check,function(errors,results,fields){
           if(results.length > 0){
                res.format({
                    'application/json': function(){
                        res.send({status: 'Failure',response: 'Mobile Number Already Exists'});
                    }
                });
           }else{
                res.format({
                    'application/json': function(){
                        res.send({status: 'Success',response: 'Welcome'});
                    }
                });
           }
       });
       
   });

    //Employee Edit API
    employeerRouter.post('/employee_edit',function(req,res){
        var service_company_id  = req.body.service_company_id;
        var staff_name          = req.body.staff_name;
        var staff_unique_id     = req.body.staff_unique_id;
        var staff_photo         = req.body.staff_photo;
        var staff_mobile        = req.body.staff_mobile;

        var up_employee = "update company_staff set staff_name='"+staff_name+"',staff_mobile='"+staff_mobile+"',staff_photo='"+staff_photo+"' where service_company_id='"+service_company_id+"' and staff_unique_id='"+staff_unique_id+"'";
        db.query(up_employee,function(errors,results,fields){
            if(results.affectedRows == '1'){
                res.format({
                    'application/json': function(){
                        res.send({status: 'Success',response: 'Employee Updated Successfully'});
                    }
                });
            }else{
                res.format({
                    'application/json': function(){
                        res.send({status: 'Failure',response: 'Failed to update'});
                    }
                });
            }
        });
    });


    //Employee Active Or Deactive

    employeerRouter.post('/enable',function(req,res){
        var service_company_id  = req.body.service_company_id;
        var staff_unique_id     = req.body.staff_unique_id;
        var enable              = req.body.enable;

        if(enable == "1"){
            var sql_up = "update company_staff set active_status = '1' where service_company_id='"+service_company_id+"' and staff_unique_id = '"+staff_unique_id+"'";
            db.query(sql_up,function(errors,results,fields){
                if(results.affectedRows == '1'){
                    res.format({
                        'application/json': function(){
                            res.send({status: "Success",response: 'Enabled Succesfully'});
                        }
                    });
                }else{
                    res.format({
                        'application/json': function(){
                            res.send({status: "Failure",response: 'Failed To Enable'});
                        }
                    });
                }
            });
        }else{
            var sql_up = "update company_staff set active_status = '0' where service_company_id='"+service_company_id+"' and staff_unique_id = '"+staff_unique_id+"'";
            db.query(sql_up,function(errors,results,fields){
                if(results.affectedRows == '1'){
                    res.format({
                        'application/json': function(){
                            res.send({status: "Success",response: 'Disabled Successfully'});
                        }
                    });
                }else{
                    res.format({
                        'application/json': function(){
                            res.send({status: "Failure",response: 'Failed To Disable'});
                        }
                    });
                }
            });
        }

        
    });
}