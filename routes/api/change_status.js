var connection = require('./db');
var path = require('path');
var dateFormat = require('dateformat');
var mydate = require('current-date');

module.exports = function(changestatusRouter){

    //API For Status Change On Member Module
    changestatusRouter.post('/status_change',function(req,res){
        var booking_id      = req.body.booking_id;
        var status          = req.body.status;
        var payment_status  = req.body.payment_status;
        if(status == 'Ongoing'){
            var up_booking = "update service_booking set status='"+status+"' where job_no='"+booking_id+"'";
            connection.query(up_booking,function(err,sel_cus,fields){
                if(sel_cus){
                    res.format({
                        'application/json': function(){
                            res.send({status : 'Success',response : 'Status Updated Successfully'});
                        }
                    });
                }else{
                    res.format({
                        'application/json': function(){
                            res.send({status : 'Failure',response : 'Failed To Change'});
                        }
                    });
                }
                
            }); 
        }else if(status == 'Completed'){
            if(payment_status == 'Unpaid'){
                var up_booking = "update service_booking set status='"+status+"',payment_mode='Cash',payment_status='Paid' where job_no='"+booking_id+"'";
                connection.query(up_booking,function(errors,results,fields){

                    var sel_amount = "select sub_total,service_company_id,employee_id from service_booking where job_no='"+booking_id+"'";
                    connection.query(sel_amount,function(errors,sel_amnt,fields){
                        var total_amount            = sel_amnt[0]['sub_total'];
                        var service_company_id      = sel_amnt[0]['service_company_id'];
                        var employee_id      = sel_amnt[0]['employee_id'];
                        var transaction_date        = mydate('date');
                        var trans_time              = mydate('time');
                        var transaction_datetime    = transaction_date+' '+trans_time;

                        if(employee_id != '0'){
                            var sel_emp_name = "select service_company_register.company_name,service_company_register.user_id,company_staff.staff_name,company_staff.staff_unique_id from service_company_register INNER JOIN company_staff ON company_staff.service_company_id=service_company_register.id and company_staff.id = '"+employee_id+"' where service_company_register.id='"+service_company_id+"'";
                        }else{
                            var sel_emp_name = "select service_company_register.company_name,service_company_register.user_id from service_company_register where service_company_register.id='"+service_company_id+"'";
                        }

                        connection.query(sel_emp_name,function(errors,emp_res,fields){
                            if(employee_id != '0'){
                                var company_name    = emp_res[0]['company_name'];
                                var user_id         = emp_res[0]['user_id'];
                                var staff_name      = emp_res[0]['staff_name'];
                                var staff_unique_id = emp_res[0]['staff_unique_id'];
                            }else{
                                var company_name    = emp_res[0]['company_name'];
                                var user_id         = emp_res[0]['user_id'];
                                var staff_name      = '';
                                var staff_unique_id = '';
                            }

                            var sel_commission = "select * from commission order by id desc LIMIT 1";
                            connection.query(sel_commission,function(errors,commission_res,fields){
                                var commission_amount = commission_res[0]['commission_amount'];
                                var commission_to_pay = total_amount * (commission_amount/100);

                                var ins_payment = "insert into payment_transaction (booking_id,transaction_date,transaction_datetime,payment_mode,total_amount,paid_amount) values ('"+booking_id+"','"+transaction_date+"','"+transaction_datetime+"','Cash','"+total_amount+"','"+total_amount+"')";
                                    connection.query(ins_payment,function(errors,rows,fields){

                                        var ins_member_wallet = "insert into member_wallet (booking_id,service_company_id,company_name,user_id,staff_name,staff_id,type,create_date,paid_amount,commission_to_pay,commission_to_get,date_time,job_status) values ('"+booking_id+"','"+service_company_id+"','"+company_name+"','"+user_id+"','"+staff_name+"','"+staff_unique_id+"','Cash','"+transaction_date+"','"+total_amount+"','"+commission_to_pay+"','','"+transaction_datetime+"','Completed')";
                                        connection.query(ins_member_wallet,function(errors,mem_res,fields){
                                            if(mem_res){
                                                res.format({
                                                    'application/json': function(){
                                                        res.send({status : 'Success',response : 'Status Updated Successfully'});
                                                    }
                                                });
                                            }else{
                                                res.format({
                                                    'application/json': function(){
                                                        res.send({status : 'Failure',response : 'Failed To Change'});
                                                    }
                                                });
                                            }
                                        });
                                        
                                    });
                            });
                        });
                    });
                });
            }else{
                var up_booking = "update service_booking set status='"+status+"' where job_no='"+booking_id+"'";
                connection.query(up_booking,function(errors,results,fields){

                    var sel_amount = "select sub_total,service_company_id from service_booking where job_no='"+booking_id+"' and payment_status = 'Paid' and payment_mode='Online'";
                    connection.query(sel_amount,function(errors,sel_amnt,fields){
                        var total_amount            = sel_amnt[0]['sub_total'];
                        var service_company_id      = sel_amnt[0]['service_company_id'];
                        var transaction_date        = mydate('date');
                        var trans_time              = mydate('time');
                        var transaction_datetime    = transaction_date+' '+trans_time;
                        var sel_commission = "select * from commission order by id desc LIMIT 1";
                        connection.query(sel_commission,function(errors,commission_res,fields){
                            var commission_amount = commission_res[0]['commission_amount'];
                            var commission_to_get = total_amount * (commission_amount/100);
                            var ins_member_wallet = "insert into member_wallet (booking_id,service_company_id,type,create_date,paid_amount,commission_to_pay,commission_to_get,date_time) values ('"+booking_id+"','"+service_company_id+"','Online','"+transaction_date+"','"+total_amount+"','','"+commission_to_get+"','"+transaction_datetime+"')";
                            connection.query(ins_member_wallet,function(errors,mem_res,fields){
                                if(results){
                                    res.format({
                                        'application/json': function(){
                                            res.send({status : 'Success',response : 'Status Updated Successfully'});
                                        }
                                    });
                                }else{
                                    res.format({
                                        'application/json': function(){
                                            res.send({status : 'Failure',response : 'Failed To Change'});
                                        }
                                    });
                                }
                            });
                        });
                    }); 
                });
            }
        }
       
    });
}