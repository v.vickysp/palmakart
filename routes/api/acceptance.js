//Include Required Modules

//Module For Database Connections
var connection = require('./db');
var path = require('path');

//Module For Date Functions
var dateFormat = require('dateformat');
var mydate = require('current-date');

module.exports = function(acceptRouter){

    //Accept Or Cancel Job API For Member
    acceptRouter.post('/accept_cancel',function(req,res){
        var status              = req.body.status;
        var service_company_id  = req.body.service_company_id;
        var employee_id         = req.body.employee_id;
        var booking_id          = req.body.booking_id;

        if(status == '1'){
            var up_accept = "update service_company_register set accept_count=(@cur_value := accept_count) + 1 where id='"+service_company_id+"'";
            connection.query(up_accept,function(errors,results,fields){
                if(employee_id == ''){
                    employee_id = 0;
                }
                var up_booking = "update service_booking set service_company_id='"+service_company_id+"',employee_id='"+employee_id+"',status='Proccessing' where job_no='"+booking_id+"'";
                connection.query(up_booking,function(errs,rows,fields){
                    res.format({
                        'application/json': function(){
                            res.send({status: 'Success',response: 'Job Accepted Successfully'});
                        }
                    });
                });
            });
        }else{
            var up_cancel = "update service_company_register set cancel_count=(@cur_value := cancel_count) + 1 where id='"+service_company_id+"'";
            connection.query(up_cancel,function(errors,results,fields){
                var up_book = "update service_booking set service_company_id='0' where job_no='"+booking_id+"'";
                connection.query(up_book,function(errs,book_res,fields){

                    var cur_date        = mydate('date');
                    var cur_time        = mydate('time');
                    var cur_datetime    = cur_date+' '+cur_time;

                    var ins_cancel_details = "insert into cancel_history (booking_id,service_company_id,dated_at,dated_attime) values ('"+booking_id+"','"+service_company_id+"','"+cur_date+"','"+cur_datetime+"')";
                    connection.query(ins_cancel_details,function(errors,ins_his,fields){
                        /*res.format({
                            'application/json': function(){
                                res.send({status: 'Success',response: 'Job Cancelled Successfully'});
                            }

                        });*/
                        
                        res.redirect('http://192.168.1.8:3000/customer/allot_service',booking_id);
                    });  
                });
            });
        }
    });
}