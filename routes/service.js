var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Stratergy;
var session = require('express-session');
var User = require('../modules/users');
var connection = require('./db');
var path = require('path');
var multer = require('multer');
var bcrypt = require('bcryptjs');
var datatablesQuery = require('datatables-query');
var dateFormat = require('dateformat');
var mydate = require('current-date');
var DateDiff = require('date-diff');


//storage 

var storage =  multer.diskStorage({
	destination: './public/uploads/proofs',
	filename: function(req, file, cb){
		cb(null,file.fieldname+ '-' + Date.now() + path.extname(file.originalname));
	}
});

var storage1 =  multer.diskStorage({
	destination: './public/uploads/id_proof',
	filename: function(req, files, cb){
		cb(null,files.fieldname+ '-' + Date.now() + path.extname(files.originalname));
	}
});

//init uploads

var upload = multer({
	storage: storage
}).fields([{name:'address_proof'},{name:'id_proof'}]);


/* var upload1 = multer({
	storage1: storage1
}).single('id_proof'); */


//Member List

router.get('/member', function (req, res) {
	var sel_member = "select company_name,user_id,id from service_company_register order by id desc";
	connection.query(sel_member,function(errors,results,fields){
		res.render('member_list',{
			items: results
		});
	});
		
});


//Member Add Page

router.get('/member_add',function(req,res){
	connection.query('select state_name,id from state order by id desc',function(error,results,fields){
		connection.query('select job_name,id from palma_job where active_status=? order by id desc',[1],function(error,rows,fields){
		
			res.render('new_member',{
				items: results,
				jobs: rows
			});
		});
	});
	
});
// Auto id generation
router.post('/autoid',function(req,res){
	connection.query('select user_id from service_company_register order by id desc LIMIT 1',function(error,results,fields){
		var num = results.length;
		if(num == 0){
			var user_id = 'PS1000';
			res.send(user_id);
		}else{
			results.forEach(function(ele){
			
				var str = ele.user_id;
				var auto = str.split('S')[1];
				var auto_id= Number(auto)+1;
				var user_id = 'PS'+auto_id;
			
				res.send(user_id);
			});
		}
	});
	
});


// Mobile Check
router.post('/mobile_check',function(req,res){
	var mobile = req.body.mobile;
	//var c_mobile = req.body.c_mobile;
	connection.query('select contact_number,mobile_number from service_company_register where mobile_number=?',[mobile],function(error,results,fields){
		
		var len = results.length;
		if(len>0){
			var msg = '1';
		}else{
			var msg = '0';
		}
			res.send(msg);
			
	});
	
	
});

//Member Ajax Table Fetch

router.post('/member_ajax',function(req,res){

			params = req.body;
			var company_id = req.body.company_id;
			var from_date   = req.body.from_date;
			var to_date     = req.body.to_date;
			var where = '';
			
			//Filter Area
			if(company_id != '' && company_id != undefined){
				where += " and id='"+company_id+"'";
			}
			if(from_date != '' && to_date == '' && company_id == ''){
				where += " and created_date >= '"+from_date+"'";
			}
			if(from_date == '' && to_date != '' && company_id == ''){
				where += " and created_date <= '"+to_date+"'";
			}
			if(from_date != '' && to_date != '' && company_id == ''){
				where += " and created_date between '"+from_date+"' and '"+to_date+"'";
			}
			if(from_date != '' && to_date != '' && company_id != '' && from_date != undefined && to_date != undefined && company_id != undefined){
				where += " and created_date between '"+from_date+"' and '"+to_date+"' and id='"+company_id+"'";
			}
			//Filter Area Query Ends Here
			
			var start = params.start;
			var end = params.length;
			var val = start,end;
			if(where == ''){
				var sq = 'select * from service_company_register order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from service_company_register where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
	connection.query(sq,function (error, results, fields) {
			connection.query('select * from service_company_register order by id desc',function(error,main,fields){	
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				var i = start;
				var img;
				results.forEach(function(element) {
					var reg_date = dateFormat(element.created_date,'dd-mm-yyyy');
					var mod = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-eye fa-2x"></i></a>';
					
					mod += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Profile Details</h4></div><div class="modal-body inner-model"><div role="tabpanel"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#uploadTab'+element.id+'" aria-controls="uploadTab" role="tab" data-toggle="tab">View Profile</a></li><li role="presentation"><a href="#browseTabss'+element.id+'" aria-controls="browseTab" role="tab" data-toggle="tab">Proof Verification</a></li><li role="presentation"><a href="#accountdetails'+element.id+'" aria-controls="browseTab" role="tab" data-toggle="tab">Account Details</a></li><li role="presentation"><a href="#experiencedetails'+element.id+'" aria-controls="browseTab" role="tab" data-toggle="tab">Experience Details</a></li></ul><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="uploadTab'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
					
					
					mod+='<script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/service/get_profile",success: function(data) {var obj = JSON.parse(data);var cus_name = obj.company_name;var cus_email = obj.email;var cus_mobile = obj.mobile_number;var cus_pass = obj.password;var cus_addr = obj.address;var cus_city = obj.city_name;var cus_state = obj.state_name;var cus_pincode = obj.pincode;var contact_name = obj.contact_name;var reg_date = obj.reg_date;var bank_name=obj.bank_name;var branch_name = obj.branch_name;var account_number = obj.account_number;var cheque_name = obj.cheque_name;var job_name=obj.job_name;var experience = obj.experience;var description = obj.description;var known_jobs="";for(var k = 0; k < job_name.length; k++){known_jobs += job_name[k]["subcategory"]+",";}';
					
					mod+='$("#user_name'+element.id+'").html(cus_name);';
					
					mod+='$("#co_name'+element.id+'").html(contact_name);';
					
					mod+='$("#u_email'+element.id+'").html(cus_email);';
					
					mod+='$("#u_mobile'+element.id+'").html(cus_mobile);';
					
					mod+='$("#u_pass'+element.id+'").html(cus_pass);';
					
					mod+='$("#u_addr'+element.id+'").html(cus_addr);';
					
					mod+='$("#u_city'+element.id+'").html(cus_city);';
					
					mod+='$("#u_state'+element.id+'").html(cus_state);';
					
					mod+='$("#u_reg_date'+element.id+'").html(reg_date);';

					mod+='$("#user_name1'+element.id+'").html(cus_name);';
					
					mod+='$("#co_name1'+element.id+'").html(contact_name);';
					
					mod+='$("#u_email1'+element.id+'").html(cus_email);';

					mod+='$("#bank_name'+element.id+'").html(bank_name);';
					
					mod+='$("#cheque_name'+element.id+'").html(cheque_name);';
					
					mod+='$("#account_number'+element.id+'").html(account_number);';
					
					mod+='$("#branch_name'+element.id+'").html(branch_name);';

					mod+='$("#job_name'+element.id+'").html(known_jobs);';

					mod+='$("#job_name_det'+element.id+'").html(known_jobs);';
					
					mod+='$("#experience'+element.id+'").html(experience);';
					
					mod+='$("#description'+element.id+'").html(description);';
											
					mod+='$("#u_pincode'+element.id+'").html(cus_pincode);} });});</script>';
					
					mod+='<div class="row"><div class="row"><div class="col-lg-12"><h1 class="only-bottom-margin"><span id="user_name'+element.id+'"></span></h1></div></div><div class="row"><div class="col-lg-12"><div class="col-md-6"><label>Contact Name:</label><span id="co_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Email:</label> <span id="u_email'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Mobile:</label> <span id="u_mobile'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Password:</label> <span id="u_pass'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Address:</label> <span id="u_addr'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>City:</label> <span id="u_city'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>State:</label> <span id="u_state'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Pincode:</label> <span id="u_pincode'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Created Date:</label> <span id="u_reg_date'+element.id+'" class="text-muted"></span></div></div></div></div></div></div></div></div></div></div>';
					
					mod+='<div role="tabpanel" class="tab-pane" id="browseTabss'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
													
					mod+='<div class="row"><div class="row"><div class="col-md-12"><h1 class="only-bottom-margin"><span id="user_name1'+element.id+'"></span></h1></div></div><div class="row"><div class="col-md-12"><div class="col-md-6"><label>Contact Name:</label><span id="co_name1'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Known Jobs:</label><span id="job_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Email:</label> <span id="u_email1'+element.id+'" class="text-muted"></span></div></div></div></div>';
					
					mod+='<div class="row"><div class="col-md-12" style="margin-top: 43px;"><div class="col-md-6 text-center"><label>Address Proof</label><a data-toggle="lightbox" href="#demoLightbox"><img class="avatar avatar-original" style="-webkit-user-select:none;display:block; margin:auto;" src="/images/user.jpg" width="150" height="150"></a></div>';
					
					mod+='<div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true"><div class="lightbox-content"><img src="image.png"><div class="lightbox-caption"><p>Your caption here</p></div></div></div>';
					
					mod+='<div class="col-md-6 text-center"><label>ID Proof</label><img class="avatar avatar-original" style="-webkit-user-select:none;display:block; margin:auto;" src="/images/user.jpg" width="150" height="150"></div></div></div>';
					
					mod+='</div></div></div></div></div></div>';
					
					mod+='<div role="tabpanel" class="tab-pane" id="accountdetails'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
											
					mod+='<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Bank Name:</label><span id="bank_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Branch Name:</label> <span id="branch_name'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Account Number:</label><span id="account_number'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Cheque Name:</label> <span id="cheque_name'+element.id+'" class="text-muted"></span></div></div></div></div></div></div></div></div></div></div>';
					
					mod+='<div role="tabpanel" class="tab-pane" id="experiencedetails'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';

					mod+='<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label><b>Jobs Name:</b></label><span id="job_name_det'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Experience:</label> <span id="experience'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Description:</label><span id="description'+element.id+'" class="text-muted"></span></div></div></div></div></div></div></div></div></div></div>'; 
					
					if(element.active_status == 1){
						var activ ='<a onclick="return approve_de'+element.id+'();" class="btn btn-success">Approve</a><script>function approve_de'+element.id+'() {swal({ title: "Do You Want To Decline?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/service/member_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var activ  ='<a onclick="return approve_ac'+element.id+'();" class="btn btn-danger">Decline</a><script>function approve_ac'+element.id+'() {swal({ title: "Do You Want To Approve?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/service/member_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					//mod+='</div>';
					var staff = '';
					if(element.type=="Company"){
						staff += "<a href='#' data-toggle='modal' data-target='#staffDetails"+element.id+"'><i class='fa fa-users fa-2x'></i></a>";
						staff += "<div class='modal animated flipInX' id='staffDetails"+element.id+"' tabindex='-1' role='dialog' aria-labelledby='basicModal' aria-hidden='true'><div class='modal-dialog'><div class='modal-content modal-report'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h4 class='modal-title' id='myModalLabel'>Employee Details</h4></div><div class='modal-body inner-model' id='emp_table"+element.id+"'></div></div></div></div>";

						staff+='<script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/service/get_employee",success: function(data) {var obj = JSON.parse(data);var staff_table = obj.staff_table;';
					
						staff+='$("#emp_table'+element.id+'").html(staff_table);';
											
						staff+='} });});</script>';

					}else{
						staff += "No Staff";
					}
					row.push([++i,element.company_name,reg_date,element.user_id,element.type,element.mobile_number,activ,mod,staff]);
				});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
});

//Get All Empoloyee Details
router.post('/get_employee',function(req,res){
	var service_company_id = req.body.str;
	var sel_empl = "select * from company_staff where service_company_id='"+service_company_id+"'";
	connection.query(sel_empl,function(errors,staff,fields){
		var staff_table ='';
		staff_table += '<table class="table table-striped table-bordered">';
		staff_table += '<th>Name</th>';
		staff_table += '<th>Employee ID</th>';
		staff_table += '<th>Mobile Number</th>';
		staff_table += '<th>Profile Image</th>';
		staff.forEach(function(element){
			//var image = "<img src="+element.staff_photo+" class='img-circle' width='80' height='80' />";
			var image = "<img src='/images/no-user.png' class='img-circle' width='80' height='80' />";
			staff_table += '<tr><td>'+element.staff_name+'</td>';
			staff_table += '<td>'+element.staff_unique_id+'</td>';
			staff_table += '<td>'+element.staff_mobile+'</td>';
			staff_table += '<td>'+image+'</td></tr>';
		});
		staff_table += '</table>';
		res.send(JSON.stringify({"staff_table" : staff_table}));
	});
});

//Get Service Company Profile
router.post('/get_profile',function(req,res){
	var id = req.body.str;
	connection.query('select * from service_company_register where id=? order by id desc', [id], function (error, results, fields) {
			var city_id = results[0]['city'];
			var state_id = results[0]['state'];

		connection.query('select state_name from state where id=?',[state_id],function(err,state,fields){
				var state_name = state[0]['state_name'];
			connection.query('select city from city where id=?',[city_id],function(ers,city,fields){
				var city_name = city[0]['city'];
				var company_name = results[0]['company_name'];
				var email = results[0]['email'];
				var mobile_number = results[0]['mobile_number'];
				var password = results[0]['password'];
				var address = results[0]['address'];
				var pincode = results[0]['pincode'];
				var cre_date = results[0]['created_date'];
				var reg_date = dateFormat(cre_date, "dd-mm-yyyy");
				var contact_name = results[0]['contact_name'];
				var job_name = results[0]['job_name'];
				var jobs = JSON.parse(job_name);
				
				var bank_name = results[0]['bank_name'];
				var account_number = results[0]['account_number'];
				var branch_name = results[0]['branch_name'];
				var cheque_name = results[0]['cheque_name'];
				var experience = results[0]['experience'];
				var description = results[0]['description'];
				var qr = "select subcategory from service_subcategory where id IN ("+jobs+")";
				connection.query(qr,function(error,resl,fields){
					
					res.send(JSON.stringify({"company_name": company_name,"email": email,"mobile_number": mobile_number,"password": password,"address": address,"pincode": pincode,"reg_date": reg_date,"city_name": city_name,"state_name": state_name,"bank_name": bank_name,"branch_name": branch_name,"account_number": account_number,"cheque_name": cheque_name,"contact_name": contact_name,"job_name": resl,"experience": experience,"description": description}));
				});
			});
		});
			 
	});
		
});

//Get Job Profile
router.post('/get_job',function(req,res){
	var id = req.body.str;
	connection.query('select job_name,experience,description from service_company_register where id=? order by id desc', [id], function (error, results, fields) {
			
		var ss = JSON.parse(results[0]['job_name']);
		var all_job;
		var jb = '';
		var description = results[0]['description'];
		var experience = results[0]['experience'];
		for (var i=0; i<ss.length; i++){
			var id = ss[i];

		connection.query('select job_name from palma_job where active_status=? and id=?',[1,id],function(err,n_job,fields){
				all_job=n_job[0]['job_name'];

		});
		
		
		}
		
		res.send(JSON.stringify({"job_name" : all_job,"description" : description,"experience" : experience}));
			 
	});
		
});

//New Member 


router.post('/new_member',function(req,res){
				
				var filename;
				var filename2;
				var brand_name;
			upload(req, res, (error) => {
					
				if(error){
					
					  throw error;
				}else{
				
					//console.log(req.files['address_proof'][0].filename);
					//console.log(req.files['id_proof'][0].filename);
					filename = req.files['address_proof'][0].filename;
					filename2 = req.files['id_proof'][0].filename;
					
					if(filename == ''){
						filename = null;
					}
					if(filename2 == ''){
						filename2 = null;
					}
				
			
					//1st TAB
					var cid = req.body.cid;
					var comapny_name = req.body.comapny_name;
					var contact_name = req.body.contact_name;
					var address = req.body.address;
					var pincode = req.body.pincode;
					var city_name = req.body.city_name;
					var state = req.body.state;
					var c_mobile = req.body.c_mobile;
					var password = req.body.password;
					var mobile = req.body.mobile;
					var email = req.body.email;
					
					//Created Date
					var created_dt = mydate('date');
				    var created_time = mydate('time');
				    var created_date = created_dt+' '+created_time;
					
					//3rd TAB
					var cheque_name = req.body.cheque_name;
					var bank_name = req.body.bank_name;
					var account_number = req.body.account_number;
					var branch_name = req.body.branch_name;
					
					//4th TAB
					var job_name = JSON.stringify(req.body.job_name);
					var experience = req.body.experience;
					var description = req.body.description;
					
					var values = [cid,comapny_name,contact_name,address,pincode,city_name,state,c_mobile,password,mobile,email,filename,filename2,created_dt,created_date,cheque_name,bank_name,account_number,branch_name,job_name,experience,description];
					
					connection.query('INSERT INTO service_company_register (user_id,company_name,contact_name,address,pincode,city,state,contact_number,password,mobile_number,email,address_proof,id_proof,created_date,created_date_time,cheque_name,bank_name,account_number,branch_name,job_name,experience,description) VALUES ?',[[values]], function (error, results, fields) {
							  if (error){
								  throw error;
							  }else{
								  req.flash('success_msg','Member Added Successfully');
								  res.redirect('/service/member');
							  }
							  
					});	
					
				}
			});
});

	
//Get City
router.post('/getcity',function(req,res){
	var stateid = req.body.str;
	connection.query('select city,id from city where stateid=? order by id desc', [stateid], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  
				  var opt = '<option value="">--Select City--</option>';
					results.forEach(function(element) {
					  opt += '<option value='+element.id+'>'+element.city+'</option>';
					});
				  res.send(opt);
			  }  
		});
		
});


//Member Active

router.get('/member_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update service_company_register set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/service/member');
			}
		});
	}else{
		connection.query('update service_company_register set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/service/member');
			}
		});
	}
	
	
});

//Member Delete

router.get('/member_delete/:id',function(req,res){
	var cid = req.params.id;
	connection.query('delete from service_company_register where id=?',[cid], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','Deleted Successfully');
				  res.redirect('/service/member');
			  }  
		});
	
});


//Refund List

router.get('/refund', function (req, res) {
	var sel_customer = "select customer_name,id from customer_register order by id desc";

	connection.query(sel_customer,function(errors,results,fields){
		res.render('refund_list',{
			items: results
		});
	});
	
});


//Refund Ajax Table Fetch

router.post('/refund_ajax',function(req,res){
	
	params = req.body;
	 var customer = req.body.customer;
    var where = '';
    
    //Filter Area
    if(customer != '' && customer != undefined){
        where += " and customer_id='"+customer+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			if(where == ''){
				var sq = 'select * from service_booking where status="Cancel" and refund_status="0" and payment_status="Paid" order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from service_booking where status="Cancel" and refund_status="0" and payment_status="Paid" '+where+' order by id desc LIMIT '+start+','+end+'';
			}
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from service_booking where status="Cancel" and refund_status="0" and payment_status="Paid" order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				
				var i = start;
				var img;
					var total_count = 0;
					results.forEach(function(element) {
						var date_work = dateFormat(element.date_time, "yyyy, mm, dd");
						//var work_date = dateFormat(element.work_date, "yyyy, mm, dd");

						var cancel_date = dateFormat(element.cancel_date, "yyyy, mm, dd");
						
						var date1 = new Date(element.work_date);
						var date2 = new Date(date_work); 
						var diff_work = new DateDiff(date1, date2);
						 
						var date3 = new Date(cancel_date);
						var diff2 = new DateDiff(date1, date3);

						var days = diff_work.days();
						var days2 = diff2.days();
						var tot_days = days - days2;
						
						if(days2 >= 2){
					
						var company_name = '<script>$(document).ready(function(){var company_id = '+element.service_company_id+';var job_id='+element.work_category_id+';var customer_id = '+element.customer_id+';$.ajax({type: "POST",data: {company_id: company_id,job_id: job_id,customer_id: customer_id},url: "/service/get_all_det",success: function(data){var obj = JSON.parse(data);$("#comp_name'+element.id+'").html(obj.service);$("#job_name'+element.id+'").html(obj.job);$("#customer_name'+element.id+'").html(obj.customer);}});});</script>';
						
						company_name += '<span id="comp_name'+element.id+'"></span>';
						
						var job_name = '<span id="job_name'+element.id+'"></span>';
						
						var customer_name = '<span id="customer_name'+element.id+'"></span>';
						
						var mod = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-money fa-2x"></i></a>';

						/* mod+='<!--<a onclick="return cnfrm'+element.id+'();"><i class="fa fa-trash fa-2x"></i></a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/service/member_delete/'+element.id+'" ;} else {swal("Cancelled", " ", "error");}});}</script>-->'; */
						
						mod += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Refund Details</h4></div><div class="modal-body inner-model">';

						mod+='<div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body"><script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/service/get_refund",success: function(data) {var obj = JSON.parse(data);var paid_amount = obj.paid_amount;var refund = paid_amount * 2 / 100;';
						
						//mod+='$("#quote_amount1'+element.id+'").html(quote_amount);';
						
						mod+='$("#paid_amount1'+element.id+'").html(paid_amount);';
						
						mod+='$("#refund_amount1'+element.id+'").html(refund);';
						
						mod+='} });});</script>';
						
						mod+='<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Paid Amount:</label> <span id="paid_amount1'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Refund Amount(10%): </label><span id="refund_amount1'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><a href="/service/update_refund/'+element.id+'" class="btn btn-primary">Refund</a></div></div></div></div></div></div></div>';


						row.push([++i,company_name,job_name,customer_name,mod]);
						total_count++;
						}
						
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": total_count,"recordsFiltered": total_count}));
					  }
			});			  
		});
		
	
});

router.get('/update_refund/:id',function(req,res){
	var id = req.params.id;
	var sql = 'select service_booking.paid_amount,service_booking.customer_id,service_booking.job_no,service_booking.id,customer_register.wallet,customer_register.id,customer_register.customer_name,work_category.work_category from service_booking,customer_register INNER JOIN work_category ON work_category.id = service_booking.work_category_id where service_booking.customer_id=customer_register.id and service_booking.id="'+id+'"';
	connection.query(sql,function(error,results,fields){
		
		var paid_amount = results[0]['paid_amount'];
		var customer_id = results[0]['customer_id'];
		var customer_name = results[0]['customer_name'];
		var work_category = results[0]['work_category'];
		var job_no = results[0]['job_no'];
		var booking_id = results[0]['id'];
		var refund_amount = paid_amount * 10/100;
		var wallet = 0;
		wallet = results[0]['wallet'] + refund_amount;
		var refund_date = mydate('date');
		connection.query('update service_booking set refund_amount=?,refund_status=? where id=?',[refund_amount,1,id],function(err,row,fields){
			connection.query('update customer_register set wallet = ? where id=?',[wallet,customer_id],function(error,rowss,fields){
				var wall_insrt = 'insert into wallet (booking_id,job_name,customer_id,customer_name,type,create_date,paid_amount,amount) values ("'+booking_id+'","'+work_category+'","'+customer_id+'","'+customer_name+'","Refund",'+refund_date+'","'+paid_amount+'","'+refund_amount+'")';
				connection.query(wall_insrt,function(errorrs,exe,fields){
					res.redirect('/service/refund');
				});
				
			});
		});
		
	});

});
//Get Company Name, Job Name, Customer Name
router.post('/get_all_det',function(req,res){
	var job_id = req.body.job_id;
	var company_id = req.body.company_id;
	var customer_id = req.body.customer_id;
	connection.query('select work_category from work_category where id=? order by id desc', [job_id], function (error, results, fields) {
		connection.query('select customer_name,address from customer_register where id=? order by id desc',[customer_id],function(err, cus, fields){
			connection.query('select company_name from service_company_register where id=? order by id desc',[company_id],function(er,serv,fields){
				
				
				var job_name = results[0]['work_category'];
				var cus_name = cus[0]['customer_name'];
				var cus_address = cus[0]['address'];
				var com_name = serv[0]['company_name'];
				
			
				res.send(JSON.stringify({job: job_name,customer: cus_name,service: com_name,cus_address: cus_address}));
			});
			
		});
				  
	});
		
});


//Get Refund Amount
router.post('/get_refund',function(req,res){
	var id = req.body.str;
	
	connection.query('select sub_total from service_booking where id=? and status=? order by id desc', [id,'Cancel'],function (error, results, fields) {
		
		//var quote_amount = results[0]['quote_amount'];
		var paid_amount = results[0]['sub_total'];
	
		res.send(JSON.stringify({paid_amount: paid_amount}));
				
	});
	
});

module.exports = router;