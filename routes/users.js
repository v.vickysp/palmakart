var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Stratergy;
var session = require('express-session');
var User = require('../modules/users');
var connection = require('./db');
var path = require('path');
var multer = require('multer');
var bcrypt = require('bcryptjs');
var datatablesQuery = require('datatables-query');
var dateFormat = require('dateformat');


//storage 

var storage =  multer.diskStorage({
	destination: './public/uploads/brand',
	filename: function(req, file, cb){
		cb(null,file.fieldname+ '-' + Date.now() + path.extname(file.originalname));
	}
});

//init uploads

var upload = multer({
	storage: storage
}).single('brand_image');
router.get('/login',function(req,res){
	res.render('index');
});

//Register

router.get('/register',function(req,res){
	res.render('register');
});

//Dashboard

router.get('/dashboard',function(req,res){
	res.render('dashboard');
});

//Brand

router.get('/brand',function(req,res){
	connection.query('select * from palma_brand order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/brand',{
					  items : results
				  });
			  }  
		});
	
});

router.post('/brand_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			var sq = 'select * from palma_brand order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_brand order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
				results.forEach(function(element) {
					
					var serv = '<span id="serv'+element.id+'"></span>';
						serv += '<script>$(document).ready(function(){var service_id = '+element.service_id+';$.ajax({type: "POST",data: {str: service_id},url: "/settings/get_service",success: function(data){$("#serv'+element.id+'").html(data);}});});</script>';

					if(element.active_status == 1){
						var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					img = '<img src=/uploads/brand/'+element.brand_img+' class="img-circle" width="80" height="80">';
					var edit = '<a href="/users/editbrand/'+element.id+'" class="btn btn-success">Edit</a><!--<a class="btn btn-danger" onclick="cnfrm'+element.id+'();">Delete</a><script>function cnfrm'+element.id+'() {swal({ title: "Are you sure?",type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/brand_delete/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';

					var brn = '<span id="bran'+element.id+'"></span>';
						
					brn += '<script>$(document).ready(function(){var subcategory_id = '+element.subcategory_id+';$.ajax({type: "POST",data: {str: subcategory_id},url: "/users/get_subcategory",success: function(data){$("#bran'+element.id+'").html(data);}});});</script>';
				  row.push([++i,serv,brn,element.brand_name,img,stat,edit]);
				});
			
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


//Get Service Subcategory Name For List Table
router.post('/get_subcategory',function(req,res){
	var subcategory_id = req.body.str;
	var qr = connection.query('select subcategory from service_subcategory where id=?', [subcategory_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['subcategory'];
				  res.send(opt);
			  }  
		});
});

router.post('/brand_type_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			var sq = 'select * from palma_brand_type order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_brand_type order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
					results.forEach(function(element) {
						
						if(element.active_brand == 1){
							var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/type_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
						}else{
							var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/type_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
						}
						
						var edit = '<a href="/users/editbrand_type/'+element.id+'" class="btn btn-success">Edit</a><!--<a class="btn btn-danger" onclick="return cnfrm'+element.id+'();">Delete</a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/brand_type_delete/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';
						
						var brn = '<span id="bran'+element.id+'"></span>';
						
						brn += '<script>$(document).ready(function(){var brand_id = '+element.brand_id+';$.ajax({type: "POST",data: {str: brand_id},url: "/users/get_bran",success: function(data){$("#bran'+element.id+'").html(data);}});});</script>';
						
											
							row.push([++i,brn,element.brand_type,stat,edit]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


router.post('/work_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			var sq = 'select * from palma_work order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_work order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
					results.forEach(function(element) {
						var serv = '<span id="serv'+element.id+'"></span>';
						serv += '<script>$(document).ready(function(){var service_id = '+element.service_id+';$.ajax({type: "POST",data: {str: service_id},url: "/settings/get_service",success: function(data){$("#serv'+element.id+'").html(data);}});});</script>';

						var serv_sub = '<span id="serv_sub'+element.id+'"></span>';
						serv_sub += '<script>$(document).ready(function(){var subcategory_id = '+element.subcategory_id+';$.ajax({type: "POST",data: {str: subcategory_id},url: "/settings/get_sub_service",success: function(data){$("#serv_sub'+element.id+'").html(data);}});});</script>';

						if(element.active_status == 1){
							var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/work_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
						}else{
							var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/work_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
						}
						
						var edit = '<a href="/users/edit_work/'+element.id+'" class="btn btn-success">Edit</a><!--<a class="btn btn-danger" onclick="return cnfrm'+element.id+'();">Delete</a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/delete_work/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';
						

						var brn = '<span id="bran'+element.id+'"></span>';
						
						brn += '<script>$(document).ready(function(){var brand_id = '+element.brand_id+';$.ajax({type: "POST",data: {str: brand_id},url: "/users/get_bran",success: function(data){$("#bran'+element.id+'").html(data);}});});</script>';
						
						var brn_ty = '<span id="bran_ty'+element.id+'"></span>';
						
						brn_ty += '<script>$(document).ready(function(){var brand_type_id = '+element.brand_type_id+';$.ajax({type: "POST",data: {brand_type_id: brand_type_id},url: "/users/get_bran_type",success: function(data){$("#bran_ty'+element.id+'").html(data);}});});</script>';
						
						
											
							row.push([++i,serv,serv_sub,brn,element.work_name,stat,edit]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});



router.post('/job_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			var sq = 'select * from palma_job order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_job order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
					results.forEach(function(element) {
						
						if(element.active_status == 1){
							var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/job_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
						}else{
							var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/job_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
						}
						
						var edit = '<a href="/users/edit_job/'+element.id+'" class="btn btn-success">Edit</a><!--<a class="btn btn-danger" onclick="return cnfrm'+element.id+'();">Delete</a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/delete_job/'+element.id+'" ;} else {swal("Cancelled", " ", "error");}});}</script>-->';
						
						var brn = '<span id="bran'+element.id+'"></span>';
						
						brn += '<script>$(document).ready(function(){var brand_id = '+element.brand_id+';$.ajax({type: "POST",data: {str: brand_id},url: "/users/get_bran",success: function(data){$("#bran'+element.id+'").html(data);}});});</script>';
						
						var brn_ty = '<span id="bran_ty'+element.id+'"></span>';
						
						brn_ty += '<script>$(document).ready(function(){var brand_type_id = '+element.brand_type_id+';$.ajax({type: "POST",data: {brand_type_id: brand_type_id},url: "/users/get_bran_type",success: function(data){$("#bran_ty'+element.id+'").html(data);}});});</script>';
						
						var wrk = '<span id="work_na'+element.id+'"></span>';
						
						wrk += '<script>$(document).ready(function(){var work_id = '+element.work_id+';$.ajax({type: "POST",data: {work_id: work_id},url: "/users/get_work",success: function(data){$("#work_na'+element.id+'").html(data);}});});</script>';
						
						
											
							row.push([++i,brn,brn_ty,wrk,element.job_name,stat,edit]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


router.post('/customer_ajax',function(req,res){
	var params_row = new Array;
	params = req.body;
	var customer_id = req.body.customer_id;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(customer_id != '' && customer_id != undefined){
        where += " and id='"+customer_id+"'";
    }
    if(from_date != '' && to_date == '' && customer_id == ''){
        where += " and reg_date >= '"+from_date+"'";
    }
    if(from_date == '' && to_date != '' && customer_id == ''){
        where += " and reg_date <= '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && customer_id == ''){
        where += " and reg_date between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && customer_id != '' && from_date != undefined && to_date != undefined && customer_id != undefined){
        where += " and reg_date between '"+from_date+"' and '"+to_date+"' and id='"+customer_id+"'";
    }
	//Filter Area Query Ends Here
	
	var start = params.start;
	var end = params.length;
	var val = start,end;
	//console.log(params_row[params['search']['value']]);
	//var ss = JSON.stringify(params.search);
	//console.log(ss);
	//console.log(params[search][value]);
	if(where == ''){
		var sq = 'select * from customer_register order by id desc LIMIT '+start+','+end+'';
	}else{
		var sq = 'select * from customer_register where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
	}
			
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from customer_register order by id desc',function(error,main,fields){
				var row = new Array;
				if(where == ''){
                    var tot = main.length;
                }else{
                    var tot = results.length;
                }
				var i = start;
				var img;
				
					results.forEach(function(element) {
						
						var reg_date = dateFormat(element.reg_date,'dd-mm-yyyy');
						
						var mod = '<!--<a href="/users/editbrand_type/'+element.id+'"><i class="fa fa-edit"></i></a>--><a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-user fa-2x" style="margin-right: 10px;"></i></a><a href="#" data-toggle="modal" data-target="#account_model'+element.id+'"><i class="fa fa-info fa-2x"></i></a><!--<a onclick="return cnfrm'+element.id+'();"><i class="fa fa-trash fa-2x"></i></a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/users/customer_delete/'+element.id+'" ;} else {swal("Cancelled", " ", "error");}});}</script>-->';
						
						mod += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h2 class="modal-title" id="myModalLabel">Profile Details</h2></div><div class="modal-body inner-model"><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
						
						mod+='<script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/users/get_profile",success: function(data) {var obj = JSON.parse(data);var cus_name = obj.cus_name;var cus_email = obj.email;var cus_mobile = obj.mobile_number;var cus_pass = obj.password;var cus_addr = obj.address;var cus_city = obj.city;var cus_state = obj.state;var cus_pincode = obj.pincode;var gender = obj.gender;var reg_date = obj.reg_date;var account_number = obj.account_number;var branch_name = obj.branch_name;var bank_name = obj.bank_name;var ifsc = obj.ifsc;';
						
						mod+='$("#user_name'+element.id+'").html(cus_name);';
						
						mod+='$("#u_email'+element.id+'").html(cus_email);';
						
						mod+='$("#u_mobile'+element.id+'").html(cus_mobile);';
						
						mod+='$("#u_pass'+element.id+'").html(cus_pass);';
						
						mod+='$("#u_addr'+element.id+'").html(cus_addr);';
						
						mod+='$("#u_city'+element.id+'").html(cus_city);';
						
						mod+='$("#u_state'+element.id+'").html(cus_state);';
						
						mod+='$("#u_pincode'+element.id+'").html(cus_pincode);';

						mod+='$("#gender'+element.id+'").html(gender);';

						mod+='$("#bank_name'+element.id+'").html(bank_name);';
						
						mod+='$("#branch_name'+element.id+'").html(branch_name);';
						
						mod+='$("#account_number'+element.id+'").html(account_number);';

						mod+='$("#ifsc'+element.id+'").html(ifsc);';

						mod+='$("#reg_date'+element.id+'").html(reg_date);} });});</script>';
						
						mod+='<div class="col-lg-12"><div class="col-md-6"><label>Customer Name:</label><span id="user_name'+element.id+'" class="nxt-line"></span></div><div class="col-md-6"><label>Email:</label> <span id="u_email'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Mobile:</label> <span id="u_mobile'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Password:</label> <span id="u_pass'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Address:</label> <span id="u_addr'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>City:</label> <span id="u_city'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>State:</label> <span id="u_state'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Pincode:</label> <span id="u_pincode'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Registered Date:</label><span id="reg_date'+element.id+'" class="nxt-line"></span></div></div></div></div></div></div></div></div></div></div></div></div></div></div>';


						mod += '<div class="modal animated flipInX" id="account_model'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h2 class="modal-title" id="myModalLabel">Account Details</h2></div><div class="modal-body inner-model"><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
						
						//mod+='<script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/users/get_profile",success: function(data) {var obj = JSON.parse(data);var cus_name = obj.cus_name;var cus_email = obj.email;var cus_mobile = obj.mobile_number;var cus_pass = obj.password;var cus_addr = obj.address;var cus_city = obj.city;var cus_state = obj.state;var cus_pincode = obj.pincode;var gender = obj.gender;var reg_date = obj.reg_date;';
						
						//mod+='$("#bank_name'+element.id+'").html(bank_name);';
						
						//mod+='$("#branch_name'+element.id+'").html(branch_name);';
						
						//mod+='$("#account_number'+element.id+'").html(account_number);} });});</script>';
						
						
						mod+='<div class="col-lg-12"><div class="col-md-6"><label>Bank Name:</label><span id="bank_name'+element.id+'" class="nxt-line"></span></div><div class="col-md-6"><label>Branch Name:</label> <span id="branch_name'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>Account Number:</label> <span id="account_number'+element.id+'" class="text-muted nxt-line"></span></div><div class="col-md-6"><label>IFSC Code : </label><span id="ifsc'+element.id+'"></span></div></div></div></div></div></div></div></div></div></div></div></div></div></div>';

											
							row.push([++i,element.customer_name,reg_date,element.mobile_number,mod]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Brand Delete

router.get('/brand_delete/:id',function(req,res){
	var bid = req.params.id;
	connection.query('delete from palma_brand where id=?',[bid], function (error, results, fields) {
			connection.query('delete from palma_brand_type where brand_id=?',[bid], function(err,ro,fields){
				connection.query('delete from palma_work where brand_id=?',[bid],function(erro,wo,fields){
					connection.query('delete from palma_job where brand_id=?',[bid],function(errs,jo,fields){
						req.flash('success_msg','Deleted Successfully');
				  res.redirect('/users/brand');
					});
				});
			});
			    
		});
	
});

//Brand Type Delete

router.get('/brand_type_delete/:id',function(req,res){
	var bid = req.params.id;
	connection.query('delete from palma_brand_type where id=?',[bid], function (error, results, fields) {
			connection.query('delete from palma_work where brand_type_id=?',[bid],function(err,wod,fields){
				connection.query('delete from palma_job where brand_type_id=?',[bid],function(er,jod,fields){
					req.flash('success_msg','Deleted Successfully');
					res.redirect('/users/brand_type');
				});
			});
			  
				  
			
		});
	
});


//Brand

router.get('/brand_add',function(req,res){
	connection.query('select name,id from service_name order by id desc', function (error, rows, fields) {
		
		if (error) 
		{	
			throw error;
		}else{
			res.render('settings/new_brand',{
				items: rows
			});
		}
	});
});

//Brand Edit

router.get('/editbrand/:id',function(req,res){
	connection.query('select * from palma_brand where id=?',[req.params.id], function (error, rows, fields) {
		if (error) 
		{	
			throw error;
		}
		connection.query('select id,subcategory from service_subcategory',function(error,results,fields){
			connection.query('select id,name from service_name',function(er,btyps,fields){
				res.render('settings/brand_edit',{
					items: rows,
					selcs: results,
					btyp: btyps
				});
			});
		});
	});
	
});

//Brand Active

router.get('/active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update palma_brand set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/brand');
			}
		});
	}else{
		connection.query('update palma_brand set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/brand');
			}
		});
	}
	
	
});



//Brand Type

router.get('/brand_type',function(req,res){
	
	connection.query('select * from palma_brand_type order by id desc', function (error, results, fields) {
				
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/brand_type',{
					  items : results
				  });
			  }  
		});
	
});


//Brand Type Active

router.get('/type_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update palma_brand_type set active_brand=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/brand_type');
			}
		});
	}else{
		connection.query('update palma_brand_type set active_brand=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/brand_type');
			}
		});
	}
	
	
});


//Brand Type Add View

router.get('/brand_add_type',function(req,res){
	connection.query('select * from palma_brand where active_status = "1" order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/brand_add_type',{
					  items : results
				  });
			  }  
		});
});

//Brand Edit

router.get('/editbrand_type/:id',function(req,res){
	
	connection.query('select * from palma_brand_type where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}/* else{
			
			res.render('brand_edit_type',{
				items: results
			});
		} */
	
	
	connection.query('select * from palma_brand', function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}//else{
				res.render('settings/brand_edit_type',{
					items: results,
					selcs: rows
				});
			//}
		});
	});
	
});

//New Brand Type


router.post('/new_brand_type',function(req,res){
		
				var brand_name = req.body.brand_name;
				var brand_type = req.body.brand_type;
				var values = [brand_name,brand_type];
				connection.query('INSERT INTO palma_brand_type (brand_id,brand_type) VALUES ?',[[values]], function (error, results, fields) {
						  if (error){
							  throw error;
						  }else{
							  req.flash('success_msg','Brand Type Added Successfully');
							  res.redirect('/users/brand_type');
						  }
						  
				});
	});


	
//Work

router.get('/work',function(req,res){
	connection.query('select * from palma_work order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/work',{
					  items : results
				  });
			  }  
		});
	
});


//Work Active

router.get('/work_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update palma_work set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/work');
			}
		});
	}else{
		connection.query('update palma_work set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/work');
			}
		});
	}
	
	
});

//Work Add View

router.get('/work_add',function(req,res){
	connection.query('select * from service_name where active_status = "1" order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }

				  res.render('settings/work_add',{
					  items : results
				  });
			  
		});
});


//New Work Add


router.post('/add_work',function(req,res){
		var brand = req.body.brand_name;
		var service_name = req.body.service_name;
		var sub_name = req.body.sub_name;
		var work_name = req.body.work_name;
		var price = req.body.price;
		var brand_name;
		if(brand == undefined){
			brand_name = '';
		}else{
			brand_name = req.body.brand_name;
		}
		
		var values = [service_name,sub_name,brand_name,work_name];
		
			connection.query('INSERT INTO palma_work (service_id,subcategory_id,brand_id,work_name) VALUES ?',[[values]], function (error, results, fields) {
					  if (error){
						  throw error;
					  }else{
						  req.flash('success_msg','Work Added Successfully');
						  res.redirect('/users/work');
					  }
					  
			});	
});


//Work Edit View

router.get('/edit_work/:id',function(req,res){
	
	connection.query('select * from palma_work where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}
		connection.query('select brand_name,id from palma_brand', function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}
			connection.query('select name,id from  service_name', function (error, btyps, fields) {
				if (error) 
				{	
					throw error;
				}
				connection.query('select subcategory,id from service_subcategory',function(err,subcat,fields){

					res.render('settings/edit_work',{
						items: results,
						selcs: rows,
						btyp: btyps,
						subca: subcat
					});
				});

			});
		});
	});
	
});



//Update Work



router.post('/work_edit',function(req,res){
	var brand_name = req.body.brand_name;

	var service_name = req.body.service_name;
	var sub_name = req.body.sub_name;
	var work_name = req.body.work_name;
	var price = req.body.price;
	var id = req.body.bid;
	var bran;
	if(brand_name == 'undefined'){
		bran =  req.body.brand_na;;
	}else{
		bran = req.body.brand_name;
	}
	var qr = connection.query('update palma_work set service_id=?,subcategory_id=?,brand_id=?,work_name=?,price=? where id=?',[service_name,sub_name,bran,work_name,price,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Work Updated Successfully');
				  res.redirect('/users/work');
			  }
			  
	});	

});

//Work Delete

router.get('/delete_work/:id',function(req,res){
	var bid = req.params.id;
	connection.query('delete from palma_work where id=?',[bid], function (error, results, fields) {
		connection.query('delete from palma_job where work_id=?',[bid],function(err,jod,fields){
				req.flash('success_msg','Deleted Successfully');
				  res.redirect('/users/work');
		});
			   
	});
	
});

//Get Brand Type Option
router.post('/getjson',function(req,res){
	var service_id = req.body.str;
	var qr = connection.query('select subcategory,id from service_subcategory where service_name=? and active_status=? order by id desc', [service_id,1], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  /* res.render('work_add',{
					  brand_ty : results
				  }); */
				  var num = results.length;
				  if(num == 0){
					  var opt = '1';
				  }else{
					var opt = '<option value="">--Select Any--</option>';
					results.forEach(function(element) {
					  opt += '<option value='+element.id+'>'+element.subcategory+'</option>';
					});
				  }
				  res.send(opt);
			  }  
		});
});





//Get Brand Option
router.post('/getbrand',function(req,res){
	var service_sub_id = req.body.str;
	var qr = connection.query('select brand_name,id from palma_brand where subcategory_id=? and active_status=? order by id desc', [service_sub_id,1], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  /* res.render('work_add',{
					  brand_ty : results
				  }); */
				  	var num = results.length;
				  	if(num != 0){
					var opt = '<div class="col-md-6 form-group"><label>Brand Name<span class="required">*</span></label><select class="form-control" name="brand_name" id="brand_name"><option value="">--Select Brand--</option>';
					results.forEach(function(element) {
					  opt += '<option value='+element.id+'>'+element.brand_name+'</option>';
					});
					opt+='</select></div>';
				 	}else{
				 		opt = '1';
				 	}
				  res.send(opt);
			  }  
		});
});


//Get Work Option
router.post('/getwork',function(req,res){
	var service_sub_id = req.body.str;
	var qr = connection.query('select work_name,id from palma_work where subcategory_id=? and active_status=? order by id desc', [service_sub_id,1], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  /* res.render('work_add',{
					  brand_ty : results
				  }); */
				  	var num = results.length;
				  	if(num != 0){
						var opt='<option value="">--Select Work--</option>';
						results.forEach(function(element) {
						opt += '<option value='+element.id+'>'+element.work_name+'</option>';
						});
				 	}else{
				 		opt = '1';
				 	}
				  res.send(opt);
			  }  
		});
});

//Get Customer Profile
router.post('/get_profile',function(req,res){
	var id = req.body.str;
	connection.query('select * from customer_register where id=? order by id desc', [id], function (error, results, fields) {
			var city_id = results[0]['city'];
			var state_id = results[0]['state'];
		connection.query('select state_name from state where id=?',[state_id],function(err,state,fields){
				var state_name = state[0]['state_name'];
			connection.query('select city from city where id=?',[city_id],function(ers,city,fields){
				var city_name = city[0]['city'];
				var cus_name = results[0]['customer_name'];
				var gender = results[0]['gender'];
				var address = results[0]['address'];
				var pincode = results[0]['pincode'];
				var mobile_number = results[0]['mobile_number'];
				var password = results[0]['password'];
				var email = results[0]['email'];
				var reg_date_es = results[0]['reg_date'];
				var reg_date = dateFormat(reg_date_es, "dd-mm-yyyy");
				var account_number = results[0]['account_number'];
				var bank_name = results[0]['bank_name'];
				var branch_name = results[0]['branch_name'];
				var ifsc_code = results[0]['ifsc_code'];
					res.send(JSON.stringify({"cus_name" : cus_name,"gender": gender,"address": address,"pincode": pincode,"mobile_number": mobile_number,"password": password,"email": email,"city" : city_name, "state": state_name, "reg_date" : reg_date,"account_number": account_number,"bank_name": bank_name,"branch_name": branch_name,"ifsc" : ifsc_code}));
			});
		});  
	});
});


//Get Brand Name For List Table
router.post('/get_bran',function(req,res){
	var brand_id = req.body.str;
	if(brand_id != '0'){
	var qr = connection.query('select brand_name as brand_name from palma_brand where id=?', [brand_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				
				  var opt = results[0]['brand_name'];
				  res.send(opt);
			  }  
		});
	}else{
		var opt = 'None';
		 res.send(opt);
	}
	
});

//Get Brand Type For List Table
/*router.post('/get_bran_type',function(req,res){
	var brand_type_id = req.body.brand_type_id;
	connection.query('select brand_type from palma_brand_type where id=?', [brand_type_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['brand_type'];
				  res.send(opt);
			  }  
		});
		
});*/


//Get Work Name For List Table
router.post('/get_work',function(req,res){
	var work_id = req.body.work_id;
	connection.query('select work_name from palma_work where id=?', [work_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['work_name'];
				  res.send(opt);
			  }  
		});
});


//Job

router.get('/job',function(req,res){
	connection.query('select * from palma_job order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/job',{
					  items : results
				  });
			  }  
		});
	
});

//Job Add View

router.get('/job_add',function(req,res){
	connection.query('select * from palma_brand where active_status = "1" order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('settings/job_add',{
					  items : results
				  });
			  }  
		});
});



//Get Work Name Option
router.post('/gettype',function(req,res){
	var brand_type = req.body.brand_type;
	var brand_id = req.body.brand_id;
	var qr = connection.query('select work_name,id from palma_work where brand_id=? and brand_type_id=? and active_status=? order by id desc', [brand_id,brand_type,1], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  /* res.render('work_add',{
					  brand_ty : results
				  }); */
				  var num = results.length;
				  if(num == 0){
					  var opt = '1';
				  }else{
					  var opt = '<option value="">--Select Any--</option>';
						results.forEach(function(element) {
						  opt += '<option value='+element.id+'>'+element.work_name+'</option>';
						});
				  }
				  res.send(opt);
			  }  
		});
});

//New Job Add


router.post('/add_job',function(req,res){
		var brand_name = req.body.brand_name;
		var brand_type = req.body.brand_type;
		var work_name = req.body.work_name;
		var job_name = req.body.job_name;
		var values = [brand_name,brand_type,work_name,job_name];
		
			connection.query('INSERT INTO palma_job (brand_id,brand_type_id,work_id,job_name) VALUES ?',[[values]], function (error, results, fields) {
					  if (error){
						  throw error;
					  }else{
						  req.flash('success_msg','Job Added Successfully');
						  res.redirect('/users/job');
					  }
					  
			});	
});

//Job Edit View

router.get('/edit_job/:id',function(req,res){
	
	connection.query('select * from palma_job where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}
	
	
		connection.query('select brand_name,id from palma_brand', function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}
			connection.query('select brand_type,id from palma_brand_type', function (error, btyps, fields) {
				if (error) 
				{	
					throw error;
				}
				
				connection.query('select work_name,id from palma_work', function (error, wrk_nm, fields) {
					
					if (error) 
					{	
						throw error;
					}

					res.render('settings/edit_job',{
						items: results,
						selcs: rows,
						btyp: btyps,
						wrk_nm: wrk_nm
					});
				});
			});
		});
	});
	
});



//Update Job



router.post('/job_edit',function(req,res){
	var brand_name = req.body.brand_name;
	var brand_type = req.body.brand_type;
	var work_name = req.body.work_name;
	var job_name = req.body.job_name;
	var id = req.body.bid;
	var qr = connection.query('update palma_job set brand_id=?,brand_type_id=?,work_id=?,job_name=? where id=?',[brand_name,brand_type,work_name,job_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Job Updated Successfully');
				  res.redirect('/users/job');
			  }
			  
	});	
	
});

//Job Delete

router.get('/delete_job/:id',function(req,res){
	var bid = req.params.id;
	connection.query('delete from palma_job where id=?',[bid], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','Deleted Successfully');
				  res.redirect('/users/job');
			  }  
		});
	
});


//Job Active

router.get('/job_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update palma_job set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/job');
			}
		});
	}else{
		connection.query('update palma_job set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/users/job');
			}
		});
	}
	
	
});


//Brand Type Delete

router.get('/customer_delete/:id',function(req,res){
	var cid = req.params.id;
	connection.query('delete from customer_register where id=?',[cid], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','Deleted Successfully');
				  res.redirect('/users/customer');
			  }  
		});
	
});


//Register User

router.post('/register',function(req,res){
	var params = req.body;
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;
	
	//Validation
	
	req.checkBody('name','Name Is Required').notEmpty();
	req.checkBody('email','Email Is Required').notEmpty();
	req.checkBody('email','Email Is not valid').isEmail();
	req.checkBody('username','Username Is Required').notEmpty();
	req.checkBody('password','Password Is Required').notEmpty();
	req.checkBody('password2','Password do not match').equals(req.body.password);
	
	var errors = req.validationErrors();
	
	if(errors){
		res.render('register',{
			errors:errors
		});
	}else{
		connection.query('INSERT INTO register SET ?',[params], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','You are registered and can now login');
				  res.redirect('/users/login');
			  }  
		});
		
		console.log(qr.sql);
	}
	
});


//New Brand


router.post('/new_brand',function(req,res){
	
	var filename;
	var brand_name;
	var service_subcategory;
	var service_id;
		upload(req, res, (error) => {
			if(error){
				/* res.render('new_brand',{
					  error_msg: error
				  }); */
				  
				  throw error;
			}else{
				filename = req.file.filename;
				if(filename == ''){
					filename = null;
				}
			
				service_subcategory = req.body.service_name;
				service_id = req.body.service_id;	
				brand_name = req.body.brand_name;
				var values = [brand_name,filename,service_subcategory,service_id];
				connection.query('INSERT INTO palma_brand (brand_name,brand_img,subcategory_id,service_id) VALUES ?',[[values]], function (error, results, fields) {
						  if (error){
							  throw error;
						  }else{
							  req.flash('success_msg','Brand Added Successfully');
							  res.redirect('/users/brand');
						  }
						  
				});	
			}
		});
});

//Edit Brand 



router.post('/brand_edit',function(req,res){
	
	//var brand_image = req.body.brand_image;
	var filename;
	upload(req, res, (error) => {
		if(error){
			
			  throw error;
		}else{
			var ff = req.file;
			if(ff != undefined){
				filename = req.file.filename;
			}else{
				filename = req.body.bimg;
			}
			var service_name = req.body.service_name;
			var brand_name = req.body.brand_name;
			var service_id = req.body.service_id;
			var id = req.body.bid;
				
			var qr = connection.query('update palma_brand set brand_name=?,brand_img=?,subcategory_id=?,service_id=? where id=?',[brand_name,filename,service_name,service_id,id], function (error, results, fields) {
					  if (error){
						  throw error;
					  }else{
						  req.flash('success_msg','Brand Updated Successfully');
						  res.redirect('/users/brand');
					  }
					  
			});
			
		}
	});
});



//Edit Brand Type



router.post('/brand_edit_type',function(req,res){
	var brand_name = req.body.brand_name;
	var brand_type = req.body.brand_type;
	var id = req.body.bid;
	var qr = connection.query('update palma_brand_type set brand_id=?,brand_type=? where id=?',[brand_name,brand_type,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Brand Type Updated Successfully');
				  res.redirect('/users/brand_type');
			  }
			  
	});	
});



//Customer List

router.get('/customer', function (req, res) {
	
		connection.query('select * from customer_register order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.render('customer_list',{
					  items : results
				  });
			  }  
		});
		
		
});

//Customer Add Page

router.get('/customer_add',function(req,res){
	res.render('new_customer');
});
	
//Login 


router.post('/login',function(req,res){
	var username = req.body.name;
	var password = req.body.password;
	
	connection.query('select * from admin where username=? and password=?',[username,password], function (error, results, fields) {
			  if (error) throw error;
			  
					var cunt = results.length;
				 
				 if(cunt!=0){
					  req.session.user = username;
					  
					  req.flash('success_msg','Login Success');
					  //res.send(username);
					  res.redirect('/users/dashboard');
					  //res.render('../views/layouts/layout.handlebars',{user: req.session.user});
					  
					  
				 }else{
					  req.flash('error_msg','Invalid Username & Password');
					  res.redirect('/users/login');
				 }
				
			  
			});
			
	
});

//FeedBack

router.get('/feedback',function(req,res){
	
	res.render('feedback_list');
	
});

router.post('/feedback_ajax',function(req,res){
	
	params = req.body;
	var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(from_date != '' && to_date == '' ){
        where += " and feed_date >= '"+from_date+"'";
    }
    if(from_date == '' && to_date != ''){
        where += " and feed_date <= '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && from_date != undefined && to_date != undefined){
        where += " and feed_date between '"+from_date+"' and '"+to_date+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			if(where == ''){
				var sq = 'select * from palma_feedback order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from palma_feedback where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_feedback order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				var i = start;
				var img;
				
					results.forEach(function(element) {

						var customer_name = element.customer_name;

						var cus_email = element.email;
						
						var description = element.description;

						var dt = element.feed_date;

						var fd_date = dateFormat(dt,'dd-mm-yyyy');

							row.push([++i,fd_date,customer_name,cus_email,description]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});



//Complaints

router.get('/complaint',function(req,res){
	
	res.render('complaints_list');
	
});

router.post('/complaint_ajax',function(req,res){
	
	params = req.body;
	var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(from_date != '' && to_date == '' ){
        where += " and complaint_date >= '"+from_date+"'";
    }
    if(from_date == '' && to_date != ''){
        where += " and complaint_date <= '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && from_date != undefined && to_date != undefined){
        where += " and complaint_date between '"+from_date+"' and '"+to_date+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			if(where == ''){
				var sq = 'select * from palma_complaints order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from palma_complaints where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
			
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_complaints order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				
				var i = start;
				var img;
				
					results.forEach(function(element) {

						var cus_email = element.email;
						
						var job_no = element.job_no;
						
						var mod = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-eye fa-2x"></i></a>';
						
						mod += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Complaint Details</h4></div><div class="modal-body inner-model">';
						mod+='<div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body"><script>$(document).ready( function () {var id = '+element.id+';$.ajax({type: "POST",data: {str: id},url: "/users/get_comp",success: function(data) {var obj = JSON.parse(data);var description = obj.description;var customer_name = obj.customer_name;var customer_address = obj.customer_address;var email = obj.email;var job_no = obj.job_no;var company_name = obj.company_name;var user_id = obj.user_id;var staff_name = obj.staff_name;var staff_unique_id = obj.staff_unique_id; ';
						
						mod+='$("#customer_name'+element.id+'").html(customer_name);';

						mod+='$("#customer_address'+element.id+'").html(customer_address);';

						mod+='$("#email'+element.id+'").html(email);';

						mod+='$("#job_no'+element.id+'").html(job_no);';

						mod+='$("#company_name'+element.id+'").html(company_name);';

						mod+='$("#user_id'+element.id+'").html(user_id);';

						mod+='$("#staff_name'+element.id+'").html(staff_name);';

						mod+='$("#staff_unique_id'+element.id+'").html(staff_unique_id);';

						mod+='$("#description'+element.id+'").html(description);';
						
						mod+='} });});</script>';
						
						mod+='<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Customer Name:</label><span id="customer_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Customer Address:</label> <span id="customer_address'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Email :</label><span id="email'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label><b>Job No :</label><span id="job_no'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Company Name :</label><span id="company_name'+element.id+'"></span></div><div class="col-md-6"><label>user ID : </label><span id="user_id'+element.id+'"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Staff Name: </label><span id="staff_name'+element.id+'"></span></div><div class="col-md-6"><label>Staff Unique ID : </label><span id="staff_unique_id'+element.id+'"></span></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Description : </label><span id="description'+element.id+'"></span></div></div></div></div></div></div></div>';
						
						var create_date = dateFormat(element.complaint_date,'dd-mm-yyyy');
							row.push([++i,create_date,cus_email,job_no,mod]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Get Description`
router.post('/get_comp',function(req,res){
	var id = req.body.str;
	connection.query('select * from palma_complaints where id=? order by id desc', [id], function (error, results, fields) {
		var customer_name = results[0]['customer_name'];
		var customer_address = results[0]['customer_address'];
		var company_name = results[0]['company_name'];
		var user_id = results[0]['user_id'];
		var staff_name = results[0]['staff_name'];
		var staff_unique_id = results[0]['staff_unique_id'];
		var description = results[0]['description'];
		var email = results[0]['email'];
		var job_no = results[0]['job_no'];

		res.send(JSON.stringify({customer_name : customer_name,customer_address : customer_address,company_name : company_name,user_id : user_id,staff_name : staff_name,staff_unique_id : staff_unique_id,description: description,email: email,job_no: job_no}));

	});
		
});

//Log out

router.get('/logout',function(req,res){
	req.logout();
	
	req.flash('success_msg','You are logged out');
	req.session.destroy();
	res.redirect('/');
});
module.exports = router;