var express = require('express');
var router = express.Router();
var connection = require('./db');
var path = require('path');
var dateFormat = require('dateformat');
var mydate = require('current-date');

router.get('/amount_history',function(req,res){
	connection.query('select company_name,id from service_company_register where active_status="1"',function(error,results,fields){
		res.render('amount/amount_history_list',{
			items: results
		});
	});
  
});


router.post('/amount_ajax',function(req,res){
	
	params = req.body;
	var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(from_date != '' && to_date == '' ){
        where += " and create_date >= '"+from_date+"'";
    }
    if(from_date == '' && to_date != ''){
        where += " and create_date <= '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && from_date != undefined && to_date != undefined){
        where += " and create_date between '"+from_date+"' and '"+to_date+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
	if(where == ''){
        var sq = 'select * from member_wallet order by id desc LIMIT '+start+','+end+'';
    }else{
        var sq = 'select * from member_wallet where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
    }
			
	connection.query(sq,function (error, results, fields) {

			connection.query('select * from member_wallet order by id',function(error,main,fields){
				var row = new Array;
				if(where ==''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				var i = start;
				
					results.forEach(function(element) {
						
                        var booking_id          = element.booking_id;
						var created_date    	= dateFormat(element.create_date,'dd-mm-yyyy');
						var company_name		= element.company_name;
						var user_id				= element.user_id; 
                        var type        		= element.type;
                        var cash_commission     = element.commission_to_pay;
                        var total_amount        = element.paid_amount;
						var online_commission   = element.commission_to_get;
						var job_status			= '<div class="label label-success">'+element.job_status+'</div>';
						//Payment Button
						if(type == "Online"){

							if(element.pay_status != 'Paid'){
							
							
							var pay_button = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-money" style="font-size:24px"></i></a>';

							pay_button += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Amount Receiving Details</h4></div><div class="modal-body inner-model"><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body"><script>$(document).ready( function () {';

							pay_button += '$("#paid_amount1'+element.id+'").html('+element.paid_amount+');';
	
							pay_button += '$("#refund_amount1'+element.id+'").html('+element.commission_to_pay+');';

							pay_button += '});</script>';

							pay_button += '<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Paid Amount:</label> <span id="paid_amount1'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Commission Amount (5%): </label><span id="refund_amount1'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><a href="/amount_details/insert_receiving/'+element.booking_id+'/'+element.paid_amount+'/'+element.commission_to_get+'/'+element.commission_to_pay+'/'+element.type+'/'+element.company_name+'" class="btn btn-primary">Pay</a></div></div></div></div></div></div></div>';
							}else{
								pay_button = '<div class="label label-success">Commission Paid</div>';
							}

							var receive_button = 'None';
						}else{
							var pay_button = 'None';
							if(element.pay_status != 'Paid'){
								var receive_button = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-money fa-2x"></i></a><div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Amount Receiving Details</h4></div><div class="modal-body inner-model"><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body"><script>$(document).ready( function () {';

								receive_button += '$("#paid_amount1'+element.id+'").html('+element.paid_amount+');';
	
								receive_button += '$("#refund_amount1'+element.id+'").html('+element.commission_to_get+');';
	
								receive_button += '});</script>';
	
								receive_button += '<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-6"><label>Paid Amount:</label> <span id="paid_amount1'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Commission Amount (5%): </label><span id="refund_amount1'+element.id+'" class="text-muted"></span></div></div></div><div class="col-md-12"><a href="/amount_details/insert_receiving/'+element.booking_id+'/'+element.paid_amount+'/'+element.commission_to_get+'/'+element.commission_to_pay+'/'+element.type+'/'+element.company_name+'" class="btn btn-primary">Receive</a></div></div></div></div></div></div></div>';
							}else{
								receive_button = '<div class="label label-success">Commission Received</div>';
							}
							
						}
						

							row.push([++i,booking_id,created_date,company_name,user_id,type,total_amount,cash_commission,online_commission,job_status,pay_button,receive_button]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

router.get('/insert_receiving/:booking_id/:paid_amount/:commission_to_get/:commission_to_pay/:type/:company_name',function(req,res){
	var booking_id 			= req.params.booking_id;
	var paid_amount			= req.params.paid_amount;
	var commission_to_get	= req.params.commission_to_get;
	var commission_to_pay	= req.params.commission_to_pay;
	var company_name	= req.params.company_name;
	var type	= req.params.type;
	var transaction_date	= mydate('date');
	var transaction_time 	= mydate('time');
	var transaction_datetime	= transaction_date+' '+transaction_time;

	var values = [booking_id,company_name,paid_amount,type,commission_to_get,commission_to_pay,transaction_date,transaction_datetime];
	var ins_payment_history = "insert into commission_payment_history (booking_id,company_name,paid_amount,type,commission_to_get,commission_to_pay,transaction_date,transaction_date_time) values (?)";

	connection.query(ins_payment_history,[values],function(error,results,fields){
		var up_amount = "update member_wallet set pay_status='Paid' where booking_id=?";
		connection.query(up_amount,[booking_id],function(errors,up_res,fields){
			res.redirect('/amount_details/amount_history');
		});
		
	});
	

});
module.exports = router;