var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Stratergy;
var session = require('express-session');
var User = require('../modules/users');
var connection = require('./db');
var path = require('path');
var multer = require('multer');
var bcrypt = require('bcryptjs');
var datatablesQuery = require('datatables-query');
var dateFormat = require('dateformat');



//storage 

var storage =  multer.diskStorage({
	destination: './public/uploads/subcategory',
	filename: function(req, file, cb){
		cb(null,file.fieldname+ '_' + Date.now() + path.extname(file.originalname));
	}
});

//init uploads

var upload = multer({
	storage: storage
}).single('sub_image');


//Main Service

router.get('/main_service',function(req,res){
	var sel_service = "select name,id from service_name order by id desc";
	connection.query(sel_service,function(errors,results,fields){
		res.render('settings/service_main',{
			items: results
		});
	});
	
});

router.post('/service_list',function(req,res){
	
	params = req.body;
	var service_name = req.body.service_name;
    var where = '';
    
    //Filter Area
    if(service_name != '' && service_name != undefined){
        where += " and id='"+service_name+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			if(where == ''){
				var sq = 'select * from service_name order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from service_name where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from service_name order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				
				var i = start;
				var img;
				
				results.forEach(function(element) {
					
					if(element.active_status == 1){
						var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/service_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/service_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					
					var edit = '<a href="/settings/editservice_name/'+element.id+'" class="btn btn-success">Edit</a>';
				  row.push([++i,element.name,stat,edit]);
				});
			
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Service Active

router.get('/service_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update service_name set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/main_service');
			}
		});
	}else{
		connection.query('update service_name set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/main_service');
			}
		});
	}
	
	
});


//Service Add Page

router.get('/service_add', function (req, res) {
	res.render('settings/new_service');
});

//Service insert

router.post('/new_service',function(req,res){
	var service_name = req.body.service_name;
		var valu = [service_name];
	connection.query('insert into service_name (name) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','Service Added Successfully');
			res.redirect('/settings/main_service');
		}
	});
});

//Edit Service Page

router.get('/editservice_name/:id',function(req,res){
	connection.query('select name,id from service_name where id=?',[req.params.id], function (error, rows, fields) {
		if (error) 
		{	
			throw error;
		}else{
			res.render('settings/service_edit',{
				items: rows
			});
		}
	});
	
});

//Update Service 

router.post('/service_edit',function(req,res){
	var service_name = req.body.service_name;
	var id = req.body.bid;
	var qr = connection.query('update service_name set name=? where id=?',[service_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Service Updated Successfully');
				  res.redirect('/settings/main_service');
			  }
			  
	});	
});

//Service Subcategory

router.get('/subcategory_service',function(req,res){
	res.render('settings/service_subcategory');
});

router.post('/service_subcategory_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			var sq = 'select * from service_subcategory order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from service_subcategory order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
				results.forEach(function(element) {
					
					if(element.active_status == 1){
						var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/service_sub_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/service_sub_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					img = '<img src=/uploads/subcategory/'+element.image+' class="img-circle" width="80" height="80">';
					var edit = '<a href="/settings/editservice_sub_name/'+element.id+'" class="btn btn-success">Edit</a>';
					
					var ser = '<span id="serv'+element.id+'"></span>';
						
						ser += '<script>$(document).ready(function(){var service_id = '+element.service_name+';$.ajax({type: "POST",data: {str: service_id},url: "/settings/get_service",success: function(data){$("#serv'+element.id+'").html(data);}});});</script>';
						
				  row.push([++i,ser,element.subcategory,img,stat,edit]);
				});
			
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Service Subcategory Active

router.get('/service_sub_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update service_subcategory set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/subcategory_service');
			}
		});
	}else{
		connection.query('update service_subcategory set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/subcategory_service');
			}
		});
	}
	
	
});


//Service Subcategory Add Page

router.get('/service_sub_add', function (req, res) {
	connection.query('select * from service_name where active_status="1" order by id desc', function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				res.render('settings/new_sub_service',{
					  items : results});
				  
			  }
	});
			  
});

//Get Service Name For List Table
router.post('/get_service',function(req,res){
	var service_id = req.body.str;
	var qr = connection.query('select name from service_name where id=?', [service_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['name'];
				  res.send(opt);
			  }  
		});
});

//Get Service Subcategory Name For List Table
router.post('/get_sub_service',function(req,res){
	var subcategory_id = req.body.str;
	var qr = connection.query('select subcategory from service_subcategory where id=?', [subcategory_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['subcategory'];
				  res.send(opt);
			  }  
		});
});

//Get Work Name For List Table
router.post('/get_workname',function(req,res){
	var work_id = req.body.str;
	connection.query('select work_name from palma_work where id=?', [work_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['work_name'];
				  res.send(opt);
			  }  
		});
	
});

//Get Work Category Name For List Table
router.post('/get_work_cat',function(req,res){
	var work_category_id = req.body.str;
	connection.query('select work_category from work_category where id=?', [work_category_id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['work_category'];
				  res.send(opt);
			  }  
		});
	
});

//Service Subcategory insert

router.post('/sub_service_new',function(req,res){
	var filename;
	upload(req, res, (error) => {
			if(error){
				/* res.render('new_brand',{
					  error_msg: error
				  }); */
				  
				  throw error;
			}else{
				
				filename = req.file.filename;
				if(filename == ''){
					filename = null;
				}
			
		
				var service_name = req.body.service_name;
				var subcategory = req.body.subcategory;
				var valu = [service_name,subcategory,filename];
				connection.query('insert into service_subcategory (service_name,subcategory,image) values ?',[[valu]],function(error,results,fields){
					if(error){
						throw error;
					}else{
						req.flash('success_msg','Subcategory Added Successfully');
						res.redirect('/settings/subcategory_service');
					}
				});
			}
		});
		
});

//Edit Service Subcategory

router.get('/editservice_sub_name/:id',function(req,res){
	connection.query('select * from service_subcategory where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}/* else{ */
		connection.query('select * from service_name', function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}
			res.render('settings/sub_service_edit',{
				items: results,
				selcs: rows
			});
		});
		//}
	});
	
});

//Update Service Subcategory

router.post('/service_sub_edit',function(req,res){
	
	var filename;
	upload(req, res, (error) => {
		if(error){
			
			  throw error;
		}else{
			var ff = req.file;
			if(ff != undefined){
				filename = req.file.filename;
			}else{
				filename = req.body.bimg;
			}
			
			var service_name = req.body.service_name;
			var subcategory = req.body.subcategory;
			var id = req.body.bid;
				
			var qr = connection.query('update service_subcategory set service_name=?,subcategory=?,image=? where id=?',[service_name,subcategory,filename,id], function (error, results, fields) {
					  if (error){
						  throw error;
					  }else{
						  req.flash('success_msg','Subcategory Updated Successfully');
						  res.redirect('/settings/subcategory_service');
					  }
					  
			});
			
		}
	});
	
	
	
});

//Work Category

router.get('/work_category',function(req,res){
	res.render('settings/list_workcat');
});

router.post('/workcat_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			
			var sq = 'select * from work_category order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from work_category order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
				results.forEach(function(element) {
					
					var serv = '<span id="serv'+element.id+'"></span>';
					serv += '<script>$(document).ready(function(){var service_id = '+element.service_id+';$.ajax({type: "POST",data: {str: service_id},url: "/settings/get_service",success: function(data){$("#serv'+element.id+'").html(data);}});});</script>';

					var serv_sub = '<span id="serv_sub'+element.id+'"></span>';
					serv_sub += '<script>$(document).ready(function(){var subcategory_id = '+element.subcategory_id+';$.ajax({type: "POST",data: {str: subcategory_id},url: "/settings/get_sub_service",success: function(data){$("#serv_sub'+element.id+'").html(data);}});});</script>';

					var work_name = '<span id="work_na'+element.id+'"></span>';
					work_name += '<script>$(document).ready(function(){var work_id = '+element.work_id+';$.ajax({type: "POST",data: {str: work_id},url: "/settings/get_workname",success: function(data){$("#work_na'+element.id+'").html(data);}});});</script>';

					if(element.active_status == 1){
						var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/work_cat_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/work_cat_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					
					var edit = '<a href="/settings/editwork_cat/'+element.id+'" class="btn btn-success">Edit</a>';

					var brn = '<span id="bran'+element.id+'"></span>';
						
						brn += '<script>$(document).ready(function(){var brand_id = '+element.brand_id+';$.ajax({type: "POST",data: {str: brand_id},url: "/users/get_bran",success: function(data){$("#bran'+element.id+'").html(data);}});});</script>';
						
						var brn_ty = '<span id="bran_ty'+element.id+'"></span>';
						
						brn_ty += '<script>$(document).ready(function(){var brand_type_id = '+element.brand_type_id+';$.ajax({type: "POST",data: {brand_type_id: brand_type_id},url: "/users/get_bran_type",success: function(data){$("#bran_ty'+element.id+'").html(data);}});});</script>';

				  row.push([++i,serv,serv_sub,brn,work_name,element.work_category,stat,edit]);
				});
			
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Work Category Active

router.get('/work_cat_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update work_category set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/work_category');
			}
		});
	}else{
		connection.query('update work_category set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/work_category');
			}
		});
	}
	
	
});


//Work Category Add Page

router.get('/work_cat_add', function (req, res) {
	connection.query('select * from service_name where active_status = "1" order by id desc', function (error, results, fields) {
	  if (error) 
	  {
		  throw error;
	  }
		res.render('settings/new_work_cat',{
			  items : results
		  });
	});
});

//Work Category insert

router.post('/add_workcat',function(req,res){
	var service_name = req.body.service_name;
	var sub_name = req.body.sub_name;
	var work_name = req.body.work_name;
	var brand = req.body.brand_name;
	var work_cat = req.body.work_cat;
	var brand_name;
	if(brand == undefined){
		brand_name = '';
	}else{
		brand_name = req.body.brand_name;
	}
	
		var valu = [service_name,sub_name,brand_name,work_name,work_cat];
	connection.query('insert into work_category (service_id,subcategory_id,brand_id,work_id,work_category) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','Work Category  Added Successfully');
			res.redirect('/settings/work_category');
		}
	});
	
});

//Edit Work Category Page


router.get('/editwork_cat/:id',function(req,res){
	
	connection.query('select * from work_category where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}
		connection.query('select brand_name,id from palma_brand', function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}
			connection.query('select name,id from  service_name', function (error, btyps, fields) {
				if (error) 
				{	
					throw error;
				}
				connection.query('select subcategory,id from service_subcategory',function(err,subcat,fields){
					connection.query('select work_name,id from palma_work',function(err,work,fields){
						res.render('settings/edit_workcat',{
							items: results,
							selcs: rows,
							btyp: btyps,
							subca: subcat,
							works: work
						});
					});
				});

			});
		});
	});
	
});


//Update Work Category 

router.post('/workcat_edit',function(req,res){
	var brand_name = req.body.brand_name;
	var service_name = req.body.service_name;
	var sub_name = req.body.sub_name;
	var work_name = req.body.work_name;
	var work_cat_name = req.body.work_cat_name;
	var price = req.body.price;
	var id = req.body.bid;
	var bran;
	if(brand_name == 'undefined'){
		bran =  req.body.brand_na;
	}else{
		bran = req.body.brand_name;
	}
	
	connection.query('update work_category set service_id=?,subcategory_id=?,brand_id=?,work_id=?,work_category=? where id=?',[service_name,sub_name,bran,work_name,work_cat_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Work Category Updated Successfully');
				  res.redirect('/settings/work_category');
			  }
			  
	});	
	
});




//Price

router.get('/price',function(req,res){
	res.render('settings/list_price');
});

router.post('/price_list',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
			var val = start,end;
			
			var sq = 'select * from palma_price order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from palma_price order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				var tot = main.length;
				var i = start;
				var img;
				
				results.forEach(function(element) {
					
					var work_name = '<span id="work_cat_na'+element.id+'"></span>';
					work_name += '<script>$(document).ready(function(){var 	work_category_id  = '+element.	work_category_id+';$.ajax({type: "POST",data: {str: work_category_id},url: "/settings/get_work_cat",success: function(data){$("#work_cat_na'+element.id+'").html(data);}});});</script>';

					if(element.active_status == 1){
						var stat = '<a onclick="return cnfrmde'+element.id+'();" class="btn btn-success">Active</a><script>function cnfrmde'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/price_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
					}else{
						var stat = '<a onclick="return cnfrmac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function cnfrmac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/price_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
					}
					
					var edit = '<a href="/settings/edit_price/'+element.id+'" class="btn btn-success">Edit</a>';

					

				  row.push([++i,work_name,element.price,stat,edit]);
				});
			
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Price Active

router.get('/price_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update palma_price set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/price');
			}
		});
	}else{
		connection.query('update palma_price set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/price');
			}
		});
	}
	
	
});


//Price  Add Page

router.get('/price_add', function (req, res) {
	connection.query('select work_name,id from palma_work where active_status = "1" order by id desc', function (error, results, fields) {
		connection.query('select * from service_name where active_status = "1" order by id desc', function (error, rows, fields) {
			if (error) 
			{
				throw error;
			}
			res.render('settings/new_price',{
				items : rows
			});
		});
	});
});

//Price insert

router.post('/add_price',function(req,res){
	var service_id = req.body.service_name;
	var service_subcategory_id = req.body.sub_name;
	var brand_id = req.body.brand_name;
	var work_name = req.body.work_name;
	var work_cat = req.body.work_cat;
	var price = req.body.price;
	var max_count = req.body.max_count;
	var off = req.body.offer;
	
	if(brand_id == undefined){
		brand_id = 0;
	}
	var perc = new Array;
	for(var i=0;i<max_count;i++){
		if(max_count == 1){
			perc.push(off);
			
		}else{
			
			perc.push(off[i]);
		}
	}
	var re = JSON.stringify({items : perc});
	var sel_qry = "select * from palma_price where work_id='"+work_name+"' and work_category_id='"+work_cat+"'";
		connection.query(sel_qry,function(err,rows,fields){
			if(rows.length == '0'){
				var sq = "insert into palma_price (service_id,service_subcategory_id,brand_id,work_id,work_category_id,max_unit,offer,price) values ('"+service_id+"','"+service_subcategory_id+"','"+brand_id+"','"+work_name+"','"+work_cat+"','"+max_count+"','"+re+"','"+price+"')";
				var q = connection.query(sq,function(error,results,fields){
					if(error){
						throw error;
					}else{
						req.flash('success_msg','Price Added Successfully');
						res.redirect('/settings/price');
					}
				});
			}else{
				req.flash('success_msg','Price Already Added');
				res.redirect('/settings/price_add');
			}
		});
});


//Delete Offer
/*
router.get('/delete_offer/:id/:del_off/:offer',function(req,res){
		var id = req.params.id;
		var del_off = req.params.del_off;
		var offer = req.params.offer;
		remove_off(id,del_off,offer);
});*/

//Get Work Category
router.post('/getwork_cat',function(req,res){
	var work_id = req.body.str;
	var qr = connection.query('select work_category,id from work_category where work_id=? and active_status=? order by id desc', [work_id,1], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  
				  var num = results.length;
				  if(num == 0){
					  //var opt = '1';
					  var opt = '<option value="">--Select Any--</option>';
				  }else{
					var opt = '<option value="">--Select Any--</option>';
					results.forEach(function(element) {
					  opt += '<option value='+element.id+'>'+element.work_category+'</option>';
					});
				  }
				  res.send(opt);
			  }  
		});
});

//Edit Price Page


router.get('/edit_price/:id',function(req,res){
	
	connection.query('select * from palma_price where id=?',[req.params.id], function (error, results, fields) {
		if (error) 
		{	
			throw error;
		}
		connection.query('select brand_name,id from palma_brand', function (error, br_rows, fields) {
			if (error) 
			{	
				throw error;
			}
			connection.query('select name,id from  service_name', function (error, btyps, fields) {
				if (error) 
				{	
					throw error;
				}
				connection.query('select subcategory,id from service_subcategory',function(err,subcat,fields){
					connection.query('select work_category,id from work_category where active_status="1"', function (error, rows, fields) {
						if (error) 
						{	
							throw error;
						}
						connection.query('select work_name,id from palma_work where active_status="1"',function(error,works,fileds){
							res.render('settings/edit_price',{
								items: results,
								selcs: rows,
								work: works,
								btyp: btyps,
								subca: subcat,
								brands: br_rows

							});
						});
					});
				});
			});
		});
	});
	
});

//Delete Maxunit and Offer JSON

router.post('/del_unit',function(req,res){
	var index_val = req.body.str;
	var id = req.body.id;
	var sql = "select offer,max_unit from palma_price where id='"+id+"'";
	connection.query(sql,function(err,rows,fields){
		var offr = rows[0]['offer'];
		var max_unit = rows[0]['max_unit'];
		var o = JSON.parse(offr);
		var o_array = o.items;
		var el = o.items[index_val];
		var el_index = o_array.indexOf(el);
		
		if(el_index != -1){
			var re = o_array.splice(el_index,1);
		}
	
		if(re){
			var jso = JSON.stringify(o);
			var units = max_unit - 1;
			var sq = "update palma_price set offer='"+jso+"',max_unit='"+units+"' where id='"+id+"'";
			connection.query(sq,function(error,results,fields){
				res.send('1');
			});
		}else{
			res.send('0');
		}
	
	}); 
	
});

//Update Price 

router.post('/price_edit',function(req,res){
	var service_id = req.body.service_name;
	var service_subcategory_id = req.body.sub_name;
	var brand_id = req.body.brand_name;
	var work_name = req.body.work_name;
	var work_cat = req.body.work_cat;
	var price = req.body.price;
	var max_count = req.body.max_count;
	var id = req.body.bid;
	var max_count = req.body.max_count;
	var off = req.body.offer;
	
	if(brand_id == 'undefined'){
		brand_id = req.body.brand_na;
	}else{
		brand_id = req.body.brand_name;
	}
	var perc = new Array;
	for(var i=0;i<max_count;i++){
		if(max_count == 1){
			perc.push(off);
			
		}else{
			
			perc.push(off[i]);
		}
	}
	var re = JSON.stringify({items : perc});

	connection.query('update palma_price set service_id=?,service_subcategory_id=?,brand_id=?,work_id=?,work_category_id=?,max_unit=?,offer=?,price=? where id=?',[service_id,service_subcategory_id,brand_id,work_name,work_cat,max_count,re,price,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Price Updated Successfully');
				  res.redirect('/settings/price');
			  }
			  
	});
	
	
});

//State List

router.get('/state', function (req, res) {
	res.render('settings/state_list');
});

//State Ajax Table
router.post('/state_ajax',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
	
			var sq = 'select state_name,id,active_status from state order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
			connection.query('select state_name,id,active_status from state order by id',function(error,main,fields){
				var row = new Array;
				var tot = main.length;
				var i = start;
				
					results.forEach(function(element) {
						
						var mod = '<a href="/settings/edit_state/'+element.id+'"><i class="fa fa-edit fa-2x"></i></a><!--<a onclick="return cnfrm'+element.id+'();"><i class="fa fa-trash fa-2x"></i></a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/state_delete/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';
						

						if(element.active_status == 1){
							var stat = '<a onclick="return state_de'+element.id+'();" class="btn btn-success">Active</a><script>function state_de'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/state_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
						}else{
							var stat = '<a onclick="return state_ac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function state_ac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/state_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
						}
						
							row.push([++i,element.state_name,mod,stat]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


//State Add Page

router.get('/state_add', function (req, res) {
	res.render('settings/new_state');
});

//state insert

router.post('/new_state',function(req,res){
	var state_name = req.body.state_name;
		var valu = [state_name];
	connection.query('insert into state (state_name) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','State Added Successfully');
			res.redirect('/settings/state');
		}
	});
});

//Edit State Page

router.get('/edit_state/:id',function(req,res){
	connection.query('select state_name,id from state where id=?',[req.params.id], function (error, rows, fields) {
		if (error) 
		{	
			throw error;
		}else{
			res.render('settings/state_edit',{
				items: rows
			});
		}
	});
	
});

//Update State 

router.post('/state_edit',function(req,res){
	var state_name = req.body.state_name;
	var id = req.body.bid;
	var qr = connection.query('update state set state_name=? where id=?',[state_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','State Updated Successfully');
				  res.redirect('/settings/state');
			  }
			  
	});	
});


//State Delete

router.get('/state_delete/:id',function(req,res){
	var id = req.params.id;
	connection.query('delete from state where id=?',[id], function (error, results, fields) {
			connection.query('delete from city where stateid=?',[id],function(err,cit,fields){
				req.flash('success_msg','Deleted Successfully');
				  res.redirect('/settings/state');
			});
			 
		});
	
});



//State Active

router.get('/state_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update state set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/state');
			}
		});
	}else{
		connection.query('update state set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/state');
			}
		});
	}
	
	
});


//Commission List

router.get('/commission', function (req, res) {
	res.render('settings/commission_list');
});

//Commission Ajax Table
router.post('/commission_ajax',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
	
			var sq = 'select commission_amount,id from commission order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
			connection.query('select commission_amount,id from commission order by id',function(error,main,fields){
				var row = new Array;
				var tot = main.length;
				var i = start;
				
					results.forEach(function(element) {
						
						var mod = '<a href="/settings/edit_commission/'+element.id+'"><i class="fa fa-edit fa-2x"></i></a>';
						var commission_amount = element.commission_amount + '(%)';
							row.push([++i,commission_amount,mod]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


//Commission Add Page

router.get('/commission_add', function (req, res) {
	res.render('settings/new_commission');
});

//state insert

router.post('/new_commission',function(req,res){
	var commission_amount = req.body.commission_amount;
		var valu = [commission_amount];
	connection.query('insert into commission (commission_amount) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','Commission Added Successfully');
			res.redirect('/settings/commission');
		}
	});
});

//Edit Commission Page

router.get('/edit_commission/:id',function(req,res){
	connection.query('select commission_amount,id from commission where id=?',[req.params.id], function (error, rows, fields) {
		if (error) 
		{	
			throw error;
		}else{
			res.render('settings/commission_edit',{
				items: rows
			});
		}
	});
	
});

//Update Commission 

router.post('/commission_edit',function(req,res){
	var commission_amount = req.body.commission_amount;
	var id = req.body.bid;
	var qr = connection.query('update commission set commission_amount=? where id=?',[commission_amount,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Commission Updated Successfully');
				  res.redirect('/settings/commission');
			  }
			  
	});	
});


//City List

router.get('/city', function (req, res) {
	res.render('settings/city_list');
});

//City Ajax Table
router.post('/city_ajax',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
	
			var sq = 'select city,id,stateid,active_status from city order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
			connection.query('select city,id,stateid,active_status from city order by id desc',function(error,main,fields){
				var row = new Array;
				var tot = main.length;
				var i = start;
				
					results.forEach(function(element) {
						
						var mod = '<a href="/settings/edit_city/'+element.id+'"><i class="fa fa-edit fa-2x"></i></a><!--<a onclick="return cnfrm'+element.id+'();"><i class="fa fa-trash fa-2x"></i></a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/city_delete/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';
						
						var s_name = '<span id="sta'+element.id+'"></span>';
						
						s_name += '<script>$(document).ready(function(){var stateid = '+element.stateid+';$.ajax({type: "POST",data: {str: stateid},url: "/settings/get_stat",success: function(data){$("#sta'+element.id+'").html(data);}});});</script>';

						if(element.active_status == 1){
							var stat = '<a onclick="return city_de'+element.id+'();" class="btn btn-success">Active</a><script>function city_de'+element.id+'() {swal({ title: "Do You Want To Deactivate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/city_active/'+element.id+'/de";} else {swal("Cancelled", " ", "error");}});}</script>';
						}else{
							var stat = '<a onclick="return city_ac'+element.id+'();" class="btn btn-danger">Deactive</a><script>function city_ac'+element.id+'() {swal({ title: "Do You Want To Activate?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/city_active/'+element.id+'/ac";} else {swal("Cancelled", " ", "error");}});}</script>';
						}

							row.push([++i,s_name,element.city,mod,stat]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//City Active

router.get('/city_active/:id/:cmd',function(req,res){
	if(req.params.cmd == 'de'){
		connection.query('update city set active_status=? where id=?',[0,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/city');
			}
		});
	}else{
		connection.query('update city set active_status=? where id=?',[1,req.params.id], function (error, rows, fields) {
			if (error) 
			{	
				throw error;
			}else{
				res.redirect('/settings/city');
			}
		});
	}
	
	
});

//Get State Name For List Table
router.post('/get_stat',function(req,res){
	var stateid = req.body.str;
	connection.query('select state_name from state where id=?', [stateid], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  var opt = results[0]['state_name'];
				  res.send(opt);
			  }  
		});
});

//City Add Page

router.get('/city_add', function (req, res) {
	connection.query('select state_name,id from state',function(error,results,fields){
		res.render('settings/new_city',{
			items: results
		});
	});
	
});

//City insert

router.post('/new_city',function(req,res){
	var state_name = req.body.state_name;
	var city_name = req.body.city_name;
		var valu = [city_name,state_name];
	connection.query('insert into city (city,stateid) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','City Added Successfully');
			res.redirect('/settings/city');
		}
	});
});

//Edit City Page

router.get('/edit_city/:id',function(req,res){
	connection.query('select city,stateid,id from city where id=?',[req.params.id], function (error, rows, fields) {
			
		connection.query('select state_name,id from state',function(error,results,fields){
			res.render('settings/city_edit',{
				items: rows,
				stats: results
			});
		});
	});
	
});

//Update City 

router.post('/city_edit',function(req,res){
	var state_name = req.body.state_name;
	var city_name = req.body.city_name;
	var id = req.body.bid;
	connection.query('update city set city=?,stateid=? where id=?',[city_name,state_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','City Updated Successfully');
				  res.redirect('/settings/city');
			  }
			  
	});	
	
});


//City Delete

router.get('/city_delete/:id',function(req,res){
	var id = req.params.id;
	connection.query('delete from city where id=?',[id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','Deleted Successfully');
				  res.redirect('/settings/city');
			  }  
		});
	
});


//Complaint List

router.get('/complaint', function (req, res) {
	res.render('settings/complaint_setting');
});

//Complaint Ajax Table
router.post('/complaint_ajax',function(req,res){
	
	params = req.body;
	
	var start = params.start;
	var end = params.length;
	
			var sq = 'select * from complaints order by id desc LIMIT '+start+','+end+'';
	connection.query(sq,function (error, results, fields) {
			connection.query('select * from complaints order by id',function(error,main,fields){
				var row = new Array;
				var tot = main.length;
				var i = start;
				
					results.forEach(function(element) {
						
						var mod = '<a href="/settings/edit_complaint/'+element.id+'"><i class="fa fa-edit fa-2x"></i></a><!--<a onclick="return cnfrm'+element.id+'();"><i class="fa fa-trash fa-2x"></i></a><script>function cnfrm'+element.id+'() {swal({ title: "Are You Sure?",type: "info",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false},function(isConfirm){if (isConfirm) {window.location.href="/settings/complaint_delete/'+element.id+'";} else {swal("Cancelled", " ", "error");}});}</script>-->';
						
							row.push([++i,element.complaint_name,mod]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

//Complaint Add Page

router.get('/complaint_add', function (req, res) {
	
		res.render('settings/new_complaint');

});

//Complaint insert

router.post('/new_complaint',function(req,res){
	var complaint_name = req.body.complaint_name;
		var valu = [complaint_name];
	connection.query('insert into complaints (complaint_name) values ?',[[valu]],function(error,results,fields){
		if(error){
			throw error;
		}else{
			req.flash('success_msg','Complaint Added Successfully');
			res.redirect('/settings/complaint');
		}
	});
});

//Edit Complaint Page

router.get('/edit_complaint/:id',function(req,res){
	connection.query('select * from complaints where id=?',[req.params.id], function (error, rows, fields) {
		
		res.render('settings/complaint_edit',{
			items: rows
		});
		
	});
	
});

//Update Complaint 

router.post('/complaint_edit',function(req,res){
	var complaint_name = req.body.complaint_name;
	var id = req.body.bid;
	connection.query('update complaints set complaint_name=? where id=?',[complaint_name,id], function (error, results, fields) {
			  if (error){
				  throw error;
			  }else{
				  req.flash('success_msg','Complaint Updated Successfully');
				  res.redirect('/settings/complaint');
			  }
			  
	});	
});

//Complaint Delete

router.get('/complaint_delete/:id',function(req,res){
	var id = req.params.id;
	connection.query('delete from complaints where id=?',[id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  req.flash('success_msg','Deleted Successfully');
				  res.redirect('/settings/complaint');
			  }  
		});
	
});
module.exports = router;