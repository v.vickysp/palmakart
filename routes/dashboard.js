var express = require('express');
var router = express.Router();
var connection = require('./db');
var path = require('path');


//Dashboard Ajax 
router.post('/dashboard_ajax',function(req,res){
	var num_com_job;
	var num_can_job;
	connection.query('select * from customer_register', function (error, results, fields) {
		var num_cus = results.length;
		connection.query('select * from service_company_register', function(err, com, fields){
				var num_comp = com.length;
			connection.query('select * from palma_complaints', function(errs, complaints,fields){
					var num_complaint = complaints.length;
					var sql = 'select count(*) as tot, sum(CASE WHEN status="Completed" THEN 1 ELSE 0 END) as com, sum(CASE WHEN status="Cancel" THEN 1 ELSE 0 END) as canc,sum(CASE WHEN status="Pending" THEN 1 ELSE 0 END) as pend,sum(CASE WHEN status="Ongoing" THEN 1 ELSE 0 END) as ongoing,sum(CASE WHEN status="Processing" THEN 1 ELSE 0 END) as process from service_booking';
				connection.query(sql, function(er,jobs,fields){
						var num_jobs = jobs[0]['tot'];
							if(jobs[0]['com'] == null){
								num_com_job = 0;
							}else{
								num_com_job = jobs[0]['com'];
							}
							
							if(jobs[0]['canc'] == null){
								num_can_job =0;
							}else{
								num_can_job =jobs[0]['canc'];
							}
							
							if(jobs[0]['pend'] == null){
								num_pend_job =0;
							}else{
								num_pend_job =jobs[0]['pend'];
							}

							if(jobs[0]['ongoing'] == null){
								num_ongoing_job =0;
							}else{
								num_ongoing_job =jobs[0]['ongoing'];
							}

							if(jobs[0]['process'] == null){
								num_process_job =0;
							}else{
								num_process_job =jobs[0]['process'];
							}
							
						res.send(JSON.stringify({num_cus: num_cus,num_comp: num_comp,num_complaint: num_complaint,num_jobs:num_jobs,num_com_job: num_com_job,num_can_job: num_can_job,num_pend_job : num_pend_job,num_ongoing_job,num_process_job}));
				});
			});
		});
				  
	});

});


module.exports = router;