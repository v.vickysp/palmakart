var express = require('express');
var router = express.Router();
var connection = require('./db');
var dateFormat = require('dateformat');
var mydate = require('current-date');
var DateDiff = require('date-diff');
var downloadCSV = require('download-csv');
var csv = require('csv-express');

// Customer Report Part
router.get('/customer_report',function(req,res){
    var sel_customer = "select customer_name,id from customer_register order by id desc";
    connection.query(sel_customer,function(errors,results,fields){
        
        res.render('report/customer_report_list',{
            items: results
        });
    });
    
});

//Customer List
router.post('/customer_ajax',function(req,res){
	
	params = req.body;
    var customer_id = req.body.customer_id;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(customer_id != 0 && customer_id != undefined){
        where += " and id='"+customer_id+"'";
    }
    if(from_date != 0 && to_date == 0 && customer_id == 0){
        where += " and reg_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0 && customer_id == 0){
        where += " and reg_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && customer_id == 0){
        where += " and reg_date between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && customer_id != 0 && from_date != undefined && to_date != undefined && customer_id != undefined){
        where += " and reg_date between '"+from_date+"' and '"+to_date+"' and id='"+customer_id+"'";
    }
    //Filter Area Query Ends Here

	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
            //console.log(params[search][value]);
            if(where == ''){
                var sq = 'select * from customer_register order by id desc LIMIT '+start+','+end+'';
            }else{
                var sq = 'select * from customer_register where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
            }
            
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from customer_register order by id desc',function(error,main,fields){
				
			
                var row = new Array;
                if(where == ''){
                    var tot = main.length;
                }else{
                    var tot = results.length;
                }
				
				var i = start;
				var img;
				
					results.forEach(function(element) {
						var reg_date = dateFormat(element.reg_date,'dd-mm-yyyy');
						row.push([++i,reg_date,element.customer_name,element.mobile_number]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});

router.get('/customer_download/:customer_id/:from_date/:to_date',function(req,res){
    var customer_id = req.params.customer_id;
    var from_date   = req.params.from_date;
    var to_date     = req.params.to_date;
    var where       = '';
   
        if(customer_id != 0 ){
            where += " and customer_register.id='"+customer_id+"'";
        }
        if(from_date != 0 && to_date == 0 && customer_id == 0){
            where += " and customer_register.reg_date >= '"+from_date+"'";
        }
        if(from_date == 0 && to_date != 0 && customer_id == 0){
            where += " and customer_register.reg_date <= '"+to_date+"'";
        }
        if(from_date != 0 && to_date != 0 && customer_id == 0){
            where += " and customer_register.reg_date between '"+from_date+"' and '"+to_date+"'";
        }
        if(from_date != 0 && to_date != 0 && customer_id != 0){
            where += " and customer_register.reg_date between '"+from_date+"' and '"+to_date+"' and id='"+customer_id+"'";
        }
  
    if(where == ''){
        var sel_cus = "select customer_register.customer_name,customer_register.contact_name,customer_register.address,customer_register.pincode,customer_register.contact_number,customer_register.mobile_number,customer_register.email,customer_register.reg_date,customer_register.reg_date_time,customer_register.account_number,customer_register.bank_name,customer_register.branch_name,customer_register.ifsc_code,customer_register.cheque,customer_register.updated_date,customer_register.updated_datetime,city.city,state.state_name from customer_register INNER JOIN city ON city.id=customer_register.city INNER JOIN state ON state.id=customer_register.state order by customer_register.id desc";
    }else{
        var sel_cus = "select customer_register.customer_name,customer_register.contact_name,customer_register.address,customer_register.pincode,customer_register.contact_number,customer_register.mobile_number,customer_register.email,customer_register.reg_date,customer_register.reg_date_time,customer_register.account_number,customer_register.bank_name,customer_register.branch_name,customer_register.ifsc_code,customer_register.cheque,customer_register.updated_date,customer_register.updated_datetime,city.city,state.state_name from customer_register INNER JOIN city ON city.id=customer_register.city INNER JOIN state ON state.id=customer_register.state where 1 "+where+" order by customer_register.id desc";
    }
    
    connection.query(sel_cus,function(errors,results,fields){
       var filename = 'customer_register.csv';
       
       var results_array    = new Array;
       
        results.forEach(function(element){
            var customer_name         = element.customer_name;
            var contact_name          = element.contact_name;
            var address               = element.address;
            var pincode               = element.pincode;
            var contact_number        = element.contact_number;
            var mobile_number         = element.mobile_number;
            var email                 = element.email;
            var created_date          = dateFormat(element.reg_date,'dd-mm-yyyy');
            var cheque_name           = element.cheque;
            var bank_name             = element.bank_name;
            var account_number        = element.account_number;
            var branch_name           = element.branch_name;
            var ifsc_code             = element.ifsc_code;
            var city                  = element.city;
            var state                 = element.state_name;
            results_array.push({customer_name,contact_name,address,pincode,contact_number,mobile_number,email,created_date,cheque_name,bank_name,account_number,branch_name,ifsc_code,city,state});
            
        });

        res.statusCode = 200;

        res.setHeader('Content-Type', 'text/csv');

        res.setHeader("Content-Disposition", 'attachment; filename='+filename);

       res.csv(results_array,true);
        
    });
});
// Customer Report Part Ends Here

//Company Report Part

router.get('/company_report',function(req,res){
    var sel_customer = "select company_name,id,user_id from service_company_register order by id desc";
    connection.query(sel_customer,function(errors,results,fields){
        res.render('report/company_report_list',{
            items : results
        });
    });
});



router.post('/member_ajax',function(req,res){

    params = req.body;

    var company_id = req.body.company_id;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(company_id != 0 && company_id != undefined){
        where += " and id='"+company_id+"'";
    }
    if(from_date != 0 && to_date == 0 && company_id == 0){
        where += " and created_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0 && company_id == 0){
        where += " and created_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id == 0){
        where += " and created_date between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id != 0 && from_date != undefined && to_date != undefined && company_id != undefined){
        where += " and created_date between '"+from_date+"' and '"+to_date+"' and id='"+company_id+"'";
    }
    //Filter Area Query Ends Here

    var start = params.start;
    var end = params.length;
    var val = start,end;
    if(where == ''){
        var sq = 'select * from service_company_register order by id desc LIMIT '+start+','+end+'';
    }else{
        var sq = 'select * from service_company_register where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
    }
    connection.query(sq,function (error, results, fields) {
        connection.query('select * from service_company_register order by id desc',function(error,main,fields){	
            var row = new Array;
            if(where == ''){
                var tot = main.length;
            }else{
                var tot = results.length;
            }
            var i = start;
            var img;
            results.forEach(function(element) {
                if(element.active_status == '1'){
                    var status = 'Active';
                }else{
                    var status = 'Deactive';
                }
                var reg_date = dateFormat(element.created_date,'dd-mm-yyyy');
                row.push([++i,element.company_name,reg_date,element.user_id,element.type,element.mobile_number,status]);
            });
                if (error) 
                {
                        throw error;
                }else{
                    
                        res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
                }
        });			  
    });
});

router.get('/company_download/:company_id/:from_date/:to_date',function(req,res){
    var company_id = req.params.company_id;
    var from_date   = req.params.from_date;
    var to_date     = req.params.to_date;
    var where = '';

    //Filter Area
    if(company_id != 0 ){
        where += " and service_company_register.id='"+company_id+"'";
    }
    if(from_date != 0 && to_date == 0 && company_id == 0){
        where += " and service_company_register.created_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0 && company_id == 0){
        where += " and service_company_register.created_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id == 0){
        where += " and service_company_register.created_date between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id != 0 ){
        where += " and service_company_register.created_date between '"+from_date+"' and '"+to_date+"' and id='"+company_id+"'";
    }
    //Filter Area Query Ends Here
    if(where == ''){
        var sel_cus = "select service_company_register.user_id,service_company_register.type,service_company_register.company_name,service_company_register.contact_name,service_company_register.address,service_company_register.pincode,service_company_register.contact_number,service_company_register.mobile_number,service_company_register.email,service_company_register.created_date,service_company_register.cheque_name,service_company_register.bank_name,service_company_register.account_number,service_company_register.branch_name,service_company_register.ifsc_code,service_company_register.experience,service_company_register.description,service_company_register.active_status,service_company_register.allot_job,service_company_register.accept_count,service_company_register.cancel_count,city.city,state.state_name from service_company_register INNER JOIN city ON service_company_register.city = city.id INNER JOIN state ON service_company_register.state = state.id order by service_company_register.id desc";
    }else{
        var sel_cus = "select service_company_register.user_id,service_company_register.type,service_company_register.company_name,service_company_register.contact_name,service_company_register.address,service_company_register.pincode,service_company_register.contact_number,service_company_register.mobile_number,service_company_register.email,service_company_register.created_date,service_company_register.cheque_name,service_company_register.bank_name,service_company_register.account_number,service_company_register.branch_name,service_company_register.ifsc_code,service_company_register.experience,service_company_register.description,service_company_register.active_status,service_company_register.allot_job,service_company_register.accept_count,service_company_register.cancel_count,city.city,state.state_name from service_company_register INNER JOIN city ON service_company_register.city = city.id INNER JOIN state ON service_company_register.state = state.id where 1 "+where+" order by service_company_register.id desc";
    }
   
    
    connection.query(sel_cus,function(errors,results,fields){
       var filename = 'member_register.csv';
        
       var results_array    = new Array;
       
        results.forEach(function(element){
            var user_id         = element.user_id;
            var type            = element.type;
            var company_name    = element.company_name;
            var contact_name    = element.contact_name;
            var address         = element.address;
            var pincode         = element.pincode;
            var contact_number  = element.contact_number;
            var mobile_number   = element.mobile_number;
            var email           = element.email;
            var created_date    = dateFormat(element.created_date,'dd-mm-yyyy');
            var cheque_name     = element.cheque_name;
            var bank_name       = element.bank_name;
            var account_number  = element.account_number;
            var branch_name     = element.branch_name;
            var ifsc_code       = element.ifsc_code;
            var experience      = element.experience;
            var description     = element.description;
            if(element.active_status == '1'){
                var active_status   = 'Active';
            }else{
                var active_status   = 'Deactive';
            }
            var allot_job       = element.allot_job;
            var accept_count    = element.accept_count;
            var cancel_count    = element.cancel_count;
            var city            = element.city;
            var state           = element.state_name;
            

            results_array.push({user_id,type,company_name,contact_name,address,pincode,contact_number,mobile_number,email,created_date,cheque_name,bank_name,account_number,branch_name,ifsc_code,experience,description,active_status,allot_job,accept_count,cancel_count,city,state});
            
        });
        
        res.statusCode = 200;

        res.setHeader('Content-Type', 'text/csv');

        res.setHeader("Content-Disposition", 'attachment; filename='+filename);

       res.csv(results_array,true);
        
    });
});

//Company Report Part Ends Here

//Order History Report

router.get('/order_report',function(req,res){
    var sel_customer = "select company_name,id,user_id from service_company_register order by id desc";
    connection.query(sel_customer,function(errors,results,fields){
        var job_no = "select job_no from service_booking order by id desc";
        connection.query(job_no,function(errors,rows,fields){
            res.render('report/order_report_list',{
                items: results,
                job: rows
            });
        });
    });
});

router.post('/jobreview_ajax',function(req,res){
	
    params = req.body;
    
    var company_id = req.body.company_id;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(company_id != 0 && company_id != undefined){
        where += " and service_company_id='"+company_id+"'";
    }
    if(from_date != 0 && to_date == 0 && company_id == 0){
        where += " and created_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0 && company_id == 0){
        where += " and created_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id == 0){
        where += " and created_date between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id != 0 && from_date != undefined && to_date != undefined && company_id != undefined){
        where += " and created_date between '"+from_date+"' and '"+to_date+"' and service_company_id='"+company_id+"'";
    }
    //Filter Area Query Ends Here
    
	var start = params.start;
	var end = params.length;
    var val = start,end;
    
        if(where == ''){
            var sq = 'select * from service_booking order by id desc LIMIT '+start+','+end+'';
        }else{
            var sq = 'select * from service_booking where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
        }
			
	connection.query(sq,function (error, results, fields) {
    
        connection.query('select * from service_booking',function(error,main,fields){
            
        
            var row = new Array;
            if(where == ''){
                var tot = main.length;
            }else{
                var tot = results.length;
            }
            
            var i = start;
            var img;
            
                results.forEach(function(element) {
                    
                    var job = '<span id="job'+element.id+'"></span>';
                    
                    job += '<script>$(document).ready( function () {var id = '+element.work_category_id+';$.ajax({type: "POST",data: {str: id},url: "/job_review/get_job",success: function(data) {var sillyString = data.substr(1).slice(0,-1);var obj = JSON.parse(sillyString);var job_name = obj.work_category;$("#job'+element.id+'").html(job_name);} }); });';
                    
                    var cus = '<span id="cus'+element.id+'"></span>';
                    
                    cus += '<script>$(document).ready( function () {var id = '+element.customer_id+';$.ajax({type: "POST",data: {str: id},url: "/job_review/get_customer",success: function(data) {var sillyString = data.substr(1).slice(0,-1);var obj = JSON.parse(sillyString);var customer_name = obj.customer_name;$("#cus'+element.id+'").html(customer_name);} }); });';
                    
                    if(element.status == 'Completed'){
                        var stat = '<div class="label label-success">Completed</div>';
                    }else if(element.status == 'Processing'){
                        var stat = '<div class="label label-primary">In Progress</div>';
                    }else if(element.status == 'Cancel'){
                        var stat = '<div class="label label-danger">Cancel</div>';
                    }else if(element.status == 'Ongoing'){
                        var stat = '<div class="label label-danger">OnGoing</div>';
                    }else if(element.status == 'Pending'){
                        var stat = '<div class="label label-danger">Pending</div>';
                    }
                    
                    if(element.status == 'Cancel'){
                        
                        var canc = '<p><span><b>Cancel Date : </b></span> <span id="cancel_date'+element.id+'"></span></p>';
                        
                        canc+='<script>$(document).ready( function () {var id = '+element.id+';var type="cancel";$.ajax({type: "POST",data: {str: id,type: type},url: "/job_review/get_booking",success: function(data) {var obj = JSON.parse(data);var cancel_date = obj.cancel_date;';
                        
                        canc+='$("#cancel_date'+element.id+'").html(cancel_date);';
                        
                        canc+=' } });});</script>';
                    
                    }else{
                        var canc = '<p style="font-size: 18px;color: red;"><b>No Details</b></p>';
                    }

                    var booking_date = dateFormat(element.date_time,'dd-mm-yyyy');
                    row.push([++i,element.job_no,booking_date,job,cus,stat,canc]);
                });
                    if (error) 
                    {
                        throw error;
                    }else{
                        
                        res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
                    }
        });			  
	});
		
	
});


router.get('/order_download/:company_id/:from_date/:to_date',function(req,res){

    var company_id = req.params.company_id;
    var from_date   = req.params.from_date;
    var to_date     = req.params.to_date;
    var where = '';
    
    //Filter Area
    if(company_id != 0 && company_id != undefined){
        where += " and service_booking.service_company_id='"+company_id+"'";
    }
    if(from_date != 0 && to_date == 0 && company_id == 0){
        where += " and service_booking.date_time >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0 && company_id == 0){
        where += " and service_booking.date_time <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id == 0){
        where += " and service_booking.date_time between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && company_id != 0 && from_date != undefined && to_date != undefined && company_id != undefined){
        where += " and service_booking.date_time between '"+from_date+"' and '"+to_date+"' and service_booking.service_company_id='"+company_id+"'";
    }
    //Filter Area Query Ends Here

    if(where == ''){
        var sel_cus = "select service_booking.job_no,service_booking.work_date,service_booking.work_address,service_booking.work_time,service_booking.max_count,service_booking.cancel_date,service_booking.offer,service_booking.offer_amount,service_booking.total_amount,service_booking.status,service_booking.sub_total,service_booking.payment_status,service_booking.payment_mode,city.city,state.state_name,customer_register.customer_name from service_booking INNER JOIN customer_register ON customer_register.id = service_booking.customer_id  INNER JOIN city ON city.id = service_booking.city INNER JOIN state ON state.id = service_booking.state order by service_booking.id desc";
    }else{
        var sel_cus = "select service_booking.job_no,service_booking.work_date,service_booking.work_address,service_booking.work_time,service_booking.max_count,service_booking.cancel_date,service_booking.offer,service_booking.offer_amount,service_booking.total_amount,service_booking.status,service_booking.sub_total,service_booking.payment_status,service_booking.payment_mode,city.city,state.state_name,customer_register.customer_name from service_booking INNER JOIN customer_register ON customer_register.id = service_booking.customer_id  INNER JOIN city ON city.id = service_booking.city INNER JOIN state ON state.id = service_booking.state where 1 "+where+" order by service_booking.id desc";
    }
    
    
    connection.query(sel_cus,function(errors,results,fields){
        if(errors){
            throw errors;
        }
       var filename = 'order_history_report.csv';
        var results_array    = new Array;
        results.forEach(function(element){
            var job_no          = element.job_no;
            var work_date       = element.work_date;
            var work_address    = element.work_address;
            var work_time       = element.work_time;
            var max_count       = element.max_count;
            var cancel_date     = element.cancel_date;
            if(cancel_date != '0000-00-00'){
                var cancel_conv_date = dateFormat(cancel_date,'dd-mm-yyyy');
            }
            var offer           = element.offer;
            var offer_amount    = element.offer_amount;
            var total_amount    = element.total_amount;
            var status          = element.status;
            var sub_total       = element.sub_total;
            var payment_status  = element.payment_status;
            var payment_mode    = element.payment_mode;
            var city            = element.city;
            var state           = element.state_name;
            var customer_name   = element.customer_name;
            results_array.push({job_no,work_date,work_address,work_time,max_count,cancel_conv_date,offer,offer_amount,total_amount,status,sub_total,payment_status,payment_mode,city,state,customer_name});
        });
        res.statusCode = 200;

        res.setHeader('Content-Type', 'text/csv');

        res.setHeader("Content-Disposition", 'attachment; filename='+filename);

       res.csv(results_array,true);
        
    });
});

//Order History Report Ends Here

//Refund Report 
router.get('/refund_report',function(req,res){
    var sel_customer = "select customer_name,id from customer_register order by id desc";

	connection.query(sel_customer,function(errors,results,fields){
        res.render('report/refund_report_list',{
            items: results
        });
    });
});


router.post('/refund_ajax',function(req,res){
	
	params = req.body;
	var customer = req.body.customer;
    var where = '';
    
    //Filter Area
    if(customer != '' && customer != undefined){
        where += " and customer_id='"+customer+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			
			if(where == ''){
				var sq = 'select * from wallet order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from wallet where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from wallet order by id desc',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				var i = start;
				var img;
                var total_count = 0;
					results.forEach(function(element) {
                        var create_date = dateFormat(element.create_date,'d-mm-yyyy');
                        row.push([++i,create_date,element.booking_id,element.job_name,element.customer_name,element.amount]);
                        total_count++;
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": total_count,"recordsFiltered": total_count}));
					  }
			});			  
		});
		
	
});

//Refund Download CSV

    
router.get('/refund_download/:customer',function(req,res){
    var customer = req.params.customer;
    var where = '';
    
    //Filter Area
    if(customer != 0 && customer != undefined){
        where += " and customer_id='"+customer+"'";
    }
    var filename = 'refund_history_report.csv';
    if(where == ''){
        var sq = 'select * from wallet order by id desc';
    }else{
        var sq = 'select * from wallet where 1 '+where+' order by id desc';
    }
    
	connection.query(sq,function (error, results, fields) {
            var row2 = new Array;
            var img;
            var i=0;
            
            results.forEach(function(element) {
                var create_date = dateFormat(element.create_date,'dd-mm-yyyy');
                var booking_id = element.booking_id;
                var job_name = element.job_name;
                var customer_name = element.customer_name;
                var amount = element.amount;
                row2.push({create_date,booking_id,job_name,customer_name,amount});
            });

            res.statusCode = 200;

            res.setHeader('Content-Type', 'text/csv');
    
            res.setHeader("Content-Disposition", 'attachment; filename='+filename);
    
           res.csv(row2,true);
    });
});

//Refund Report Ends Here

//Commission History Report

router.get('/commission_history',function(req,res){
    res.render('report/commission_history_list');
});

//Commission Ajax Table Values
router.post('/commission_ajax',function(req,res){

    params = req.body;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(from_date != 0 && to_date == 0 ){
        where += " and transaction_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0){
        where += " and transaction_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && from_date != undefined && to_date != undefined){
        where += " and transaction_date between '"+from_date+"' and '"+to_date+"'";
    }
    //Filter Area Query Ends Here

    var start = params.start;
    var end = params.length;
    var val = start,end;
    if(where == ''){
        var sq = 'select * from  commission_payment_history order by id desc LIMIT '+start+','+end+'';
    }else{
        var sq = 'select * from  commission_payment_history where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
    }
    
    connection.query(sq,function (error, results, fields) {
       
            var main_sq = 'select * from  commission_payment_history order by id desc';
       
        connection.query(main_sq,function(error,main,fields){	
            var row = new Array;
            if(where == ''){
                var tot = main.length;
            }else{
                var tot = results.length;
            }
            
            var i = start;
            var img;
            results.forEach(function(element) {
               var created_date = dateFormat(element.transaction_date,'dd-mm-yyyy');
                row.push([++i,created_date,element.booking_id,element.company_name,element.commission_to_get,element.commission_to_pay]);
            });
                if (error) 
                {
                        throw error;
                }else{
                    
                        res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
                }
        });			  
    });
});

//Commission Download CSV
router.get('/commission_download/:from_date/:to_date',function(req,res){
    var from_date   = req.params.from_date;
    var to_date     = req.params.to_date;
    var where = '';
    
    //Filter Area
    if(from_date != 0 && to_date == 0 ){
        where += " and transaction_date >= '"+from_date+"'";
    }
    if(from_date == 0 && to_date != 0){
        where += " and transaction_date <= '"+to_date+"'";
    }
    if(from_date != 0 && to_date != 0 && from_date != undefined && to_date != undefined){
        where += " and transaction_date between '"+from_date+"' and '"+to_date+"'";
    }
    //Filter Area Query Ends Here

    if(where == ''){
        var sel_cus = "select * from  commission_payment_history order by id desc";
    }else{
        var sel_cus = "select * from  commission_payment_history where 1 "+where+" order by id desc";
    }
    
    connection.query(sel_cus,function(errors,results,fields){
        if(errors){
            throw errors;
        }
       var filename = 'commission_history_report.csv';
        var results_array    = new Array;
        results.forEach(function(element){
            var job_no               = element.booking_id;
            var company_name            = element.company_name;
            var commission_received  = element.commission_to_get;
            var commission_pay       = element.commission_to_pay;
            var transaction_date     = dateFormat(element.transaction_date,'dd-mm-yyyy');

            results_array.push({transaction_date,job_no,company_name,commission_received,commission_pay});
        });
        res.statusCode = 200;

        res.setHeader('Content-Type', 'text/csv');

        res.setHeader("Content-Disposition", 'attachment; filename='+filename);

       res.csv(results_array,true);
        
    });
});
//Commission History Report Ends Here

module.exports = router;