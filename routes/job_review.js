var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Stratergy;
var session = require('express-session');
var User = require('../modules/users');
var connection = require('./db');
var path = require('path');
var multer = require('multer');
var bcrypt = require('bcryptjs');
var datatablesQuery = require('datatables-query');
var dateFormat = require('dateformat');
var mydate = require('current-date');


//storage 

var storage =  multer.diskStorage({
	destination: './public/uploads/proofs',
	filename: function(req, file, cb){
		cb(null,file.fieldname+ '-' + Date.now() + path.extname(file.originalname));
	}
});


//init uploads

var upload = multer({
	storage: storage
}).fields([{name:'address_proof'},{name:'id_proof'}]);

//Job Review List

router.get('/jobreview', function (req, res) {
	var sel_customer = "select company_name,id,user_id from service_company_register order by id desc";
    connection.query(sel_customer,function(errors,results,fields){
        var job_no = "select job_no from service_booking order by id desc";
        connection.query(job_no,function(errors,rows,fields){
			res.render('jobreview_list',{
				items: results,
                job: rows
			});
		});
	});
});

//Job Review Ajax Table Fetch

router.post('/jobreview_ajax',function(req,res){
	
	params = req.body;
	var company_id = req.body.company_id;
    var from_date   = req.body.from_date;
    var to_date     = req.body.to_date;
    var where = '';
    
    //Filter Area
    if(company_id != '' && company_id != undefined){
        where += " and service_company_id='"+company_id+"'";
    }
    if(from_date != '' && to_date == '' && company_id == ''){
        where += " and date_time >= '"+from_date+"'";
    }
    if(from_date == '' && to_date != '' && company_id == ''){
        where += " and date_time <= '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && company_id == ''){
        where += " and date_time between '"+from_date+"' and '"+to_date+"'";
    }
    if(from_date != '' && to_date != '' && company_id != '' && from_date != undefined && to_date != undefined && company_id != undefined){
        where += " and date_time between '"+from_date+"' and '"+to_date+"' and service_company_id='"+company_id+"'";
    }
    //Filter Area Query Ends Here
	var start = params.start;
	var end = params.length;
			var val = start,end;
			//var ss = JSON.stringify(params.search);
			//console.log(ss);
			//console.log(params[search][value]);
			if(where == ''){
				var sq = 'select * from service_booking order by id desc LIMIT '+start+','+end+'';
			}else{
				var sq = 'select * from service_booking where 1 '+where+' order by id desc LIMIT '+start+','+end+'';
			}
			
	connection.query(sq,function (error, results, fields) {
		
			connection.query('select * from service_booking',function(error,main,fields){
				
			
				var row = new Array;
				if(where == ''){
					var tot = main.length;
				}else{
					var tot = results.length;
				}
				var i = start;
				var img;
				
					results.forEach(function(element) {
						
						var job = '<span id="job'+element.id+'"></span>';
						
						job += '<script>$(document).ready( function () {var id = '+element.work_category_id+';$.ajax({type: "POST",data: {str: id},url: "/job_review/get_job",success: function(data) {var sillyString = data.substr(1).slice(0,-1);var obj = JSON.parse(sillyString);var job_name = obj.work_category;$("#job'+element.id+'").html(job_name);} }); });';
						
						var cus = '<span id="cus'+element.id+'"></span>';
						
						cus += '<script>$(document).ready( function () {var id = '+element.customer_id+';$.ajax({type: "POST",data: {str: id},url: "/job_review/get_customer",success: function(data) {var sillyString = data.substr(1).slice(0,-1);var obj = JSON.parse(sillyString);var customer_name = obj.customer_name;$("#cus'+element.id+'").html(customer_name);} }); });';
						
						if(element.status == 'Completed'){
							var stat = '<div class="label label-success">Completed</div>';
						}else if(element.status == 'Processing'){
							var stat = '<div class="label label-primary">In Progress</div>';
						}else if(element.status == 'Cancel'){
							var stat = '<div class="label label-danger">Cancel</div>';
						}else if(element.status == 'Ongoing'){
							var stat = '<div class="label label-danger">OnGoing</div>';
						}else if(element.status == 'Pending'){
							var stat = '<div class="label label-danger">Pending</div>';
						}
						
						//if(element.status != "Cancel"){
						var mod = '<a href="#" data-toggle="modal" data-target="#basicModal'+element.id+'"><i class="fa fa-eye fa-2x"></i></a>';
						
						mod += '<div class="modal animated flipInX" id="basicModal'+element.id+'" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><div class="modal-dialog"><div class="modal-content modal-report"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Job Details</h4></div><div class="modal-body inner-model"><div role="tabpanel"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#uploadTab'+element.id+'" aria-controls="uploadTab" role="tab" data-toggle="tab">View Job</a></li><li role="presentation"><a href="#browseTabss'+element.id+'" aria-controls="browseTab" role="tab" data-toggle="tab">Payment Details</a></li></ul><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="uploadTab'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default"><div class="panel-body">';
						
						
						mod+='<script>$(document).ready( function () {var id = "'+element.job_no+'";$.ajax({type: "POST",data: {str: id},url: "/job_review/get_booking",success: function(data) {var obj = JSON.parse(data);var job_name = obj.job;var job_no = obj.job_no;var work_date = obj.work_date;var service_company = obj.service;var work_address = obj.work_location;var customer = obj.customer;var description = obj.description;var paid_amount = obj.paid_amount;var quote_amount = obj.quote_amount;';
						
						mod+='$("#job_no'+element.id+'").html(job_no);';
						
						mod+='$("#work_date'+element.id+'").html(work_date);';
						
						mod+='$("#work_address'+element.id+'").html(work_address);';
						
						mod+='$("#description'+element.id+'").html(description);';

						mod+='$("#customer_name'+element.id+'").html(customer);';
						
						mod+='$("#service_company'+element.id+'").html(service_company);';
						
						mod+='$("#job_name'+element.id+'").html(job_name);';

						mod+='$("#job_no'+element.id+'").html(job_no);';
						
						mod+='$("#paid_amount'+element.id+'").html(paid_amount);';
						
						mod+='$("#quote_amount'+element.id+'").html(quote_amount);';
						
						mod+=' } });});</script>';
						
						mod+='<div class="row"><div class="row"><div class="col-md-12"><h1 class="only-bottom-margin"><span id="job_no'+element.id+'"></span></h1></div></div><div class="row"><div class="col-lg-12"><div class="col-md-6"><label>Job Name:</label><span id="job_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Customer Name:</label><span id="customer_name'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Service Company:</label><span id="service_company'+element.id+'" class="text-muted"></span></div><div class="col-md-6"><label>Work Date:</label><span id="work_date'+element.id+'" class="text-muted"></span></div><div class="col-lg-6"><label>Work Location:</label> <span id="work_address'+element.id+'" class="text-muted"></span></div><div class="col-lg-6"><label>Description:</label> <span id="description'+element.id+'" class="text-muted"></span></div></div></div></div></div></div></div></div></div></div>';
						
						mod+='<div role="tabpanel" class="tab-pane" id="browseTabss'+element.id+'"><br><div class="container"><div class="row"><div class="col-md-9"><div class="panel panel-default">';
																	
						mod+='<div class="row"><div class="col-lg-12"><div class="col-md-6"><label>Quote Amount : </label><span id="quote_amount'+element.id+'" class="text-muted" style="font-size: 18px;"></span></div><div class="col-md-6"><label>Paid Amount : </label> <span id="paid_amount'+element.id+'" class="text-muted" style="font-size: 18px;"></span></div></div></div>';
						
						mod+='</div></div></div></div></div></div></div></div>';
						/*}else{
							var mod = '<p style="font-size: 18px;color: red;"><b>No Details</b></p>';
						}*/
						
						if(element.status == 'Cancel'){
							
							var canc = '<p><span><b>Cancel Date : </b></span> <span id="cancel_date'+element.id+'"></span></p>';
							
							canc+='<script>$(document).ready( function () {var id = '+element.id+';var type="cancel";$.ajax({type: "POST",data: {str: id,type: type},url: "/job_review/get_booking",success: function(data) {var obj = JSON.parse(data);var cancel_date = obj.cancel_date;';
							
							canc+='$("#cancel_date'+element.id+'").html(cancel_date);';
							
							canc+=' } });});</script>';
						
						}else{
							var canc = '<p style="font-size: 18px;color: red;"><b>No Details</b></p>';
						}
						var booking_date = dateFormat(element.date_time,'dd-mm-yyyy');
						row.push([++i,element.job_no,booking_date,job,cus,stat,mod,canc]);
					});
					  if (error) 
					  {
							throw error;
					  }else{
						  
							res.send(JSON.stringify({ "data": row,"recordsTotal": tot,"recordsFiltered": tot}));
					  }
			});			  
		});
		
	
});


//Get Job Name

router.post('/get_job',function(req,res){
	var id = req.body.str;
	var qr = connection.query('select work_category from work_category where id=? order by id desc', [id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.send(JSON.stringify(results));
			  }  
		});
		
});

//Get Customer Name

router.post('/get_customer',function(req,res){
	var id = req.body.str;
	var qr = connection.query('select customer_name from customer_register where id=? order by id desc', [id], function (error, results, fields) {
			  if (error) 
			  {
				  throw error;
			  }else{
				  res.send(JSON.stringify(results));
			  }  
		});
		
});

//Get Booking Details

router.post('/get_booking',function(req,res){
	var id = req.body.str;
	var type = req.body.type;
	
	if(type == 'cancel'){
		
		var sql_book = "select * from service_booking where id='"+id+"' order by id desc";
	}else{
		var sql_book = "select * from service_booking where job_no='"+id+"' order by id desc";
	}
	connection.query(sql_book, [id], function (error, results, fields) {
		
		var job_id = results[0]['job_no'];
		var customer_id = results[0]['customer_id'];
		var service_id = results[0]['service_company_id'];
		var employee_id = results[0]['employee_id'];
		var work_date = results[0]['work_date'];
		var work_time = results[0]['work_time'];
		var work_category_id = results[0]['work_category_id'];
		var work_address = results[0]['work_address'];
		var flag = 0;
		if(employee_id == '0' && service_id != '0'){
			sql_employee = "select company_name,user_id from service_company_register where id='"+service_id+"'";
		}else if(service_id != '0' && employee_id != '0'){
			sql_employee = "select company_staff.staff_name,company_staff.staff_unique_id,service_company_register.company_name,service_company_register.user_id from company_staff inner join service_company_register on service_company_register.id = company_staff.service_company_id where company_staff.id='"+employee_id+"' and service_company_id='"+service_id+"'";
			flag = 1;
		}
		connection.query('select work_category from work_category where id=? order by id desc', [work_category_id], function (error, jobs, fields) {
				var job_name = jobs[0]['work_category'];
			connection.query('select customer_name from customer_register where id=? order by id desc',[customer_id],function(err, cus, fields){
				var customer_name = cus[0]['customer_name'];
				var company_name;
				var user_id;
				var staff_name;
				var staff_unique_id;
				if((service_id != '0' && employee_id != '0') || (service_id != '0' && employee_id == '0')){
					connection.query(sql_employee,function(er,serv,fields){
						if(flag == 1){
							company_name = serv[0]['company_name'];
							user_id = serv[0]['user_id'];
							staff_name = serv[0]['staff_name'];
							staff_unique_id = serv[0]['staff_unique_id'];
						}else{
							company_name = serv[0]['company_name'];
							user_id = serv[0]['user_id'];
							staff_name = '';
							staff_unique_id = '';
						}
					});
				}else{
					company_name = 'Not Alloted';
					user_id = '';
					staff_name = '';
					staff_unique_id = '';
				}
					// var work_date = dateFormat(work,'dd-mm-yyyy');
					 var status = results[0]['status'];
					 var description = results[0]['description'];
					 var paid_amount = results[0]['paid_amount'];
					 var quote_amount = results[0]['quote_amount'];
					 if( status == 'Cancel'){
					 	var cancel = results[0]['cancel_date'];
					 	var cancel_date = dateFormat(cancel,'dd-mm-yyyy');
					 	var cancel_reason = results[0]['cancel_reason'];
					 }
				  	res.send(JSON.stringify({"job": job_name,"customer": customer_name,"service": company_name,"user_id": user_id,"staff_name": staff_name,"staff_unique_id": staff_unique_id,"job_no": job_id,"work_date": work_date,"work_location": work_address,"cancel_date": cancel_date,"cancel_reason": cancel_reason,"description": description,"paid_amount": paid_amount,"quote_amount": quote_amount}));
				
			});
		});
			
	});
	
		
});


//Get service comapny name,customer name,job name

router.post('/get_all',function(req,res){
	var job_id = req.body.job_id;
	var service_id = req.body.service_id;
	var customer_id = req.body.customer_id;
	connection.query('select job_name from palma_job where id=? order by id desc', [job_id], function (error, results, fields) {
			  connection.query('select customer_name from customer_register where id=? order by id desc',[customer_id],function(err, cus, fields){
				 connection.query('select company_name from service_company_register where id=? order by id desc',[service_id],function(er,serv,fields){
					 
					 
					 var job_name = results[0]['job_name'];
					 var cus_name = cus[0]['customer_name'];
					 var com_name = serv[0]['company_name'];
					
					  res.send(JSON.stringify({job: job_name,customer: cus_name,service: com_name}));
				  });
				  
			  });
				  
		});
		
});

module.exports = router;